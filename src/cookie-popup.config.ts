import { NgcCookieConsentConfig } from 'ngx-cookieconsent';

export const cookieConfig: NgcCookieConsentConfig = {
    cookie: {
        domain: 'tinesoft.github.io'
    },
    position: 'bottom-right',
    theme: 'edgeless',
    palette: {
        popup: {
            background: '#cdcdcd',
            text: '#000000',
            link: '#ffffff'
        },
        button: {
            background: '#422134',
            text: '#ffffff',
            border: 'transparent'
        }
    },
    type: 'opt-in',
    content: {
        message: 'Diese Website verwendet Cookies – nähere Informationen dazu und zu Ihren Rechten als Benutzer finden Sie in unserer Datenschutzerklärung am Ende der Seite. Klicken Sie auf „Ich stimme zu“, um Cookies zu akzeptieren und direkt unsere Website besuchen zu können.',
        dismiss: 'Alles klar!',
        allow: 'Alles klar!',
        deny: 'Nein danke!',
        link: 'Zeig mir mehr',
        href: 'https://cookiesandyou.com'
    },
    autoOpen: false,
    revokable: false,
};