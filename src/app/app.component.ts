import {
    AfterContentChecked,
    AfterContentInit, AfterViewInit, ChangeDetectorRef,
    Component, Input, OnInit
} from '@angular/core';
import { AuthGuardService } from './shared/services/auth-guard.service';
import { environment } from '../environments/environment';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { NotificationService } from './shared/services/notification.service';
import { PrivacyService } from './shared/services/privacy.service';
import { startWith, tap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { Angulartics2GoogleGlobalSiteTag } from 'angulartics2/gst';
import { GuidedTour, GuidedTourService, Orientation } from 'ngx-guided-tour';
import { StartTourService } from './shared/services/start-tour.service';

@Component({
    selector: 'eos-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, AfterViewInit{

    public isAuthenticated: boolean;
    public version;

    constructor(private authGuardService: AuthGuardService,
                private titleService: Title,
                private router: Router,
                private translate: TranslateService,
                private notificationService: NotificationService,
                private angulartics: Angulartics2GoogleGlobalSiteTag,
                private guidedTourService: GuidedTourService,
                private startTourService: StartTourService,
                private privacyService: PrivacyService)
    {
        translate.setDefaultLang('de');
        translate.use('de');
        angulartics.startTracking();
    }

    ngOnInit() {
        this.authGuardService.populate();
        this.version = environment.version;
        this.titleService.setTitle(environment.appName);
        this.privacyService.setCookieWorkflow();
        this.checkAuth();
        this.startTourService.startTourEmitter
            .subscribe((tour) => {
                this.guidedTourService.startTour(tour);
        });
    }

    checkAuth(){
        this.isAuthenticated = this.authGuardService.isLoggedIn();
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.authGuardService.isAuthenticated
                .pipe(
                    startWith(false),
                    tap(() => this.checkAuth())
                ).subscribe();
        });
    }
}
