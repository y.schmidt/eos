import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { SharedModule } from '../shared/shared.module';
import { CalendarViewComponent } from './calendar-view/calendar-view.component';
import { CalendarRoutingModule } from './calendar-routing.module';
import { EventModalComponent } from '../project-management/event-modal/event-modal.component';
import { ProjectManagementModule } from '../project-management/project-management.module';
import { EventDeleteConfirmModalComponent } from '../project-management/event-delete-confirm-modal/event-delete-confirm-modal.component';
import { UserCalendarComponent } from './user-calendar/user-calendar.component';

@NgModule({
  declarations: [
    CalendarViewComponent,
    UserCalendarComponent
  ],
  imports: [
    CalendarRoutingModule,
    CommonModule,
    SharedModule,
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory }),
      ProjectManagementModule
  ],
  entryComponents: [
    EventModalComponent,
      EventDeleteConfirmModalComponent
  ]
})
export class EosCalendarModule { }
