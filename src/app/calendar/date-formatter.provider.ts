import { CalendarDateFormatter, DateFormatterParams } from 'angular-calendar';
import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import {
    addDays,
    startOfWeek,
    endOfWeek
} from 'date-fns';

@Injectable()
export class DateFormatter extends CalendarDateFormatter {

    public dayViewHour({ date, locale }: DateFormatterParams): string {
        return new DatePipe(locale).transform(date, 'HH:mm', locale);
    }

    public weekViewHour({ date, locale }: DateFormatterParams): string {
        return this.dayViewHour({ date, locale });
    }

    public monthViewTitlesmall({ date, locale }: DateFormatterParams): string {
        return new DatePipe(locale).transform(date, 'MMM yy', locale);
    }

    public monthViewColumnHeadersmall({ date, locale }: DateFormatterParams): string {
        return new DatePipe(locale).transform(date, 'EEEEEE', locale);
    }

    public weekViewColumnHeadersmall({ date, locale }: DateFormatterParams): string {
        return this.monthViewColumnHeadersmall({date, locale});
    }

    public weekViewTitle({ date, locale }: DateFormatterParams): string {
        return `${new DatePipe(locale).transform(startOfWeek(date, {weekStartsOn: 1}), 'd. MMM', locale)} - 
        ${new DatePipe(locale).transform(endOfWeek(date, {weekStartsOn: 1}), 'd. MMM yyyy', locale)}`;
    }

    public weekViewTitlesmall({ date, locale }: DateFormatterParams): string {
        return `${new DatePipe(locale).transform(startOfWeek(date, {weekStartsOn: 1}), 'd', locale)} - 
        ${new DatePipe(locale).transform(endOfWeek(date, {weekStartsOn: 1}), 'd.MM', locale)}`;
    }

    public dayViewTitlesmall({ date, locale }: DateFormatterParams): string {
        return new DatePipe(locale).transform(date, 'EEEEEE d.MM', locale);
    }

    public dayViewTitle({ date, locale }: DateFormatterParams): string {
        return new DatePipe(locale).transform(date, 'EEEE, dd. MMM, yyyy', locale);
    }

}