import { Component, OnInit } from '@angular/core';
import { Project } from '../../shared/models/project';
import { UserService } from '../../shared/services/user.service';
import { ProjectService } from '../../shared/services/project.service';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
  selector: 'eos-user-calendar',
  templateUrl: './user-calendar.component.html',
  styleUrls: ['./user-calendar.component.css']
})
export class UserCalendarComponent implements OnInit {

  projects: Project[];

  constructor(private userService: UserService,
              private projectService: ProjectService,
              private notificationService: NotificationService) { }

  ngOnInit() {
    this.getProjects();
  }

  getProjects() {
    this.projectService.getProjects(this.userService.getUserId())
        .subscribe(projects => {
              this.projects = projects;
            },
            error => this.notificationService.setErrorNotification(error));
  }
}
