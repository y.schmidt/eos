import { LOCALE_ID, Inject, Injectable } from '@angular/core';
import { CalendarEventTitleFormatter, CalendarEvent } from 'angular-calendar';
import { DatePipe } from '@angular/common';
import { Event } from '../shared/models/event';

@Injectable()
export class EventTitleFormatter extends CalendarEventTitleFormatter {
    constructor(@Inject(LOCALE_ID) private locale: string) {
        super();
    }

    // you can override any of the methods defined in the parent class
    month(event: Event): string {
        // check if event is eventgroup
        if(event.type !== undefined){
            return `<b>${new DatePipe(this.locale).transform(
                event.start,
                'HH:mm',
                this.locale
            )} - ${new DatePipe(this.locale).transform(
                event.end,
                'HH:mm',
                this.locale
            )} Uhr</b> ${event.title}, ${event.location}`;
        } else {
            return `${event.title}, ${event.location}`;
        }
    }

    week(event: CalendarEvent): string {
        return `${event.title}`;
    }

    day(event: CalendarEvent): string {
        return `${event.title}`;
    }
}
