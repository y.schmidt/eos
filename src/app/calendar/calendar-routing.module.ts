import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuardService } from '../shared/services/auth-guard.service';
import { UserCalendarComponent } from './user-calendar/user-calendar.component';

const routes: Routes = [
  { path: '', component: UserCalendarComponent, canActivate:[AuthGuardService]}
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})

export class CalendarRoutingModule {}



