import {
  Component,
  ChangeDetectionStrategy,
  OnInit, ChangeDetectorRef, OnDestroy, Input
} from '@angular/core';
import { Subject } from 'rxjs';
import { tap, takeUntil } from 'rxjs/operators';

import {
  isSameDay,
  isSameMonth,
  isSameMinute
} from 'date-fns';
import {
  CalendarEvent,
  CalendarDateFormatter,
  CalendarEventTitleFormatter,
  CalendarView,
  CalendarEventAction,
  CalendarMonthViewDay,
} from 'angular-calendar';
import { EventService } from '../../shared/services/event.service';
import { EventTitleFormatter } from '../event-title-formatter.provider';
import { Router } from '@angular/router';
import { DateFormatter } from '../date-formatter.provider';
import { ProjectService } from '../../shared/services/project.service';
import { UserService } from '../../shared/services/user.service';
import { Project } from '../../shared/models/project';
import { Event } from '../../shared/models/event';
import { NotificationService } from '../../shared/services/notification.service';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { EventModalComponent } from '../../project-management/event-modal/event-modal.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { EventDeleteConfirmModalComponent } from '../../project-management/event-delete-confirm-modal/event-delete-confirm-modal.component';
import { EventgroupService } from '../../shared/services/eventgroup.service';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { environment } from '../../../environments/environment';

const colors: any = [
  {
    primary: '#ad2121',
    secondary: '#FAE3E3',
    theme: 'danger'
  },
  {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
    theme: 'primary'
  },
  {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
    theme: 'warning'
  }
];


@Component({
  selector: 'eos-calendar-view',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './calendar-view.component.html',
  styleUrls: ['./calendar-view.component.css'],
  providers: [
    {
      provide: CalendarEventTitleFormatter,
      useClass: EventTitleFormatter
    },
    {
      provide: CalendarDateFormatter,
      useClass: DateFormatter
    }
  ]
})
export class CalendarViewComponent implements OnInit, OnDestroy {

  _projects: Project[];

  modalRef: BsModalRef;
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  viewDate: Date = new Date();
  refresh: Subject<any> = new Subject();
  activeDayIsOpen: boolean = true;
  private destroy$ = new Subject();

  events: any[] = [];
  showEvents: any[] = [];
  headerView: string = '';

  groupedSimilarEvents: CalendarEvent[] = [];

  calendarProjectFilter: {
    project: Project,
    show: boolean
  }[] = [];

  calendarFilter =  {
    announced: true,
    unannounced: true,
    noResponse: true
  };

  actions: CalendarEventAction[] = [
    {
      label: '<a class="text-info"><i class="fas fa-edit"></i></a>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.openEditEventModal(event);
      }
    },
    {
      label: '<a class="text-danger"><i class="fas fa-trash"></i></a>',
      a11yLabel: 'Delete',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.openConfirmModal(event);
      }
    }
  ];

  constructor(private eventService: EventService,
              private breakpointObserver: BreakpointObserver,
              private eventgroupService: EventgroupService,
              private userService: UserService,
              private projectService: ProjectService,
              private notificationService: NotificationService,
              private router: Router,
              private authGuard: AuthGuardService,
              private cd: ChangeDetectorRef,
              private modalService: BsModalService) { }

  ngOnInit() {
    this.initBreakpointObserver();
  }

  @Input() set projects(projects: Project[]) {
    this._projects = projects;
    this.setFilterConfig();
    this.getEvents();
    this.getEventgroups();
    this.groupEvents();
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  initBreakpointObserver(){
    const CALENDAR_RESPONSIVE = {
      small: {
        breakpoint: '(max-width: ' + environment.breakpoints.small + 'px)',
        headerView: 'small'
      },
      medium: {
        breakpoint: '(max-width: ' + environment.breakpoints.medium + 'px)',
        headerView: 'small'
      },
      large: {
        breakpoint: '(max-width: ' + environment.breakpoints.large + 'px)',
        headerView: ''
      }
    };

    this.breakpointObserver
        .observe(
            Object.values(CALENDAR_RESPONSIVE).map(({ breakpoint }) => breakpoint)
        )
        .pipe(takeUntil(this.destroy$))
        .subscribe((state: BreakpointState) => {
          const foundBreakpoint = Object.values(CALENDAR_RESPONSIVE).find(
              ({ breakpoint }) => !!state.breakpoints[breakpoint]
          );
          if (foundBreakpoint) {
            this.headerView = foundBreakpoint.headerView;
          }
          this.cd.markForCheck();
        });
  }

  setFilterConfig(){
    this._projects.forEach((project) => {
      this.calendarProjectFilter.push({project: project, show: true});
    })
  }

  getEventgroups(){
    for(let i = 0; i < this._projects.length; i++){
      const isProjectEditor = this.authGuard.hasUserPermission('projectEditor', this._projects[i].ensembles[0].id);
      this.eventgroupService.getEventgroupsForProject(this._projects[i].ensembles[0].id, this._projects[i].id)
          .pipe(tap(eventgroups => {
            return eventgroups.map(eventgroup => {
              if(isProjectEditor){
                eventgroup.actions = this.actions;
              }
              eventgroup.color = colors[i%3];
              eventgroup.allDay = true;
              return eventgroup;
            })
          }))
          .subscribe((eventgroups) => {
            this.events = this.events.concat(eventgroups);
            this.showEvents = this.events;
            this.refresh.next();
          })
    }
  }

  getEvents(){
    this.events = [];
    this.showEvents = [];
    for(let i = 0; i < this._projects.length; i++){
      this._projects[i].color = colors[i%3];
      const isProjectEditor = this.authGuard.hasUserPermission('projectEditor', this._projects[i].ensembles[0].id);
      this.eventService.getEventsForProject(this._projects[i].ensembles[0].id, this._projects[i].id, '/includeEventgroups')
          .pipe(tap(events => {
            return events.map(event => {
              if(isProjectEditor){
                event.actions = this.actions;
              }
              event.color = colors[i%3];
              return event;
            })
          }))
          .subscribe((events) => {
            this.events = this.events.concat(events);
            this.showEvents = this.events;
            this.refresh.next();
          });
    }
  }

  filterCalendar(){
    let events: Event[] = [];
    this.calendarProjectFilter.forEach((filter) => {
      if(filter.show){
        if(this.calendarFilter.announced){
          const filtered = this.events.filter((el) => {
            return el.project.id == filter.project.id && el.announcements.length > 0 && el.announcements[0].status;
          });
          events = events.concat(filtered);
        }
        if(this.calendarFilter.unannounced){
          const filtered = this.events.filter((el) => {
            return el.project.id == filter.project.id && el.announcements.length > 0 && !el.announcements[0].status;
          });
          events = events.concat(filtered);
        }
        if(this.calendarFilter.noResponse){
          const filtered = this.events.filter((el) => {
            return el.project.id == filter.project.id && el.announcements.length == 0;
          });
          events = events.concat(filtered);
        }
      }
    });
    this.showEvents = events;
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.activeDayIsOpen = !((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
          events.length === 0);
      this.viewDate = date;
    }
  }

  handleEvent (action: string, event): void {
    const project = this._projects.find((el) => el.id == event.project.id);
    this.router.navigateByUrl(`/projects/${event.project.id}/ensembles/${project.ensembles[0].id}`);
  }


  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  deleteEvent(eventToDelete) {
    if(eventToDelete.deleteSerial){
      this.eventService.deleteSerialEvent(eventToDelete.event.id, this.getEnsembleIdFromEvent(eventToDelete.event))
          .subscribe(() => {
            this.getEvents();
          }, error => this.notificationService.setErrorNotification(error));
    } else {
      this.eventService.deleteEvent(eventToDelete.event.id, this.getEnsembleIdFromEvent(eventToDelete.event))
          .subscribe(() => {
            this.events = this.events.filter(event => event.id !== eventToDelete.event.id);
            this.showEvents = this.showEvents.filter(event => event.id !== eventToDelete.event.id);
            this.refresh.next();
          }, error => this.notificationService.setErrorNotification(error));
    }
  }

  openConfirmModal(event){
    this.modalRef = this.modalService.show(EventDeleteConfirmModalComponent, {initialState:
          {event: event}});
    this.modalRef.content.confirmEvent
        .subscribe((event) => {
          this.deleteEvent(event);
          this.modalRef.hide();
        });
  }

  openEditEventModal(event) {
    const cacheEvent = event;
    this.modalRef = this.modalService.show(EventModalComponent,{
      initialState: {
        mode: 'edit',
        event: Object.assign({}, event),
        ensembleId: this.getEnsembleIdFromEvent(event),
        project: event.project,
        dateMode: 'project'
      }
    });
    this.modalRef.content.updateMultipleEventsEmitter
        .subscribe(() => this.getEvents());
    this.modalRef.content.updateEventEmitter
        .subscribe((newEvent) => {
          newEvent.color = cacheEvent.color;
          newEvent.actions = cacheEvent.actions;
          const index = this.showEvents.findIndex((el) => el.id == newEvent.id);
          this.showEvents[index] = newEvent;
          this.refresh.next();
        });
  }

  getEnsembleIdFromEvent(event): string {
    const project = this._projects.find((el) => el.id == event.project.id);
    return project.ensembles[0].id;
  }

  groupEvents(){
    // group any events together that have the same type and dates
    // use for when you have a lot of events on the week or day view at the same time
    this.groupedSimilarEvents = [];
    const processedEvents = new Set();
    this.events.forEach(event => {
      if (processedEvents.has(event)) {
        return;
      }
      const similarEvents = this.events.filter(otherEvent => {
        return (
            otherEvent !== event &&
            !processedEvents.has(otherEvent) &&
            isSameMinute(otherEvent.start, event.start) &&
            (isSameMinute(otherEvent.end, event.end) ||
                (!otherEvent.end && !event.end)) &&
            otherEvent.color.primary === event.color.primary &&
            otherEvent.color.secondary === event.color.secondary
        );
      });
      processedEvents.add(event);
      similarEvents.forEach(otherEvent => {
        processedEvents.add(otherEvent);
      });
      if (similarEvents.length > 0) {
        this.groupedSimilarEvents.push({
          title: `${similarEvents.length + 1} events`,
          color: event.color,
          start: event.start,
          end: event.end,
          meta: {
            groupedEvents: [event, ...similarEvents]
          }
        });
      } else {
        this.groupedSimilarEvents.push(event);
      }
    });
  }

  beforeMonthViewRender({ body }: { body: Array<CalendarMonthViewDay<any>>; }): void {
    // month view has a different UX from the week and day view so we only really need to group by the type
    body.forEach(cell => {
      const groups = {};
      cell.events.forEach((event: Event) => {
        groups[event.color.theme] = groups[event.color.theme] || [];
        groups[event.color.theme].push(event);
      });
      cell['eventGroups'] = Object.entries(groups);
    });
  }
}