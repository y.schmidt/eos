import { Component, OnInit } from '@angular/core';
import { CommunicationService } from '../../shared/services/communication.service';
import { Mail } from '../../shared/models/mail';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../shared/services/user.service';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
    selector: 'eos-mail-details',
    templateUrl: './mail-details.component.html',
    styleUrls: ['./mail-details.component.css']
})
export class MailDetailsComponent implements OnInit {

    public mail: Mail;
    constructor(private route: ActivatedRoute,
                private comService: CommunicationService,
                private userService: UserService,
                private notificationService: NotificationService) { }

    ngOnInit() {
        this.comService.selectedMail.subscribe((mail) => {
            if(mail.subject == ''){
                this.getMailById();
            } else {
                this.mail = mail;
            }
        }, error => this.notificationService.setErrorNotification(error));
    }

    getMailById(){
        const mailId = this.route.snapshot.paramMap.get('id');
        this.comService.getMailById(this.userService.getUserId(), mailId)
            .subscribe((mail) => {
                this.mail = mail;
            }, error => this.notificationService.setErrorNotification(error));
    }

}
