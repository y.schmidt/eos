import { Component, Input, OnInit } from '@angular/core';
import { Mail } from '../../shared/models/mail';

@Component({
  selector: 'eos-mail-preview',
  templateUrl: './mail-preview.component.html',
  styleUrls: ['./mail-preview.component.css']
})
export class MailPreviewComponent implements OnInit {

  @Input()
  mail: Mail;

  constructor() { }

  ngOnInit() {
  }

}
