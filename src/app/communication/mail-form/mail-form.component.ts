import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Mail } from '../../shared/models/mail';
import { NgForm } from '@angular/forms';
import { FormService } from '../../shared/services/form.service';

@Component({
    selector: 'eos-mail-form',
    templateUrl: './mail-form.component.html',
    styleUrls: ['./mail-form.component.css']
})
export class MailFormComponent implements OnInit {

    @Input()
    mail: Mail;

    @ViewChild('f')
    form: NgForm;

    constructor(private formService: FormService) { }

    ngOnInit() {
    }

    @Input()
    set touched(value: boolean){
        if(value){
            this.markAsTouched();
        }
    }

    markAsTouched(){
        this.formService.markFormGroupTouched(this.form);
    }

    messageChanged(data: string){
        this.mail.message = data;
    }

    fileChangeEvent(event: any): void {
        this.mail.attachements = this.mail.attachements.concat(Array.prototype.slice.call(event.target.files));
    }
}
