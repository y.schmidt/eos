import { Component, OnInit } from '@angular/core';
import { Mail } from '../../shared/models/mail';
import { CommunicationService } from '../../shared/services/communication.service';
import { UserService } from '../../shared/services/user.service';
import { Router } from '@angular/router';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
  selector: 'eos-drafts',
  templateUrl: './drafts.component.html',
  styleUrls: ['./drafts.component.css']
})
export class DraftsComponent implements OnInit {

  mails: Mail[];

  constructor(private comService: CommunicationService,
              private userService: UserService,
              private router: Router,
              private notificationService: NotificationService) { }

  ngOnInit() {
    this.getMails();
  }

  getMails(){
    this.comService.getDraftsForUser(this.userService.getUserId())
        .subscribe((mails) => {
          this.mails = mails;
        }, error => this.notificationService.setErrorNotification(error));
  }
}
