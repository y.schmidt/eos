import { Component, OnInit } from '@angular/core';
import { CommunicationService } from '../../shared/services/communication.service';
import { UserService } from '../../shared/services/user.service';
import { NotificationService } from '../../shared/services/notification.service';
import { Mail } from '../../shared/models/mail';
import { Router } from '@angular/router';

@Component({
  selector: 'eos-sent-mail',
  templateUrl: './sent-mail.component.html',
  styleUrls: ['./sent-mail.component.css']
})
export class SentMailComponent implements OnInit {

  mails: Mail[];

  constructor(private comService: CommunicationService,
              private userService: UserService,
              private router: Router,
              private notificationService: NotificationService) { }

  ngOnInit() {
    this.getMails();
  }

  getMails(){
    this.comService.getMailsForUser(this.userService.getUserId())
        .subscribe((mails) => {
          this.mails = mails;
        }, error => this.notificationService.setErrorNotification(error));
  }

}
