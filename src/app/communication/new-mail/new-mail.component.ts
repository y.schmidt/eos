import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Mail } from '../../shared/models/mail';
import { Observable } from 'rxjs';
import { CanComponentDeactivate } from '../../shared/services/discard-changes.guard';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DiscardChangesComponent } from '../../shared/components/discard-changes/discard-changes.component';
import { MailFormComponent } from '../mail-form/mail-form.component';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { CommunicationService } from '../../shared/services/communication.service';
import { NotificationService } from '../../shared/services/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../shared/services/user.service';

@Component({
    selector: 'eos-new-mail',
    templateUrl: './new-mail.component.html',
    styleUrls: ['./new-mail.component.css']
})
export class NewMailComponent implements OnInit, CanComponentDeactivate, AfterViewInit {

    mail: Mail = new Mail();
    modalRef: BsModalRef;

    @ViewChild(MailFormComponent)
    mailFormComp: MailFormComponent;

    mailFormCompleted: boolean = false;
    mailFormError: boolean = false;

    mailSent: boolean = false;

    constructor(private modalService: BsModalService,
                private route: ActivatedRoute,
                private comService: CommunicationService,
                private notificationService: NotificationService,
                private routerService: Router,
                private userService: UserService,
                private authService: AuthGuardService) { }

    ngOnInit() {
        const mailId = this.route.snapshot.paramMap.get('id');
        if(mailId != null){
            this.comService.getMailById(this.userService.getUserId(), mailId)
                .subscribe((mail) => {
                    this.mail = mail;
                    if(mail.recipients && mail.recipients.length == 0){
                        this.mail.recipients = [];
                    }
                    this.mail.sender = this.authService.getAuthedUser();

                })
        } else {
            this.mail.sender = this.authService.getAuthedUser();
        }
    }

    ngAfterViewInit (): void {
        this.mailFormComp.form.statusChanges.subscribe(
            result => {
                this.mailFormCompleted = result == 'VALID';
            }
        );
    }

    canDeactivate(): Observable<boolean> | boolean {
        if(this.mailSent || !this.isMailEdited()){
            return true;
        } else {
            this.modalRef = this.modalService.show(DiscardChangesComponent, {
                initialState: {
                    showSaveEvent: true
                }
            });
            this.modalRef.content.confirmEvent.subscribe(() => {
                this.modalRef.hide();
            });
            this.modalRef.content.saveEvent.subscribe(() => {
                this.saveAsDraft();
                this.modalRef.hide();
            });
            return this.modalRef.content.confirmEvent;
        }
    }

    saveAsDraft(){
        this.comService.saveDrafts(this.userService.getUserId(), this.mail)
            .subscribe((mail) => {
                this.mail = mail;
            }, error => this.notificationService.setErrorNotification(error))
    }

    isMailEdited(): boolean{
        return this.mail.message != '' || this.mail.subject != '';
    }

    selectChanged(){
        this.mailFormError = !this.mailFormCompleted;
    }

    sendMail(){
        this.comService.sendMail(this.mail)
            .subscribe((mail) =>{
                this.mailSent = true;
                this.notificationService.setSuccessNotification('Mail verschickt');
                this.routerService.navigateByUrl('/communication/sent');
            }, error => this.notificationService.setErrorNotification(error));
    }
}
