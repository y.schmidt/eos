import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InboxComponent } from './inbox/inbox.component';
import { SharedModule } from '../shared/shared.module';
import { CommunicationRoutingModule } from './communication-routing.module';
import { MailFormComponent } from './mail-form/mail-form.component';
import { RecipientComponent } from './recipient/recipient.component';
import { MailDetailsComponent } from './mail-details/mail-details.component';
import { NewMailComponent } from './new-mail/new-mail.component';
import { SentMailComponent } from './sent-mail/sent-mail.component';
import { DraftsComponent } from './drafts/drafts.component';
import { MailReminderComponent } from './mail-reminder/mail-reminder.component';
import { MailPreviewComponent } from './mail-preview/mail-preview.component';
import { MailTableComponent } from './mail-table/mail-table.component';

@NgModule({
    imports: [
        CommonModule,
        CommunicationRoutingModule,
        SharedModule
    ],
    declarations: [ InboxComponent, MailFormComponent, RecipientComponent, MailDetailsComponent, NewMailComponent, SentMailComponent, DraftsComponent, MailReminderComponent, MailPreviewComponent, MailTableComponent ],
    exports: [
        MailFormComponent
    ]
})
export class CommunicationModule { }
