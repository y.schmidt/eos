import { Component, OnInit } from '@angular/core';
import { Mail } from '../../shared/models/mail';
import { CommunicationService } from '../../shared/services/communication.service';
import { UserService } from '../../shared/services/user.service';
import { Router } from '@angular/router';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
  selector: 'eos-mail-reminder',
  templateUrl: './mail-reminder.component.html',
  styleUrls: ['./mail-reminder.component.css']
})
export class MailReminderComponent implements OnInit {

  mails: Mail[];

  constructor(private comService: CommunicationService,
              private userService: UserService,
              private router: Router,
              private notificationService: NotificationService) { }

  ngOnInit() {
    this.getMails();
  }

  getMails(){
    this.comService.getReminderForUser(this.userService.getUserId())
        .subscribe((mails) => {
          this.mails = mails;
        }, error => this.notificationService.setErrorNotification(error));
  }
}
