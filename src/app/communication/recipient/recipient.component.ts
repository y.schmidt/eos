import {
    AfterContentInit, AfterViewChecked, AfterViewInit,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges
} from '@angular/core';
import { CommunicationService } from '../../shared/services/communication.service';
import { UserService } from '../../shared/services/user.service';
import { NotificationService } from '../../shared/services/notification.service';
import { RecipientGroup } from '../../shared/models/recipientGroup';
import { User } from '../../shared/models/user';
import { Mail } from '../../shared/models/mail';

@Component({
    selector: 'eos-recipient',
    templateUrl: './recipient.component.html',
    styleUrls: ['./recipient.component.css']
})
export class RecipientComponent implements OnInit {

    @Input()
    mail: Mail;

    recipientGroup: any[] = [];
    users: User[];

    @Output()
    recipientsChanged: EventEmitter<RecipientGroup> = new EventEmitter();

    selectedGroup: string;

    selectedCategory: string = 'organizations';
    selectedEventConfig: 'announced' | 'unannounced' | 'noresponse' = 'announced';

    constructor(private comService: CommunicationService,
                private userService: UserService,
                private notificationService: NotificationService) { }

    ngOnInit() {
        this.getRecipients();
        this.getRecipientGroup(this.selectedCategory);
    }

    getRecipients(){
        this.comService.getRecipientsForUser(this.userService.getUserId())
            .subscribe((users) => {
                this.users = users;
                this.setCheckedUser();
            },error => this.notificationService.setErrorNotification(error));
    }

    getRecipientGroup(config: string = ''){
        this.comService.getRecipientGroupForUser(this.userService.getUserId(), `/${config}`)
            .subscribe((group) => {
                this.recipientGroup = group;
            },error => this.notificationService.setErrorNotification(error));
    }

    setCheckedUser(){
        this.users.map((user) => {
            if(this.mail.recipients.some(e => e.id === user.id)){
                user.checked = true;
            }
        });
    }

    changeRecipient(recipients: any[]){
        let users = [];
        let announcementUsers = [];
        recipients.forEach((el) => {
            switch(this.selectedCategory){
                case 'organizations':
                case 'ensembles':
                    let ensembleUsers = [];
                    el.forEach(ensembleUser => {
                        ensembleUsers.push(ensembleUser.user);
                    });
                    users = users.concat(ensembleUsers);
                    break;
                case 'projects':
                    if(this.selectedEventConfig === 'noresponse'){
                        el.ensembles.forEach(ensemble => {
                            ensemble.ensembleUsers.forEach(ensembleUser => {
                                announcementUsers.push(ensembleUser.user);
                            });
                        });
                        users = users.concat(announcementUsers);
                    } else {
                        el.announcements.forEach(announcement => {
                            announcementUsers.push(announcement.user);
                        });
                        users = users.concat(announcementUsers);
                    }
                    break;
                case 'eventgroups':
                case 'events':
                    if(this.selectedEventConfig === 'noresponse'){
                        el.project.ensembles.forEach(ensemble => {
                            ensemble.ensembleUsers.forEach(ensembleUser => {
                                announcementUsers.push(ensembleUser.user);
                            });
                        });
                        users = users.concat(announcementUsers);
                    } else {
                        el.announcements.forEach(announcement => {
                            announcementUsers.push(announcement.user);
                        });
                        users = users.concat(announcementUsers);
                    }
                    break;
                default:
                    users = users.concat(el);
            }
        });
        users = users.filter((user, index) => users.findIndex(el => el.id == user.id) === index);
        this.mail.recipients = users;
        this.selectUsers(users);
    }

    userSelected(user: User){
        if(user.checked){
            this.mail.recipients.push(user);
        } else {
            const index = this.mail.recipients.findIndex((el) => el.id === user.id);
            if (index !== -1) {
                this.mail.recipients.splice(index, 1);
            }
        }
    }

    selectUsers(users: User[]){
        this.users.forEach((user) => {
            user.checked = users.findIndex(el => el.id == user.id) > -1;
        });
    }

    announcementStatusChanged(status: 'announced' | 'unannounced' | 'noresponse'){
        this.getRecipientGroup(`${this.selectedCategory}/${this.selectedEventConfig}`);
    }

    groupChanged(value: string){
        switch (value) {
            case 'projects':
            case 'eventgroups':
            case 'events':
                this.getRecipientGroup(`${this.selectedCategory}/${this.selectedEventConfig}`);
                break;
            default:
                this.getRecipientGroup(value);
        }
    }
}
