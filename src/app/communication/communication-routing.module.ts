import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../shared/services/auth-guard.service';
import { InboxComponent } from './inbox/inbox.component';
import { NewMailComponent } from './new-mail/new-mail.component';
import { SentMailComponent } from './sent-mail/sent-mail.component';
import { DraftsComponent } from './drafts/drafts.component';
import { DiscardChangesGuard } from '../shared/services/discard-changes.guard';
import { MailDetailsComponent } from './mail-details/mail-details.component';
import { MailReminderComponent } from './mail-reminder/mail-reminder.component';

const routes: Routes = [
    { path: '', component: InboxComponent, canActivate:[AuthGuardService], children: [
        { path: 'sent', component: SentMailComponent, canActivate:[AuthGuardService] },
        { path: 'reminder', component: MailReminderComponent, canActivate:[AuthGuardService] },
        { path: 'sent/:id', component: MailDetailsComponent, canActivate:[AuthGuardService] },
        { path: 'new', component: NewMailComponent, canActivate:[AuthGuardService], canDeactivate:[DiscardChangesGuard] },
        { path: 'new/:id', component: NewMailComponent, canActivate:[AuthGuardService], canDeactivate:[DiscardChangesGuard] },
        { path: 'drafts', component: DraftsComponent, canActivate:[AuthGuardService] },
        { path: 'drafts/:id', component: MailDetailsComponent, canActivate:[AuthGuardService] },
        { path: '', redirectTo: 'sent', pathMatch: 'full' },
        ]
    }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class CommunicationRoutingModule { }
