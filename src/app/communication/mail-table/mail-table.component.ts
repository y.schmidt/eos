import { Component, Input, OnInit } from '@angular/core';
import { Mail } from '../../shared/models/mail';
import { CommunicationService } from '../../shared/services/communication.service';
import { UserService } from '../../shared/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'eos-mail-table',
  templateUrl: './mail-table.component.html',
  styleUrls: ['./mail-table.component.css']
})
export class MailTableComponent implements OnInit {

  @Input()
  mails: Mail[];

  constructor(private comService: CommunicationService,
              private userService: UserService,
              private router: Router) { }

  ngOnInit() {
  }

  setSelectedMail(mail: Mail){
    this.comService.setSelectedMail(mail);
    if(mail.isDraft){
      this.router.navigateByUrl('/communication/drafts/' + mail.id);
    } else {
      this.router.navigateByUrl('/communication/sent/' + mail.id);
    }
  }

}
