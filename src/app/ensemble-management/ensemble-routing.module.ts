import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyEnsemblesComponent } from './my-ensembles/my-ensembles.component';
import { AuthGuardService } from '../shared/services/auth-guard.service';
import { EnsembleDashboardComponent } from './ensemble-dashboard/ensemble-dashboard.component';
import { EnsembleUsersComponent } from './ensemble-users/ensemble-users.component';
import { EnsembleRegisterComponent } from './ensemble-register/ensemble-register.component';
import { EnsembleSettingsComponent } from './ensemble-settings/ensemble-settings.component';

const routes: Routes = [
    { path: '', component: MyEnsemblesComponent, canActivate:[AuthGuardService] },
    { path: ':id', component: EnsembleDashboardComponent, canActivate:[AuthGuardService], children: [
        { path: '', redirectTo: 'members', pathMatch: 'full' },
        { path: 'registers', component: EnsembleRegisterComponent, canActivate:[AuthGuardService] },
        { path: 'members', component: EnsembleUsersComponent, canActivate:[AuthGuardService] },
        { path: 'settings', component: EnsembleSettingsComponent, canActivate:[AuthGuardService] },
    ] },

];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class EnsembleRoutingModule { }
