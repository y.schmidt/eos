import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnsembleSettingsComponent } from './ensemble-settings.component';

describe('EnsembleSettingsComponent', () => {
  let component: EnsembleSettingsComponent;
  let fixture: ComponentFixture<EnsembleSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnsembleSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnsembleSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
