import { Component, OnInit } from '@angular/core';
import { Ensemble } from '../../shared/models/ensemble';
import { ActivatedRoute, Router } from '@angular/router';
import { EnsembleService } from '../../shared/services/ensemble.service';
import { NotificationService } from '../../shared/services/notification.service';
import { SharedService } from '../../shared/services/shared.service';
import { ConfirmModalAdvancedComponent } from '../../shared/components/confirm-modal-advanced/confirm-modal-advanced.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'eos-ensemble-settings',
    templateUrl: './ensemble-settings.component.html',
    styleUrls: ['./ensemble-settings.component.css']
})
export class EnsembleSettingsComponent implements OnInit {

    ensemble: Ensemble;
    private modalRef: BsModalRef;

    constructor(private route: ActivatedRoute,
                private ensembleService: EnsembleService,
                private notificationService: NotificationService,
                private sharedService: SharedService,
                private modalService: BsModalService,
                private router: Router,
                private spinner: NgxSpinnerService) { }

    ngOnInit() {
        this.spinner.show();
        this.getEnsemble();
    }

    getEnsemble() {
        const id = this.route.parent.snapshot.paramMap.get('id');
        this.ensembleService.getEnsembleById(id, 'join=owner')
            .subscribe(
                ensemble => {
                    this.ensemble = ensemble;
                    this.spinner.hide();
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    updateEnsemble(){
        this.ensembleService.updateEnsemble(this.ensemble)
            .subscribe((ensemble) => {
                delete this.ensemble.owner;
                this.ensemble = ensemble;
                this.sharedService.emitChange(ensemble);
                this.notificationService.setSuccessNotification('Ensemble erfolgreich gespeichert!');
            },
                error => this.notificationService.setErrorNotification(error)
            )
    }

    openDeleteConfirmModal(){
        this.modalRef = this.modalService.show(ConfirmModalAdvancedComponent,{
            initialState: {
                object: this.ensemble,
            }
        });
        this.modalRef.content.confirmEvent.subscribe((ensemble) => {
            this.ensembleService.deleteEnsemble(ensemble.id)
                .subscribe(() => {
                    this.router.navigate(['/ensembles']);
                    this.modalRef.hide();
                });

        });
    }


}
