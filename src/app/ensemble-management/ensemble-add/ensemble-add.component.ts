import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Ensemble } from '../../shared/models/ensemble';
import { EnsembleService } from '../../shared/services/ensemble.service';
import { Organization } from '../../shared/models/organization';
import { EnsembleFormComponent } from '../ensemble-form/ensemble-form.component';
import { NotificationService } from '../../shared/services/notification.service';
import { AuthGuardService } from '../../shared/services/auth-guard.service';

@Component({
    selector: 'eos-ensemble-add',
    templateUrl: './ensemble-add.component.html',
    styleUrls: ['./ensemble-add.component.css']
})
export class EnsembleAddComponent implements OnInit {

    ensemble: Ensemble;

    @Output()
    newEnsemble: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild(EnsembleFormComponent)
    ensembleFormComp: EnsembleFormComponent;

    isAdmin: boolean = false;

    constructor(public bsModalRef: BsModalRef,
                private ensembleService: EnsembleService,
                private notificationService: NotificationService,
                private authGuardService: AuthGuardService) { }

    ngOnInit() {
        this.ensemble = new Ensemble();
        this.ensemble.organization = new Organization();
        this.isAdmin = this.authGuardService.isUserAdminInOrganization();
    }

    createEnsemble(){
        if(this.ensembleFormComp.form.form.valid){
            this.ensembleService.createEnsemble(this.ensemble)
                .subscribe((ensemble) => {
                    this.authGuardService.renewToken()
                        .subscribe(() => {
                                this.newEnsemble.emit(ensemble);
                                this.bsModalRef.hide();
                            },
                            error => this.notificationService.setErrorNotification(error));
                }, (error) => {
                    this.notificationService.setErrorNotification(error);
                });
        } else {
            this.ensembleFormComp.markAsTouched();
        }
    }


}
