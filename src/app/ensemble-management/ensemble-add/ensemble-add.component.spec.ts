import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnsembleAddComponent } from './ensemble-add.component';

describe('EnsembleAddComponent', () => {
  let component: EnsembleAddComponent;
  let fixture: ComponentFixture<EnsembleAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnsembleAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnsembleAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
