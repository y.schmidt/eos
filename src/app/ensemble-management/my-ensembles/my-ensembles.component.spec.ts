import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyEnsemblesComponent } from './my-ensembles.component';

describe('MyEnsemblesComponent', () => {
  let component: MyEnsemblesComponent;
  let fixture: ComponentFixture<MyEnsemblesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyEnsemblesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyEnsemblesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
