import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UserService } from '../../shared/services/user.service';
import { NotificationService } from '../../shared/services/notification.service';
import { EnsembleAddComponent } from '../ensemble-add/ensemble-add.component';
import { Ensemble } from '../../shared/models/ensemble';

@Component({
    selector: 'eos-my-ensembles',
    templateUrl: './my-ensembles.component.html',
    styleUrls: ['./my-ensembles.component.css']
})
export class MyEnsemblesComponent implements OnInit {

    ensembles: Ensemble[];
    modalRef: BsModalRef;

    constructor(private userService: UserService,
                private modalService: BsModalService,
                private notificationService: NotificationService) { }

    ngOnInit() {
        this.getUserEnsembles();
    }

    getUserEnsembles(){
        this.userService.userGetEnsembleWithStats(this.userService.getUserId())
            .subscribe(
                ensembles => {
                    this.ensembles = ensembles;
                },
                        error => this.notificationService.setErrorNotification(error)

            );
    }

    addEnsembleModal(){
        this.modalRef = this.modalService.show(EnsembleAddComponent);
        this.modalRef.content.newEnsemble.subscribe(() => this.getUserEnsembles());
    }
}
