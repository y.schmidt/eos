import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { Ensemble } from '../../shared/models/ensemble';
import { EnsembleUser } from '../../shared/models/ensembleUser';
import { User } from '../../shared/models/user';
import { EnsembleService } from '../../shared/services/ensemble.service';
import { NotificationService } from '../../shared/services/notification.service';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Role } from '../../shared/models/role';

@Component({
    selector: 'eos-ensemble-users',
    templateUrl: './ensemble-users.component.html',
    styleUrls: ['./ensemble-users.component.css']
})
export class EnsembleUsersComponent implements OnInit {

    ensemble: Ensemble;
    ensembleUsers: EnsembleUser[];
    organizationUsers: User[];
    searchText: string;
    roles: Role[];

    constructor(private ensembleService: EnsembleService,
                private route: ActivatedRoute,
                private notificationService: NotificationService,
                private spinner: NgxSpinnerService) { }

    ngOnInit() {
        this.spinner.show();
        this.getEnsemble();
    }

    getEnsemble() {
        const id = this.route.parent.snapshot.paramMap.get('id');
        this.ensembleService.getEnsembleById(id, 'join=owner%20organization')
            .subscribe(
                ensemble => {
                    this.ensemble = ensemble;
                    this.getUsers();
                    this.getRoles();
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    getRoles(){
        this.ensembleService.getEnsembleRoles(this.ensemble.id)
            .subscribe(roles => {
                this.roles = roles;
            },
                error => this.notificationService.setErrorNotification(error))
    }

    getUsers(){
        this.ensembleService.getUserForEnsemble(this.ensemble.id)
            .subscribe(
                ensembleUsers => this.ensembleUsers = ensembleUsers,
                error => this.notificationService.setErrorNotification(error)
            );
        this.ensembleService.getUserForOrganization(this.ensemble.id, this.ensemble.organization.id)
            .subscribe(
                organizationUsers => {
                    this.spinner.hide();
                    this.organizationUsers = organizationUsers;
                },
                        error => this.notificationService.setErrorNotification(error)
            );
    }

    assignUser(organizationUser: User){
        this.ensembleService.assignUser(this.ensemble.id, organizationUser.id)
            .subscribe((ensembleUser) => {
                    this.ensembleUsers.push(ensembleUser);
                    this.notificationService.setSuccessNotification('Benutzer wurde hinzugefügt');
                    const index = this.organizationUsers.findIndex((el) => (el === organizationUser));
                    if (index !== -1) {
                        this.organizationUsers.splice(index, 1);
                    }
                    },
                    error => this.notificationService.setErrorNotification(error)
                );
    }

    unassignUser(ensembleUser: EnsembleUser){
        this.ensembleService.unassignUser(this.ensemble.id, ensembleUser.user.id)
            .subscribe(() => {
                this.notificationService.setSuccessNotification('Benutzer entfernt');
                const index = this.ensembleUsers.findIndex((el) => (el === ensembleUser));
                console.log(index);
                if (index !== -1) {
                    this.ensembleUsers.splice(index, 1);
                }
                this.organizationUsers.push(ensembleUser.user);
                },
                    error => this.notificationService.setErrorNotification(error)
            )

    }

    changeRole(ensembleUser: EnsembleUser){
        ensembleUser.ensemble = this.ensemble;
        this.ensembleService.updateEnsembleUser(ensembleUser)
            .subscribe( (ensembleUser) =>
                    this.notificationService.setSuccessNotification('Benutzerrolle wurde geändert'),
                error => {
                this.notificationService.setErrorNotification(error);
                }
            );
    }

    isOwner(ensembleUser: EnsembleUser){
        return this.ensemble.owner.id == ensembleUser.user.id;
    }

    newAssignedUser(ensembleUser: EnsembleUser){
        this.ensembleUsers.push(ensembleUser);
        const index = this.organizationUsers.findIndex((elt) => (elt.mail === ensembleUser.user.mail));
        if (index !== -1) {
            this.organizationUsers.splice(index, 1);
        }
    }
}
