import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnsembleUsersComponent } from './ensemble-users.component';

describe('EnsembleUsersComponent', () => {
  let component: EnsembleUsersComponent;
  let fixture: ComponentFixture<EnsembleUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnsembleUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnsembleUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
