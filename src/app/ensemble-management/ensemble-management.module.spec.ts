import { EnsembleManagementModule } from './ensemble-management.module';

describe('EnsembleManagementModule', () => {
  let ensembleManagementModule: EnsembleManagementModule;

  beforeEach(() => {
    ensembleManagementModule = new EnsembleManagementModule();
  });

  it('should create an instance', () => {
    expect(ensembleManagementModule).toBeTruthy();
  });
});
