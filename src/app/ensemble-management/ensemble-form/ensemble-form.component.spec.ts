import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnsembleFormComponent } from './ensemble-form.component';

describe('EnsembleFormComponent', () => {
  let component: EnsembleFormComponent;
  let fixture: ComponentFixture<EnsembleFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnsembleFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnsembleFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
