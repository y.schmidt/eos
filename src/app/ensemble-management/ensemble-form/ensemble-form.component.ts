import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Ensemble } from '../../shared/models/ensemble';
import { UserService } from '../../shared/services/user.service';
import { NotificationService } from '../../shared/services/notification.service';
import { FormService } from '../../shared/services/form.service';
import { NgForm } from '@angular/forms';
import { Organization } from '../../shared/models/organization';

@Component({
    selector: 'eos-ensemble-form',
    templateUrl: './ensemble-form.component.html',
    styleUrls: ['./ensemble-form.component.css']
})
export class EnsembleFormComponent implements OnInit {

    @Input()
    ensemble: Ensemble;

    @ViewChild('f')
    form: NgForm;

    organizations: Organization[];
    constructor(private userService: UserService,
                private notificationService: NotificationService,
                private formService: FormService) { }

    ngOnInit() {
        this.getOrganizations();
    }

    getOrganizations(){
        this.userService.userGetAdminOrganizations(this.userService.getUserId())
            .subscribe((organizations) => {
                this.organizations = organizations;
                if(this.organizations.length == 1){
                    this.ensemble.organization = organizations[0];
                }
                },error => this.notificationService.setErrorNotification(error)
            );
    }

    markAsTouched(){
        this.formService.markFormGroupTouched(this.form);
    }

}
