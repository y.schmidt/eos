import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyEnsemblesComponent } from './my-ensembles/my-ensembles.component';
import { EnsembleRoutingModule } from './ensemble-routing.module';
import { EnsembleCardComponent } from './ensemble-card/ensemble-card.component';
import { EnsembleFormComponent } from './ensemble-form/ensemble-form.component';
import { EnsembleAddComponent } from './ensemble-add/ensemble-add.component';
import { EnsembleDashboardComponent } from './ensemble-dashboard/ensemble-dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { EnsembleUsersComponent } from './ensemble-users/ensemble-users.component';
import { EnsembleRegisterComponent } from './ensemble-register/ensemble-register.component';
import { EnsembleSettingsComponent } from './ensemble-settings/ensemble-settings.component';
import { EnsembleOfficersComponent } from './ensemble-officers/ensemble-officers.component';
import { UserDetailsComponent } from './user-details/user-details.component';

@NgModule({
    imports: [
        CommonModule,
        EnsembleRoutingModule,
        SharedModule
    ],
    declarations: [
        MyEnsemblesComponent,
        EnsembleCardComponent,
        EnsembleFormComponent,
        EnsembleAddComponent,
        EnsembleDashboardComponent,
        EnsembleUsersComponent,
        EnsembleRegisterComponent,
        EnsembleSettingsComponent,
        EnsembleOfficersComponent,
        UserDetailsComponent,
    ],
    exports: [
        EnsembleFormComponent,
    ]
})
export class EnsembleManagementModule { }
