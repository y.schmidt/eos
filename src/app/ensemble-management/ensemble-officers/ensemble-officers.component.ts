import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { Ensemble } from '../../shared/models/ensemble';
import { EnsembleService } from '../../shared/services/ensemble.service';
import { EnsembleUser } from '../../shared/models/ensembleUser';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NotificationService } from '../../shared/services/notification.service';
import { User } from '../../shared/models/user';
import { AuthGuardService } from '../../shared/services/auth-guard.service';

@Component({
    selector: 'eos-ensemble-officers',
    templateUrl: './ensemble-officers.component.html',
    styleUrls: ['./ensemble-officers.component.css']
})
export class EnsembleOfficersComponent implements OnInit {

    @Input()
    ensemble: Ensemble;

    ensembleUsers: EnsembleUser[];
    modalRef: BsModalRef;
    confirmString: string;
    user: User;
    newOwner: User;
    constructor(private ensembleService: EnsembleService,
                private modalService: BsModalService,
                private notificationService: NotificationService,
                private authService: AuthGuardService) { }

    ngOnInit() {
        this.getUsers();
        this.user = this.authService.getAuthedUser();
        this.newOwner = this.ensemble.owner;
    }

    getUsers(){
        this.ensembleService.getUserForEnsemble(this.ensemble.id)
            .subscribe((ensembleUsers) => {
                this.ensembleUsers = ensembleUsers;
            },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    changeOwner(){
        this.ensemble.owner = this.newOwner;
        this.ensembleService.changeOwner(this.ensemble)
            .subscribe(() => {
                this.notificationService.setSuccessNotification('Besitzer erfolgreich geändert!');
                this.modalRef.hide();
            },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    openConfirmModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    compareFn(c1: any, c2: any): boolean {
        return c1 && c2 ? c1.id === c2.id : c1 === c2;
    }
}
