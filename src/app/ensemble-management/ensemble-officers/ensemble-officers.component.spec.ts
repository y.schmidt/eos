import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnsembleOfficersComponent } from './ensemble-officers.component';

describe('EnsembleOfficersComponent', () => {
  let component: EnsembleOfficersComponent;
  let fixture: ComponentFixture<EnsembleOfficersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnsembleOfficersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnsembleOfficersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
