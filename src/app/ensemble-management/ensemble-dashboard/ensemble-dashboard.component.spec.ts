import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnsembleDashboardComponent } from './ensemble-dashboard.component';

describe('EnsembleDashboardComponent', () => {
  let component: EnsembleDashboardComponent;
  let fixture: ComponentFixture<EnsembleDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnsembleDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnsembleDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
