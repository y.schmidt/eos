import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../shared/services/notification.service';
import { ActivatedRoute } from '@angular/router';
import { EnsembleService } from '../../shared/services/ensemble.service';
import { Ensemble } from '../../shared/models/ensemble';
import { SharedService } from '../../shared/services/shared.service';

@Component({
    selector: 'eos-ensemble-dashboard',
    templateUrl: './ensemble-dashboard.component.html',
    styleUrls: ['./ensemble-dashboard.component.css']
})
export class EnsembleDashboardComponent implements OnInit {

    ensemble: Ensemble;

    constructor(private route: ActivatedRoute,
                private ensembleService: EnsembleService,
                private notificationService: NotificationService,
                private sharedService: SharedService) { }

    ngOnInit() {
        this.getEnsemble();
        this.sharedService.changeEmitted
            .subscribe((ensemble) => {
                this.ensemble = ensemble;
            });
    }

    getEnsemble() {
        const id = this.route.snapshot.paramMap.get('id');
        this.ensembleService.getEnsembleById(id, 'join=owner%20organization')
            .subscribe(
                ensemble => this.ensemble = ensemble,
                error => this.notificationService.setErrorNotification(error)
            );
    }
}
