import { Component, Input, OnInit } from '@angular/core';
import { Ensemble } from '../../shared/models/ensemble';
import { AuthGuardService } from '../../shared/services/auth-guard.service';

@Component({
    selector: 'eos-ensemble-card',
    templateUrl: './ensemble-card.component.html',
    styleUrls: ['./ensemble-card.component.css']
})
export class EnsembleCardComponent implements OnInit {

    @Input()
    ensemble: Ensemble;

    isAdmin: boolean = false;

    constructor(private authGuard: AuthGuardService) { }

    ngOnInit() {
        this.isAdmin = this.authGuard.hasUserPermission('ensembleAdmin', this.ensemble.id);
    }



}
