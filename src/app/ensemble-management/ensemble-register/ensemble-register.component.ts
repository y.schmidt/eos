import { Component, OnInit, ViewChild } from '@angular/core';
import { Ensemble } from '../../shared/models/ensemble';
import { RegisterService } from '../../shared/services/register.service';
import { Register } from '../../shared/models/register';
import { NotificationService } from '../../shared/services/notification.service';
import { EnsembleService } from '../../shared/services/ensemble.service';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../shared/models/user';
import { NgForm } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'eos-ensemble-register',
    templateUrl: './ensemble-register.component.html',
    styleUrls: ['./ensemble-register.component.css']
})
export class EnsembleRegisterComponent implements OnInit {

    ensemble: Ensemble;

    registerUsers: User[];

    registers: Register[];
    newRegister = new Register();

    @ViewChild('f')
    form: NgForm;
    constructor(private registerService: RegisterService,
                private route: ActivatedRoute,
                private ensembleService: EnsembleService,
                private notificationService: NotificationService,
                private spinner: NgxSpinnerService) { }

    ngOnInit() {
        this.spinner.show();
        this.getEnsemble();
    }

    getEnsemble() {
        const id = this.route.parent.snapshot.paramMap.get('id');
        this.ensembleService.getEnsembleById(id, '')
            .subscribe(
                ensemble => {
                    this.ensemble = ensemble;
                    this.getRegisters();
                    this.getRegisterUsers();
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    getRegisters(){
        this.registerService.getRegistersForEnsemble(this.ensemble.id)
            .subscribe((registers) => {
                this.registers = registers;
            });
    }

    getRegisterUsers(){
        this.registerService.getUsersForEnsembleRegisters(this.ensemble.id)
            .subscribe((registerUsers) => {
                this.registerUsers = registerUsers;
                this.spinner.hide();
            });
    }

    createRegister(){
        if(this.form.form.valid) {
            this.newRegister.ensemble = this.ensemble;
            this.newRegister.prio = this.registers.length;
            this.registerService.createRegister(this.newRegister)
                .subscribe((register) => {
                    register.userCounter = 0;
                        this.form.form.reset();
                        this.registers.push(register);
                        this.notificationService.setSuccessNotification('Register erfolgreich angelegt');
                    },
                    error => this.notificationService.setErrorNotification(error)
                );
        }
    }

    decrementRegister(register: Register){
        const lowerRegister = this.registers.find(reg => reg.prio === register.prio+1);
        lowerRegister.prio--;
        register.prio++;
        this.updateRegister(lowerRegister, false);
        this.updateRegister(register, false);
    }

    incrementRegister(register: Register){
        const lowerRegister = this.registers.find(reg => reg.prio === register.prio-1);
        lowerRegister.prio++;
        register.prio--;
        this.updateRegister(lowerRegister, false);
        this.updateRegister(register, false);
    }

    updateRegister(register: Register, notification = true){
        register.ensemble = this.ensemble;
        this.registerService.updateRegister(register)
            .subscribe((reg) => {
                register = reg;
                if(notification){
                    this.notificationService.setSuccessNotification('Register erfolgreich geändert');
                }
            },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    removeRegister(register: Register){
        register.ensemble = this.ensemble;
        this.registerService.removeRegister(register)
            .subscribe(() => {
                this.getRegisters();
                this.notificationService.setSuccessNotification('Register gelöscht');
            },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    changeUserRegister(user: User, register: Register){
       if(this.isUserInRegister(user.registers, register)){
           this.registerService.unassignUser(user.id, register.id, this.ensemble.id)
               .subscribe( () => {
                       const index = user.registers.findIndex((elt) => (elt === register));
                       if (index !== -1) {
                           user.registers.splice(index, 1);
                       }
                       register.userCounter--;
                   this.notificationService.setSuccessNotification('Benutzer erfolgreich aus Register entfernt');
               },
                   error => this.notificationService.setErrorNotification(error));
       } else {
           this.registerService.assignUser(user.id, register.id, this.ensemble.id)
               .subscribe( () => {
                       if(user.registers == undefined){
                           user.registers = [register];
                       } else {
                           user.registers.push(register);
                       }
                       register.userCounter++;
                       this.notificationService.setSuccessNotification('Benutzer erfolgreich zu Register hinzugefügt');
                   },
                   error => this.notificationService.setErrorNotification(error));
       }
    }

    isUserInRegister(userRegisters: Register[], register: Register){
        if(userRegisters == undefined){
            return false;
        } else {
            let response = false;
            userRegisters.forEach((userRegister) => {
                if(userRegister.id == register.id){
                    response = true;
                }
            });
            return response;
        }
    }

}
