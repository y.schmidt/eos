import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnsembleRegisterComponent } from './ensemble-register.component';

describe('EnsembleRegisterComponent', () => {
  let component: EnsembleRegisterComponent;
  let fixture: ComponentFixture<EnsembleRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnsembleRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnsembleRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
