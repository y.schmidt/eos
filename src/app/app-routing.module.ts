import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { ImpressumComponent } from './app-default/impressum/impressum.component';
import { FaqComponent } from './app-default/faq/faq.component';
import { DatenschutzComponent } from './app-default/datenschutz/datenschutz.component';

const routes: Routes = [
    { path: 'impressum', component: ImpressumComponent },
    { path: 'datenschutz', component: DatenschutzComponent },
    { path: 'faq', component: FaqComponent },
    { path: 'start', loadChildren: () => import('src/app/start/start.module').then(m => m.StartModule)},
    { path: 'ensembles', loadChildren: () => import('src/app/ensemble-management/ensemble-management.module').then(m => m.EnsembleManagementModule)},
    { path: 'projects', loadChildren: () => import('src/app/project-management/project-management.module').then(m => m.ProjectManagementModule)},
    { path: 'calendar', loadChildren: () => import('src/app/calendar/eos-calendar.module').then(m => m.EosCalendarModule)},
    { path: 'organizations', loadChildren: () => import('src/app/organization-management/organization-management.module').then(m => m.OrganizationManagementModule)},
    { path: 'users', loadChildren: () => import('src/app/user-management/user-management.module').then(m => m.UserManagementModule)},
    { path: 'start', loadChildren: () => import('src/app/start/start.module').then(m => m.StartModule)},
    { path: 'communication', loadChildren: () => import('src/app/communication/communication.module').then(m => m.CommunicationModule)},
    { path: 'dashboard', loadChildren: () => import('src/app/dashboard/dashboard.module').then(m => m.DashboardModule)},
    { path: 'start-tour', loadChildren: () => import('src/app/start-tour/start-tour.module').then(m => m.StartTourModule)},
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            onSameUrlNavigation: 'reload',
            preloadingStrategy: PreloadAllModules
        })
    ],
    exports: [ RouterModule ]
})

export class AppRoutingModule {}
