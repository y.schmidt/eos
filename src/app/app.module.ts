import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { HeaderNavComponent } from './app-default/header-nav/header-nav.component';
import { HeaderNavProfileComponent } from './app-default/header-nav-profile/header-nav-profile.component';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import { DataTableComponent } from './data-table/data-table.component';
import { FaqComponent } from './app-default/faq/faq.component';
import { ImpressumComponent } from './app-default/impressum/impressum.component';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { deLocale } from 'ngx-bootstrap/locale';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatenschutzComponent } from './app-default/datenschutz/datenschutz.component';
import { SidebarNavComponent } from './app-default/sidebar-nav/sidebar-nav.component';

registerLocaleData(localeDe, 'de');
defineLocale('de', deLocale);

@NgModule({
    declarations: [
        AppComponent,
        DataTableComponent,
        HeaderNavComponent,
        HeaderNavProfileComponent,
        SidebarNavComponent,
        ImpressumComponent,
        FaqComponent,
        DatenschutzComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        SharedModule,
        AppRoutingModule,
    ],
    providers: [ { provide: LOCALE_ID, useValue: 'de' } ],
    bootstrap: [AppComponent]
})
export class AppModule { }
