import { Project } from './shared/models/project';
import { Eventgroup } from './shared/models/eventgroup';
import { Event } from './shared/models/event';

export const colorSchemes = {
    announcements: {
        domain: ['#29d487', '#f4686c', '#ffc424']
    }
};

export const mockupProject: Project = {
    address: undefined,
    color: undefined,
    enableInformation: false,
    ensembles: [],
    eventgroups: [],
    events: [],
    range: [],
    registers: [],
    autoReminderSent: false,
    autoReminder: false,
    reminderDaysBeforeDeadline: 7,
    id: 'example-project',
    name: 'onesemble Beispielprojekt',
    description: '<p>Das Projekt zeigt dir die Funktionalitäten von onsemble-Projekten</p>',
    status: 'active',
    deadline: new Date('2020-10-11T07:00:15.000Z'),
    start: new Date('2020-05-11T07:00:15.000Z'),
    end: new Date('2020-11-29T23:00:00.000Z'),
    announcements: []
}


export const mockupEventgroup: Eventgroup[] = [
    {
        actions: undefined,
        allDay: false,
        color: undefined,
        // @ts-ignore
        cssClass: '',
        draggable: false,
        events: [],
        meta: undefined,
        range: [],
        resizable: {},
        id: 'example-eventgroup',
        title: 'Probenwochenende',
        location: 'Ochsenhausen',
        description: '',
        start: new Date('2020-01-23T22:00:00.000Z'),
        end: new Date('2020-01-25T22:00:00.000Z'),
        deadline: new Date('2020-01-24T11:00:00.000Z'),
        autoReminderSent: false,
        autoReminder: false,
        enableInformation: true,
        roomWish: true,
        eatingHabits: true,
        allergy: true,
        reminderDaysBeforeDeadline: 0,
        project: mockupProject,
        address: null,
        announcements: [],
        registers: []
    }
];


export const mockupEvents: Event[] = [
    {
        id: 'example-event',
        title: 'Konzert',
        start: new Date(),
        end: new Date('2020-03-27T21:00:00.000Z'),
        location: 'Kirche',
        type: 'concert',
        serial: false,
        description: '',
        deadline: null,
        autoReminderSent: false,
        autoReminder: false,
        project: mockupProject,
        address: null,
        successor: null,
        announcements: [],
        registers: [
            // @ts-ignore
            {
                id: 'bass',
                name: 'Bass',
                prio: 3,
                shortcut: 'B'
            },
            // @ts-ignore
            {
                id: 'sopran',
                name: 'Sopran',
                prio: 0,
                shortcut: 'S'
            },
            // @ts-ignore
            {
                id: 'tenor',
                name: 'Tenor',
                prio: 2,
                shortcut: 'T'
            },
            // @ts-ignore
            {
                id: 'alt',
                name: 'Alt',
                prio: 1,
                shortcut: 'A'
            }
        ]
    },
    {
        id: 'example-event',
        title: 'regelmäßige Probe',
        start: new Date('2020-01-07T18:00:00.000Z'),
        end: new Date('2020-01-07T20:30:00.000Z'),
        location: 'A 144',
        type: 'rehearsal',
        serial: true,
        description: '',
        deadline: null,
        autoReminderSent: false,
        autoReminder: false,
        project: mockupProject,
        address: null,
        // @ts-ignore
        successor: {
            id: '99ec86b9-c2b3-4d9f-8d5e-f4c84baeb105',
            title: 'erste Probe',
            start: new Date('2020-01-07T18:00:00.000Z'),
            end: new Date('2020-01-07T20:30:00.000Z'),
            location: 'A 144',
            type: 'rehearsal',
            serial: true,
            description: '',
            deadline: null,
            autoReminderSent: false,
            autoReminder: false
        },
        announcements: [],
        registers: [
            // @ts-ignore
            {
                id: 'bass',
                name: 'Bass',
                prio: 3,
                shortcut: 'B'
            },
            // @ts-ignore
            {
                id: 'sopran',
                name: 'Sopran',
                prio: 0,
                shortcut: 'S'
            },
            // @ts-ignore
            {
                id: 'tenor',
                name: 'Tenor',
                prio: 2,
                shortcut: 'T'
            },
            // @ts-ignore
            {
                id: 'alt',
                name: 'Alt',
                prio: 1,
                shortcut: 'A'
            }
        ]
    },
    {
        id: 'example-event',
        title: 'Generalprobe',
        start: new Date('2020-03-26T18:00:00.000Z'),
        end: new Date('2020-03-26T20:30:00.000Z'),
        location: 'Konzertsaal',
        type: 'rehearsal',
        serial: false,
        description: '',
        deadline: null,
        autoReminderSent: false,
        autoReminder: false,
        project: mockupProject,
        address: null,
        announcements: [],
        registers: [
            // @ts-ignore
            {
                id: 'bass',
                name: 'Bass',
                prio: 3,
                shortcut: 'B'
            },
            // @ts-ignore
            {
                id: 'sopran',
                name: 'Sopran',
                prio: 0,
                shortcut: 'S'
            },
            // @ts-ignore
            {
                id: 'tenor',
                name: 'Tenor',
                prio: 2,
                shortcut: 'T'
            },
            // @ts-ignore
            {
                id: 'alt',
                name: 'Alt',
                prio: 1,
                shortcut: 'A'
            }
        ]
    },
    {
        id: 'example-event',
        title: 'regelmäßige Probe',
        start: new Date('2020-01-07T18:00:00.000Z'),
        end: new Date('2020-01-07T20:30:00.000Z'),
        location: 'A 144',
        type: 'rehearsal',
        serial: true,
        description: '',
        deadline: null,
        autoReminderSent: false,
        autoReminder: false,
        project: mockupProject,
        address: null,
        // @ts-ignore
        successor: {
            id: '99ec86b9-c2b3-4d9f-8d5e-f4c84baeb105',
            title: 'erste Probe',
            start: new Date('2020-01-07T18:00:00.000Z'),
            end: new Date('2020-01-07T20:30:00.000Z'),
            location: 'A 144',
            type: 'rehearsal',
            serial: true,
            description: '',
            deadline: null,
            autoReminderSent: false,
            autoReminder: false
        },
        announcements: [],
        registers: [
            // @ts-ignore
            {
                id: 'bass',
                name: 'Bass',
                prio: 3,
                shortcut: 'B'
            },
            // @ts-ignore
            {
                id: 'sopran',
                name: 'Sopran',
                prio: 0,
                shortcut: 'S'
            },
            // @ts-ignore
            {
                id: 'tenor',
                name: 'Tenor',
                prio: 2,
                shortcut: 'T'
            },
            // @ts-ignore
            {
                id: 'alt',
                name: 'Alt',
                prio: 1,
                shortcut: 'A'
            }
        ]
    },
];

export const mockupPastEvents: Event[] = [
    {
        id: 'example-event',
        title: 'regelmäßige Probe',
        start: new Date('2020-01-07T18:00:00.000Z'),
        end: new Date('2020-01-07T20:30:00.000Z'),
        location: 'A 144',
        type: 'rehearsal',
        serial: true,
        description: '',
        deadline: null,
        autoReminderSent: false,
        autoReminder: false,
        project: mockupProject,
        address: null,
        // @ts-ignore
        successor: {
            id: '99ec86b9-c2b3-4d9f-8d5e-f4c84baeb105',
            title: 'erste Probe',
            start: new Date('2020-01-07T18:00:00.000Z'),
            end: new Date('2020-01-07T20:30:00.000Z'),
            location: 'A 144',
            type: 'rehearsal',
            serial: true,
            description: '',
            deadline: null,
            autoReminderSent: false,
            autoReminder: false
        },
        announcements: [],
        registers: [
            // @ts-ignore
            {
                id: 'bass',
                name: 'Bass',
                prio: 3,
                shortcut: 'B'
            },
            // @ts-ignore
            {
                id: 'sopran',
                name: 'Sopran',
                prio: 0,
                shortcut: 'S'
            },
            // @ts-ignore
            {
                id: 'tenor',
                name: 'Tenor',
                prio: 2,
                shortcut: 'T'
            },
            // @ts-ignore
            {
                id: 'alt',
                name: 'Alt',
                prio: 1,
                shortcut: 'A'
            }
        ]
    }
];