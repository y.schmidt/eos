import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { SidebarService } from '../../shared/services/sidebar.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../shared/services/user.service';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
  selector: 'eos-header-nav',
  templateUrl: './header-nav.component.html',
  styleUrls: ['./header-nav.component.css'],
})
export class HeaderNavComponent implements OnInit {

  @ViewChild('feedbackPopover')
  feedbackPopover;

  feedbackText: string = '';

  constructor(private authGuard: AuthGuardService,
              private sidebarService: SidebarService,
              private userService: UserService,
              private notificationService: NotificationService,
              private router: Router) { }

  ngOnInit() {
  }

  logout(){
    this.authGuard.logout();
  }

  toggleSidebar(){
    this.sidebarService.toggle();
  }

  sendFeedback(){
    const feedbackObj = {
      url: this.router.url,
      feedback: this.feedbackText
    };
    this.userService.sendFeedback(feedbackObj)
        .subscribe(() => {
          this.feedbackText = '';
          this.feedbackPopover.hide();
          this.notificationService.setSuccessNotification('Feedback gesendet');
        }, error => this.notificationService.setErrorNotification(error));
  }

}
