import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderNavProfileComponent } from './header-nav-profile.component';

describe('HeaderNavProfileComponent', () => {
  let component: HeaderNavProfileComponent;
  let fixture: ComponentFixture<HeaderNavProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderNavProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderNavProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
