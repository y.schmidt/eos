import { Component, OnInit } from '@angular/core';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { User } from '../../shared/models/user';

@Component({
  selector: 'app-header-nav-profile',
  templateUrl: './header-nav-profile.component.html',
  styleUrls: ['./header-nav-profile.component.css']
})
export class HeaderNavProfileComponent implements OnInit {

    public user: User;
    constructor(private authGuardService: AuthGuardService) {}

    ngOnInit() {
        this.authGuardService.authedUser.subscribe(
            (userData) => {
                this.user = userData;
            }
        )
    }


}
