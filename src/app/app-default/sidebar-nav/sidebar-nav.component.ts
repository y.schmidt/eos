import { Component, HostListener, OnInit, Renderer2, ViewChild } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { User } from '../../shared/models/user';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { environment } from '../../../environments/environment';
import { SidebarService } from '../../shared/services/sidebar.service';
import { UserService } from '../../shared/services/user.service';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
  selector: 'eos-sidebar-nav',
  templateUrl: './sidebar-nav.component.html',
  styleUrls: ['./sidebar-nav.component.css'],
  animations: [
    trigger('slide', [
      state('up', style({ height: 0 })),
      state('down', style({ height: '*' })),
      transition('up <=> down', animate(200))
    ])
  ]
})
export class SidebarNavComponent implements OnInit {

  public version;
  public user: User;
  profilePicture: string;
  menus = [];

  constructor(public sidebarService: SidebarService,
              private userService: UserService,
              private notificationService: NotificationService,
              private authGuardService: AuthGuardService,
              private renderer: Renderer2) {
  }

  ngOnInit() {
    this.profilePicture = './assets/img/user.jpg';
    this.menus = this.sidebarService.getMenuList();
    this.version = environment.version;
    this.getUser();
    this.userService.profilePictureEmitter
        .subscribe(() => {
          this.getProfilePicture();
        });
    this.checkSize();
  }

  getUser(){
    this.authGuardService.authedUser
        .subscribe((user) => {
              if(user.id != undefined){
                this.user = user;
                this.getProfilePicture();
              }
            }
        );
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.setMiniSidebar(event.currentTarget.innerWidth);
  }

  checkSize(){
    this.setMiniSidebar(window.innerWidth);
  }

  setMiniSidebar(width: number){
    if(width < environment.breakpoints.large + 1){
      if(width < environment.breakpoints.small + 1){
        this.renderer.removeClass(document.body, 'mini-sidebar');
      } else {
        this.sidebarService.setMenusInactive();
        this.renderer.addClass(document.body, 'mini-sidebar');
      }
    } else {
      this.renderer.removeClass(document.body, 'mini-sidebar');
    }
  }

  getProfilePicture(){
    this.userService.userGetProfilePicture(this.user.id)
        .subscribe((pic) => {
          this.profilePicture = 'data:image/webp;base64,' + pic.picture;
        }, err => this.notificationService.setErrorNotification(err));
  }

  toggleSubmenu(currentMenu) {
    if (currentMenu.type === 'dropdown') {
      this.menus.forEach(element => {
        if (element === currentMenu) {
          currentMenu.active = !currentMenu.active;
        } else {
          element.active = false;
        }
      });
    } else {
      this.sidebarService.setMenusInactive();
    }
    this.sidebarService.menuClick(currentMenu);
  }

  submenuClick(currentMenu){
    this.sidebarService.submenuClick(currentMenu);
  }

  linkClick(){
    this.sidebarService.toggle();
  }

  getState(currentMenu) {
    if (currentMenu.active) {
      return 'down';
    } else {
      return 'up';
    }
  }

}
