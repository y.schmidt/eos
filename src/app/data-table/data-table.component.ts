import { Component, OnInit } from '@angular/core';
import {UserDataTable} from './userDataTable';
import {USERS} from './mock-users';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit {

  searchText: string;
  users: UserDataTable[];
  constructor() { }

  ngOnInit() {
    this.users = USERS;
  }

}
