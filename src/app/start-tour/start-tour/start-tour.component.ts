import { Component, OnInit } from '@angular/core';
import { User } from '../../shared/models/user';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { StartTourService } from '../../shared/services/start-tour.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'eos-start-tour',
  templateUrl: './start-tour.component.html',
  styleUrls: ['./start-tour.component.css']
})
export class StartTourComponent implements OnInit {

  user: User

  constructor(private authGuard: AuthGuardService,
              private cookieService: CookieService,
              private startTourService: StartTourService) { }

  ngOnInit(): void {
    this.cookieService.delete('os_organisatorTourPayload', '/start-tour');
    this.user = this.authGuard.getAuthedUser();
  }

  startMemberTour(){
    this.startTourService.startMemberTour();
  }
}
