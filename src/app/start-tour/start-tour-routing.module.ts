import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../shared/services/auth-guard.service';
import { StartTourComponent } from './start-tour/start-tour.component';
import { MemberTourComponent } from './member-tour/member-tour.component';
import { OrganisatorTourComponent } from './organisator-tour/organisator-tour.component';
import { OrganisatorTourProjectComponent } from './organisator-tour-project/organisator-tour-project.component';


const routes: Routes = [
  { path: '', component: StartTourComponent, canActivate:[AuthGuardService] },
  { path: 'member', component: MemberTourComponent, canActivate:[AuthGuardService] },
  { path: 'organisator', component: OrganisatorTourComponent, canActivate:[AuthGuardService] },
  { path: 'organisator/project', component: OrganisatorTourProjectComponent, canActivate:[AuthGuardService] }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class StartTourRoutingModule { }
