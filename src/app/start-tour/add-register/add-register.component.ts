import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Ensemble } from '../../shared/models/ensemble';
import { Register } from '../../shared/models/register';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'eos-add-register',
  templateUrl: './add-register.component.html',
  styleUrls: ['./add-register.component.css']
})
export class AddRegisterComponent implements OnInit {

  @Input()
  ensemble: Ensemble;

  newRegister = new Register();

  @ViewChild('f')
  form: NgForm;

  @Output()
  registerEmitter: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
    if(!this.ensemble.registers){
      this.ensemble.registers = [];
    }
  }

  createRegister(){
    if(this.form.form.valid) {
      this.newRegister.prio = this.ensemble.registers.length;
      this.ensemble.registers.push(this.newRegister);
      this.registerEmitter.emit(this.ensemble);
      this.newRegister = new Register();
      this.form.form.reset();
    }
  }

  decrementRegister(register: Register){
    const lowerRegister = this.ensemble.registers.find(reg => reg.prio === register.prio+1);
    lowerRegister.prio--;
    register.prio++;
  }

  incrementRegister(register: Register){
    const lowerRegister = this.ensemble.registers.find(reg => reg.prio === register.prio-1);
    lowerRegister.prio++;
    register.prio--;
  }

  removeRegister(register: Register){
    const index = this.ensemble.registers.findIndex((el) => (el === register));
    if (index !== -1) {
      this.ensemble.registers.splice(index, 1);
    }
  }
}
