import { Component, OnInit, ViewChild } from '@angular/core';
import { Project } from '../../shared/models/project';
import { Address } from '../../shared/models/address';
import { ProjectFormComponent } from '../../project-management/project-form/project-form.component';
import { ProjectService } from '../../shared/services/project.service';
import { StartTourService } from '../../shared/services/start-tour.service';

@Component({
  selector: 'eos-organisator-tour-project',
  templateUrl: './organisator-tour-project.component.html',
  styleUrls: ['./organisator-tour-project.component.css']
})
export class OrganisatorTourProjectComponent implements OnInit {

  project: Project = new Project();
  loading: boolean = false;

  @ViewChild(ProjectFormComponent)
  private formComp: ProjectFormComponent;

  constructor(private projectService: ProjectService,
              private startTourService: StartTourService) { }

  ngOnInit(): void {
    this.project.address = new Address();
  }

  createProject(){
    if(this.formComp.form.form.valid){
      this.loading = true;
      this.project.start = this.project.range[0];
      this.project.end = this.project.range[1];
      this.projectService.createProject(this.project, this.project.ensembles[0].id)
          .subscribe((project) => {
            this.loading = false;
            this.startTourService.setOrganisatorPayload(project, 'project');
            this.startTourService.startOrganisatorTour();
          });
    } else {
      this.formComp.markAsTouched();
    }
  }
}
