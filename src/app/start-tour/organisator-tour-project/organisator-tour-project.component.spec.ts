import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisatorTourProjectComponent } from './organisator-tour-project.component';

describe('OrganisatorTourProjectComponent', () => {
  let component: OrganisatorTourProjectComponent;
  let fixture: ComponentFixture<OrganisatorTourProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisatorTourProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisatorTourProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
