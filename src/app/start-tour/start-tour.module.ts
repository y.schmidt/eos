import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StartTourComponent } from './start-tour/start-tour.component';
import { StartTourRoutingModule } from './start-tour-routing.module';
import { OrganisatorTourComponent } from './organisator-tour/organisator-tour.component';
import { MemberTourComponent } from './member-tour/member-tour.component';
import { OrganizationManagementModule } from '../organization-management/organization-management.module';
import { SharedModule } from '../shared/shared.module';
import { EnsembleManagementModule } from '../ensemble-management/ensemble-management.module';
import { AddRegisterComponent } from './add-register/add-register.component';
import { OrganisatorTourProjectComponent } from './organisator-tour-project/organisator-tour-project.component';
import { ProjectManagementModule } from '../project-management/project-management.module';


@NgModule({
    declarations: [StartTourComponent, OrganisatorTourComponent, MemberTourComponent, AddRegisterComponent, OrganisatorTourProjectComponent],
    imports: [
        CommonModule,
        StartTourRoutingModule,
        OrganizationManagementModule,
        SharedModule,
        EnsembleManagementModule,
        ProjectManagementModule
    ]
})
export class StartTourModule {
}
