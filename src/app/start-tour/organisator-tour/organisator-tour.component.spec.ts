import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisatorTourComponent } from './organisator-tour.component';

describe('OrganisatorTourComponent', () => {
  let component: OrganisatorTourComponent;
  let fixture: ComponentFixture<OrganisatorTourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisatorTourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisatorTourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
