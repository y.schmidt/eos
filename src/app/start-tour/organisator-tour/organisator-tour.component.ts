import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Organization } from '../../shared/models/organization';
import { Ensemble } from '../../shared/models/ensemble';
import { OrganizationFormComponent } from '../../organization-management/organization-form/organization-form.component';
import { Invite } from '../../shared/models/invite';
import { User } from '../../shared/models/user';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { NgForm } from '@angular/forms';
import { UiWizardService } from '../../shared/services/ui-wizard.service';
import { Router } from '@angular/router';
import { StartTourService } from '../../shared/services/start-tour.service';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
  selector: 'eos-organisator-tour',
  templateUrl: './organisator-tour.component.html',
  styleUrls: ['./organisator-tour.component.css']
})
export class OrganisatorTourComponent implements OnInit, AfterViewInit {

  organization: Organization = new Organization();
  ensemble: Ensemble = new Ensemble();
  invites: Invite[] = [];

  newMember: User = new User();

  @ViewChild(OrganizationFormComponent)
  orgaFormComp: OrganizationFormComponent;

  @ViewChild('inviteForm')
  inviteForm: NgForm;

  @ViewChild('ensembleForm')
  ensembleForm: NgForm;

  orgaFormCompleted: boolean = false;
  orgaFormError: boolean = false;
  selectedIndex: number = 0;

  constructor(private authGuard: AuthGuardService,
              private router: Router,
              private cookieService: CookieService,
              private startTourService: StartTourService,
              private uiWizardService: UiWizardService,
              private notificationService: NotificationService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.ensemble.organization = this.organization;
    this.getCookie();
  }

  ngAfterViewInit (): void {
    this.orgaFormComp.form.statusChanges.subscribe(
        result => {
          this.orgaFormCompleted = result == 'VALID';
        }
    );
  }

  selectChanged(stepperData){
    if(stepperData){
      this.selectedIndex = stepperData.selectedIndex;
    }
    this.saveAsCookie();
    this.orgaFormError = !this.orgaFormCompleted;
  }

  saveAsCookie(){
    const payload = {
      organization: this.organization,
      ensemble: this.ensemble,
      invites: this.invites,
      stepIndex: this.selectedIndex
    };
    console.log(payload);
    this.cookieService.set('os_organizatorTourPayload', JSON.stringify(payload), 1, '/')
  }

  getCookie(){
    if(this.cookieService.check('os_organizatorTourPayload')){
      const payload = JSON.parse(this.cookieService.get('os_organizatorTourPayload'));
      if(payload){
        this.selectedIndex = payload.stepIndex;
        this.organization = payload.organization;
        this.ensemble = payload.ensemble;
        this.invites = payload.invites;
      }
    }
  }

  registerChanged(ensemble){
    this.ensemble = ensemble;
    this.saveAsCookie()
  }

  inviteUser(){
    const invite = new Invite(
        this.organization,
        this.newMember.mail,
        this.authGuard.getAuthedUser(),
        this.ensemble);
    this.invites.push(invite);
    this.newMember = new User();
    this.inviteForm.form.reset();
    this.saveAsCookie();
  }

  deleteInvite(invite: Invite){
    const index = this.invites.findIndex(remInvite => remInvite.id == invite.id);
    if (index !== -1) {
      this.invites.splice(index, 1);
      this.saveAsCookie();
    }
  }

  submit(){
    this.spinner.show();
    const data = {
      organization: this.organization,
      ensemble: this.ensemble,
      invites: this.invites
    };
    this.uiWizardService.organisatorStart(data)
        .subscribe((newData) => {
          this.ensemble.id = newData.ensemble.id;
          this.organization.id = newData.organization.id;
          this.saveAsCookie();
          this.authGuard.renewToken()
              .subscribe(() => {
                this.spinner.hide();
                this.router.navigateByUrl('/start-tour/organisator/project')
              }, error => {
                this.notificationService.setErrorNotification(error);
                this.spinner.hide();
              });
        }, error => {
          this.notificationService.setErrorNotification(error);
          this.spinner.hide();
        });
  }

}
