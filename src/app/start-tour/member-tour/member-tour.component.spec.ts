import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberTourComponent } from './member-tour.component';

describe('MemberTourComponent', () => {
  let component: MemberTourComponent;
  let fixture: ComponentFixture<MemberTourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberTourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberTourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
