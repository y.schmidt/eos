import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventGroupCardComponent } from './event-group-card/event-group-card.component';
import { EventFormComponent } from './event-form/event-form.component';
import { EventCardComponent } from './event-card/event-card.component';
import { EventGroupComponent } from './event-group/event-group.component';
import { EventModalComponent } from './event-modal/event-modal.component';
import { ProjectComponent } from './project/project.component';
import { MyProjectsComponent } from './my-projects/my-projects.component';
import { ProjectRoutingModule } from './project-routing.module';
import { SharedModule } from '../shared/shared.module';
import { EventGroupModalComponent } from './event-group-modal/event-group-modal.component';
import { EventGroupFormComponent } from './event-group-form/event-group-form.component';
import { EventListModalComponent } from './event-list-modal/event-list-modal.component';
import { AnnouncementButtonComponent } from './announcement-button/announcement-button.component';
import { EventSerialModalComponent } from './event-serial-modal/event-serial-modal.component';
import { EventComponent } from './event/event.component';
import { EventPastCardComponent } from './event-past-card/event-past-card.component';
import { ProjectStatisticsComponent } from './project-statistics/project-statistics.component';
import { ProjectAddComponent } from './project-add/project-add.component';
import { ProjectFormComponent } from './project-form/project-form.component';
import { TargetGroupComponent } from './target-group/target-group.component';
import { EventDeleteConfirmModalComponent } from './event-delete-confirm-modal/event-delete-confirm-modal.component';
import { ProjectCardComponent } from './project-card/project-card.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { ICalModalComponent } from './i-cal-modal/i-cal-modal.component';
import { AnnouncementConfigurationComponent } from './announcement-configuration/announcement-configuration.component';
import { AnnouncementModalComponent } from './announcement-modal/announcement-modal.component';
import { EventgroupStatisticsComponent } from './eventgroup-statistics/eventgroup-statistics.component';
import { EventOverviewTableComponent } from './event-overview-table/event-overview-table.component';
import { MyPastProjectsComponent } from './my-past-projects/my-past-projects.component';
import { EventTypeOverviewComponent } from './event-type-overview/event-type-overview.component';
import { SendReminderModalComponent } from './send-reminder-modal/send-reminder-modal.component';
import { CommunicationModule } from '../communication/communication.module';
import { ProjectSettingsComponent } from './project-settings/project-settings.component';
import { EventGroupSettingsComponent } from './event-group-settings/event-group-settings.component';
import { EventTypeByUserOverviewComponent } from './event-type-by-user-overview/event-type-by-user-overview.component';
import { AnnouncementListComponent } from './announcement-list/announcement-list.component';
import { AnnouncementUserPercentComponent } from './announcement-user-percent/announcement-user-percent.component';
import { AnnouncementStatsComponent } from './announcement-stats/announcement-stats.component';
import { AnnouncementRegistersComponent } from './announcement-registers/announcement-registers.component';

@NgModule({
    imports: [
        CommonModule,
        ProjectRoutingModule,
        SharedModule,
        CommunicationModule,
    ],
    declarations: [
        EventCardComponent,
        EventGroupCardComponent,
        ProjectComponent,
        EventGroupComponent,
        EventFormComponent,
        EventModalComponent,
        MyProjectsComponent,
        EventGroupModalComponent,
        EventGroupFormComponent,
        EventListModalComponent,
        AnnouncementButtonComponent,
        EventSerialModalComponent,
        EventComponent,
        EventPastCardComponent,
        ProjectStatisticsComponent,
        ProjectAddComponent,
        ProjectFormComponent,
        TargetGroupComponent,
        EventDeleteConfirmModalComponent,
        ProjectCardComponent,
        EventDetailsComponent,
        ICalModalComponent,
        AnnouncementConfigurationComponent,
        AnnouncementModalComponent,
        EventgroupStatisticsComponent,
        EventOverviewTableComponent,
        MyPastProjectsComponent,
        EventTypeOverviewComponent,
        SendReminderModalComponent,
        ProjectSettingsComponent,
        EventGroupSettingsComponent,
        EventTypeByUserOverviewComponent,
        AnnouncementListComponent,
        AnnouncementUserPercentComponent,
        AnnouncementStatsComponent,
        AnnouncementRegistersComponent,
    ],
    exports: [
        EventComponent,
        EventModalComponent,
        ProjectFormComponent
    ]
})
export class ProjectManagementModule { }
