import { Component, Input, OnInit } from '@angular/core';
import { Event } from '../../shared/models/event';
import { User } from '../../shared/models/user';
import { Attendance } from '../../shared/models/attendance';
import { Announcement } from '../../shared/models/announcement';

@Component({
  selector: 'eos-event-overview-table',
  templateUrl: './event-overview-table.component.html',
  styleUrls: ['./event-overview-table.component.css']
})
export class EventOverviewTableComponent implements OnInit {


  @Input()
  mode: 'attendance' | 'announcement';

  @Input()
  events: Event[];

  @Input()
  users: User[];

  constructor() { }

  ngOnInit() {  }

  checkUser(subject: Attendance[] | Announcement[], eventId: string){
    if(subject.length <= 0){
      return null;
    } else {
      let check = null;
      // @ts-ignore
      subject.forEach((subject) => {
        if(subject.event != null && subject.event.id == eventId){
          check = subject.status;
        }
      });
      return check;
    }
  }

  getClass(type: string){
    switch(type){
      case 'rehearsal': return 'badge-success';
      case 'concert': return 'badge-warning';
      case 'travel': return 'badge-info';
      case 'breakfast': return 'badge-secondary';
      case 'lunch': return 'badge-secondary';
      case 'dinner': return 'badge-secondary';
      case 'other': return 'badge-info';
      case 'overnight': return 'badge-info';
      default: return 'badge-info';
    }
  }
}
