import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventOverviewTableComponent } from './event-overview-table.component';

describe('EventOverviewTableComponent', () => {
  let component: EventOverviewTableComponent;
  let fixture: ComponentFixture<EventOverviewTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventOverviewTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventOverviewTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
