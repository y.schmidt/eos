import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Event } from '../../shared/models/event';
import { ActivatedRoute } from '@angular/router';
import { Project } from '../../shared/models/project';
import { Eventgroup } from '../../shared/models/eventgroup';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { EventModalComponent } from '../event-modal/event-modal.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
    selector: 'eos-event-card',
    templateUrl: './event-card.component.html',
    styleUrls: ['./event-card.component.css']
})
export class EventCardComponent implements OnInit {

    @Input()
    events: Event[];

    @Input()
    project: Project;

    @Input()
    eventgroup: Eventgroup;

    @Output()
    updateMultipleEventsEmitter: EventEmitter<any> = new EventEmitter();

    dateMode: 'project' | 'eventgroup' = 'project';
    modalRef: BsModalRef;
    ensembleId: string;
    isProjectEditor: boolean = false;
    currentDate: Date;
    constructor(private route: ActivatedRoute,
                private authGuard: AuthGuardService,
                private modalService: BsModalService) { }

    ngOnInit() {
        this.currentDate = new Date();
        this.ensembleId = this.route.snapshot.paramMap.get('ensembleId');
        this.isProjectEditor = this.authGuard.hasUserPermission('projectEditor', this.ensembleId);
        if(this.eventgroup){
            this.dateMode = 'eventgroup';
        }
    }

    openNewEventModal() {
        this.modalRef = this.modalService.show(EventModalComponent, {
            initialState: {
                mode: 'add',
                ensembleId: this.ensembleId,
                event: new Event(),
                project: this.project,
                dateMode: this.dateMode
            }
        });
        this.modalRef.content.event.eventgroup = this.eventgroup;
        this.modalRef.content.newEvent.subscribe((newevent) => {
            this.events.push(newevent);
        });
    }

    newEvent(event: Event){
        this.events.push(event);
    }

    deleteEvent(event: Event){
        const index = this.events.findIndex((elt) => (elt === event));
        if (index !== -1) {
            this.events.splice(index, 1);
        }
    }

    updateMultipleEvents(){
        this.updateMultipleEventsEmitter.emit();
    }
}
