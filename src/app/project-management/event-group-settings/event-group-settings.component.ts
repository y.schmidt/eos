import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { AnnouncementService } from '../../shared/services/announcement.service';
import { NotificationService } from '../../shared/services/notification.service';
import { EventgroupService } from '../../shared/services/eventgroup.service';
import { Eventgroup } from '../../shared/models/eventgroup';
import { ConfirmModalAdvancedComponent } from '../../shared/components/confirm-modal-advanced/confirm-modal-advanced.component';
import { ProjectService } from '../../shared/services/project.service';
import { Project } from '../../shared/models/project';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'eos-event-group-settings',
    templateUrl: './event-group-settings.component.html',
    styleUrls: ['./event-group-settings.component.css']
})
export class EventGroupSettingsComponent implements OnInit {

    eventgroup: Eventgroup;
    project: Project;
    ensembleId: string;
    projectId: string;
    private modalRef: BsModalRef;
    isProjectEditor: boolean = false;
    isProjectManager: boolean = false;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private modalService: BsModalService,
                private eventgroupService: EventgroupService,
                private projectService: ProjectService,
                public authGuard: AuthGuardService,
                private announcementService: AnnouncementService,
                private notificationService: NotificationService,
                private spinner: NgxSpinnerService) { }

    ngOnInit(): void {
        this.spinner.show();
        this.ensembleId = this.route.snapshot.paramMap.get('ensembleId');
        this.projectId = this.route.snapshot.paramMap.get('projectId');
        this.isProjectEditor = this.authGuard.hasUserPermission('projectEditor', this.ensembleId);
        this.isProjectManager = this.authGuard.hasUserPermission('projectManager', this.ensembleId);
        this.getEventgroup();
        this.getProject()
    }

    getEventgroup() {
        const eventgroupId = this.route.snapshot.paramMap.get('eventgroupId');
        this.eventgroupService.getEventgroupById(this.ensembleId, eventgroupId)
            .subscribe((eventgroup) => {
                    this.eventgroup = eventgroup;
                    this.spinner.hide();
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    getProject(){
        this.projectService.getProjectById(this.ensembleId, this.projectId)
            .subscribe((project) => {
                    this.project = project;
                },error => this.notificationService.setErrorNotification(error)
            );
    }

    saveEventgroup() {
        this.eventgroup.start = this.eventgroup.range[0];
        this.eventgroup.end = this.eventgroup.range[1];
        this.eventgroupService.updateEventgroup(this.ensembleId, this.eventgroup)
            .subscribe(eventgroup => {
                    this.notificationService.setSuccessNotification('Probenphase erfolgreich gespeichert');
                    this.eventgroup = eventgroup;
                },
                error => this.notificationService.setErrorNotification(error));
    }

    openAnnouncementDeleteConfirmModal(){
        this.modalRef = this.modalService.show(ConfirmModalAdvancedComponent,{
            initialState: {
                object: this.eventgroup,
                heading: 'Alle Anmeldungen löschen?',
                confirmationInfo: 'Sollen wirklich alle Anmeldungen zur Probenphasen und allen Terminen gelöscht werden?'
            }
        });
        this.modalRef.content.confirmEvent.subscribe(() => {
            this.announcementService.deleteAllAnnouncementsForEventgroup(this.ensembleId, this.eventgroup.id)
                .subscribe(() => {
                        this.modalRef.hide();
                        this.notificationService.setSuccessNotification('Alle Anmeldungen wurden gelöscht');
                    },
                    error => this.notificationService.setErrorNotification(error));
        });
    }

    openEventgroupDeleteConfirmModal(){
        this.modalRef = this.modalService.show(ConfirmModalAdvancedComponent,{
            initialState: {
                object: this.eventgroup,
            }
        });
        this.modalRef.content.confirmEvent.subscribe(() => {
            this.eventgroupService.deleteEventgroup(this.ensembleId, this.eventgroup.id)
                .subscribe(() => {
                        this.modalRef.hide();
                        this.router.navigate([`/projects/${this.projectId}/ensembles/${this.ensembleId}`]);
                        this.notificationService.setSuccessNotification('Probenphase erfolgreich gelöscht');
                    },
                    error => this.notificationService.setErrorNotification(error)
                );
        });
    }

}
