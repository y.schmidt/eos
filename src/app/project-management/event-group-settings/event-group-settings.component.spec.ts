import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventGroupSettingsComponent } from './event-group-settings.component';

describe('EventGroupSettingsComponent', () => {
  let component: EventGroupSettingsComponent;
  let fixture: ComponentFixture<EventGroupSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventGroupSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventGroupSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
