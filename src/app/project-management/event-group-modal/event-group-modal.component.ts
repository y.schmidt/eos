import {
    Component,
    EventEmitter,
    OnInit,
    Output,
    ViewChild
} from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Eventgroup } from '../../shared/models/eventgroup';
import { EventGroupFormComponent } from '../event-group-form/event-group-form.component';
import { EventgroupService } from '../../shared/services/eventgroup.service';
import { NotificationService } from '../../shared/services/notification.service';
import { Project } from '../../shared/models/project';
import { Register } from '../../shared/models/register';

@Component({
  selector: 'eos-event-group-modal',
  templateUrl: './event-group-modal.component.html',
  styleUrls: ['./event-group-modal.component.css']
})
export class EventGroupModalComponent implements OnInit {

    eventgroup: Eventgroup;
    ensembleId: string;
    project: Project;

    mode: string;
    @Output()
    newEventgroup: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    eventgroupUpdated: EventEmitter<any> = new EventEmitter<any>();


    @ViewChild(EventGroupFormComponent)
    private formComp: EventGroupFormComponent;

    constructor(public bsModalRef: BsModalRef,
                private eventgroupService: EventgroupService,
                private notificationService: NotificationService) { }

    ngOnInit() {
        if(this.mode == 'duplicate'){
            delete this.eventgroup.address.id;
            this.eventgroup.title += ' (copy)';
        }
        if(this.mode == 'add'){
            this.eventgroup.project = this.project;
        }
    }

    submit(){
        if(this.formComp.form.form.valid){
            this.eventgroup.start = this.eventgroup.range[0];
            this.eventgroup.end = this.eventgroup.range[1];
            if(this.mode == 'add' || this.mode == 'duplicate') {
                this.eventgroupService.createEventgroup(this.ensembleId, this.project.id, this.eventgroup)
                    .subscribe((eventgroup) => {
                        this.newEventgroup.emit(eventgroup);
                        this.bsModalRef.hide();
                        this.notificationService.setSuccessNotification('Probenphase erfolgreich angelegt!')
                    }, (error) => {
                        this.notificationService.setErrorNotification(error);
                    });
            } else if(this.mode == 'edit') {
                this.eventgroupService.updateEventgroup(this.ensembleId, this.eventgroup)
                    .subscribe((eventgroup) => {
                        // new eventgroup has no announcement
                        eventgroup.announcements = this.eventgroup.announcements;
                        this.eventgroupUpdated.emit(eventgroup);
                        this.bsModalRef.hide();
                        this.notificationService.setSuccessNotification('Probenphase erfolgreich gespeichert!')
                    }, (error) => {
                        this.notificationService.setErrorNotification(error);
                    });
            }
        } else {
            this.formComp.markAsTouched();
        }
    }

    changeTarget(registers: Register[]){
        this.eventgroup.registers = registers.slice();
    }
}
