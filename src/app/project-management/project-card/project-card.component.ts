import { Component, Input, OnInit } from '@angular/core';
import { Project } from '../../shared/models/project';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { PictureUploadComponent } from '../../shared/components/picture-upload/picture-upload.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ProjectService } from '../../shared/services/project.service';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
    selector: 'eos-project-card',
    templateUrl: './project-card.component.html',
    styleUrls: ['./project-card.component.css']
})
export class ProjectCardComponent implements OnInit {

    @Input()
    project: Project;
    modalRef: BsModalRef;
    projectPicture: string;

    isProjectEditor: boolean = false;

    constructor(private authGuard: AuthGuardService,
                private modalService: BsModalService,
                private projectService: ProjectService,
                private notificationService: NotificationService) { }

    ngOnInit() {
        this.isProjectEditor = this.authGuard.hasUserPermission('projectEditor', this.project.ensembles[0].id);
        this.projectPicture = './assets/img/projectDefault.jpg';
        this.getProjectPicture();
    }

    getProjectPicture(){
        this.projectService.getProjectPicture(this.project.ensembles[0].id, this.project.id)
            .subscribe((pic) => {
                this.projectPicture = 'data:image/webp;base64,' + pic.picture;
            }, err => this.notificationService.setErrorNotification(err));
    }

    openFileUploadModal(){
        this.modalRef = this.modalService.show(PictureUploadComponent, {
            initialState: {
                mode: 'projectPicture',
                aspectRatio: 2 / 1.2,
                resizeWidth: 600,
                resizeHeight: 360,
                target: this.project
            }
        });
        this.modalRef.content.uploadEmitter.subscribe(() => {
            this.getProjectPicture();
        })
    }

}
