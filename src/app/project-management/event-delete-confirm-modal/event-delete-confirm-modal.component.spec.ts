import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDeleteConfirmModalComponent } from './event-delete-confirm-modal.component';

describe('EventDeleteConfirmModalComponent', () => {
  let component: EventDeleteConfirmModalComponent;
  let fixture: ComponentFixture<EventDeleteConfirmModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDeleteConfirmModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDeleteConfirmModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
