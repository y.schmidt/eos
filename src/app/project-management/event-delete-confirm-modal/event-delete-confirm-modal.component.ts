import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
    selector: 'eos-event-delete-confirm-modal',
    templateUrl: './event-delete-confirm-modal.component.html',
    styleUrls: ['./event-delete-confirm-modal.component.css']
})
export class EventDeleteConfirmModalComponent implements OnInit {

    event: Event;
    deleteSerial: boolean = false;
    @Output()
    confirmEvent: EventEmitter<any> = new EventEmitter<any>();

    constructor(public confirmModal: BsModalRef) {}

    ngOnInit() {
    }

    confirm(){
        this.confirmEvent.emit({event: this.event, deleteSerial: this.deleteSerial});
    }

}
