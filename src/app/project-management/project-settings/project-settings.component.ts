import { Component, OnInit } from '@angular/core';
import { Project } from '../../shared/models/project';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from '../../shared/services/project.service';
import { NotificationService } from '../../shared/services/notification.service';
import { AnnouncementService } from '../../shared/services/announcement.service';
import { ConfirmModalAdvancedComponent } from '../../shared/components/confirm-modal-advanced/confirm-modal-advanced.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'eos-project-settings',
    templateUrl: './project-settings.component.html',
    styleUrls: ['./project-settings.component.css']
})
export class ProjectSettingsComponent implements OnInit {

    project: Project;
    ensembleId: string;
    private modalRef: BsModalRef;
    isProjectEditor: boolean = false;
    isProjectManager: boolean = false;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private modalService: BsModalService,
                private projectService: ProjectService,
                public authGuard: AuthGuardService,
                private announcementService: AnnouncementService,
                private notificationService: NotificationService,
                private spinner: NgxSpinnerService) { }

    ngOnInit(): void {
        this.spinner.show();
        this.ensembleId = this.route.snapshot.paramMap.get('ensembleId');
        this.isProjectEditor = this.authGuard.hasUserPermission('projectEditor', this.ensembleId);
        this.isProjectManager = this.authGuard.hasUserPermission('projectManager', this.ensembleId);
        this.getProject();
    }

    getProject() {
        const projectId = this.route.snapshot.paramMap.get('projectId');
        this.projectService.getProjectById(this.ensembleId, projectId)
            .subscribe(project => {
                    this.project = project;
                    this.spinner.hide();
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    saveProject() {
        this.project.start = this.project.range[0];
        this.project.end = this.project.range[1];
        this.checkProjectTimeline();
        this.projectService.updateProject(this.project, this.ensembleId)
            .subscribe(project => {
                    this.notificationService.setSuccessNotification('Projekt erfolgreich gespeichert');
                    this.project = project;
                },
                error => this.notificationService.setErrorNotification(error));
    }

    checkProjectTimeline(){
        if(this.project.end < new Date()){
            this.project.status = 'past';
        } else if(this.project.status == 'past'){
            this.project.status = 'draft';
        }
    }

    openAnnouncementDeleteConfirmModal(){
        this.modalRef = this.modalService.show(ConfirmModalAdvancedComponent,{
            initialState: {
                object: this.project,
                heading: 'Alle Anmeldungen löschen?',
                confirmationInfo: 'Sollen wirklich alle Anmeldungen zum Projekt, Probenphasen und allen Terminen gelöscht werden?'
            }
        });
        this.modalRef.content.confirmEvent.subscribe(() => {
            this.announcementService.deleteAllAnnouncementsForProject(this.ensembleId, this.project.id)
                .subscribe(() => {
                        this.modalRef.hide();
                        this.notificationService.setSuccessNotification('Alle Anmeldungen wurden gelöscht');
                    },
                    error => this.notificationService.setErrorNotification(error));
        });
    }

    openProjectDeleteConfirmModal(){
        this.modalRef = this.modalService.show(ConfirmModalAdvancedComponent,{
            initialState: {
                object: this.project,
            }
        });
        this.modalRef.content.confirmEvent.subscribe(() => {
            this.projectService.deleteProject(this.ensembleId, this.project.id)
                .subscribe(() => {
                        this.modalRef.hide();
                        this.router.navigate(['/projects']);
                        this.notificationService.setSuccessNotification('Projekt erfolgreich gelöscht');
                    },
                    error => this.notificationService.setErrorNotification(error)
                );
        });
    }
}
