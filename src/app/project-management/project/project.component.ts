import { Component, OnInit } from '@angular/core';
import { Project } from '../../shared/models/project';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from '../../shared/services/project.service';
import { NotificationService } from '../../shared/services/notification.service';
import { EventService } from '../../shared/services/event.service';
import { Announcement } from '../../shared/models/announcement';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { Event } from '../../shared/models/event';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import saveAs from 'file-saver';
import { EventgroupService } from '../../shared/services/eventgroup.service';
import { Eventgroup } from '../../shared/models/eventgroup';
import { NgxSpinnerService } from 'ngx-spinner';
import { ICalModalComponent } from '../i-cal-modal/i-cal-modal.component';
import { SendReminderModalComponent } from '../send-reminder-modal/send-reminder-modal.component';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { mockupEventgroup, mockupEvents, mockupPastEvents, mockupProject } from '../../globals';

@Component({
    selector: 'app-project',
    templateUrl: './project.component.html',
    styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

    project: Project;
    ensembleId: string;
    pastEvents: Event[];
    events: Event[];
    eventgroups: Eventgroup[];
    modalRef: BsModalRef;
    isProjectEditor: boolean = false;
    isProjectManager: boolean = false;

    constructor(private route: ActivatedRoute,
                private projectService: ProjectService,
                private notificationService: NotificationService,
                private eventService: EventService,
                private eventgroupService: EventgroupService,
                public authGuard: AuthGuardService,
                private router: Router,
                private localeService: BsLocaleService,
                private modalService: BsModalService,
                private spinner: NgxSpinnerService) { }

    ngOnInit() {
        this.spinner.show();
        this.localeService.use('de');
        this.ensembleId = this.route.snapshot.paramMap.get('ensembleId');
        this.isProjectEditor = this.authGuard.hasUserPermission('projectEditor', this.ensembleId);
        this.isProjectManager = this.authGuard.hasUserPermission('projectManager', this.ensembleId);
        this.getProject();
    }

    getProject() {
        const projectId = this.route.snapshot.paramMap.get('projectId');
        if(projectId == 'example'){
            this.project = mockupProject;
            this.eventgroups = mockupEventgroup;
            this.events = mockupEvents;
            this.pastEvents = mockupPastEvents;
            this.spinner.hide();
        } else {
            this.projectService.getProjectById(this.ensembleId, projectId)
                .subscribe(project => {
                        this.project = project;
                        this.getEvents();
                        this.getPastEvents();
                        this.getEventgroups();
                    },
                    error => this.notificationService.setErrorNotification(error)
                );
        }

    }

    getEvents(){
        this.eventService.getEventsForProject(this.ensembleId, this.project.id, '/future')
            .subscribe((events) => {
                    this.events = events;
                    this.spinner.hide();
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    getPastEvents(){
        this.eventService.getEventsForProject(this.ensembleId, this.project.id, '/past')
            .subscribe((events) => {
                    this.pastEvents = events;
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    getEventgroups(){
        this.eventgroupService.getEventgroupsForProject(this.ensembleId, this.project.id)
            .subscribe((eventgroups) => {
                this.eventgroups = eventgroups;
            },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    announcementChanged(){
        this.getProject();
    }

    getPdf() {
        this.projectService.getProjectPdfExport(this.ensembleId, this.project.id)
            .subscribe(res => {
                saveAs(res, this.project.name + ' - Probenplan.pdf');
            });
    }

    updateMultipleEvents(){
        this.getEvents();
    }

    sendReminder(){
        this.modalRef = this.modalService.show(SendReminderModalComponent, {
            initialState: {
                ensembleId: this.ensembleId,
                target: this.project,
                mode: 'project'
            }
        });
    }

    getIcal() {
        this.modalRef = this.modalService.show(ICalModalComponent,{
            initialState: {
                ensembleId: this.ensembleId,
                projectId: this.project.id
            }
        });
    }
}