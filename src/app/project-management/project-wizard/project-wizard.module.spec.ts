import { ProjectWizardModule } from './project-wizard.module';

describe('ProjectWizardModule', () => {
  let projectWizardModule: ProjectWizardModule;

  beforeEach(() => {
    projectWizardModule = new ProjectWizardModule();
  });

  it('should create an instance', () => {
    expect(projectWizardModule).toBeTruthy();
  });
});
