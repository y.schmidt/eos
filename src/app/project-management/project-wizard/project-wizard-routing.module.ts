import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectWizardStartComponent } from './project-wizard-start/project-wizard-start.component';


const routes: Routes = [
  { path: '', component: ProjectWizardStartComponent },
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class ProjectWizardRoutingModule { }
