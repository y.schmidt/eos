import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectWizardStartComponent } from './project-wizard-start/project-wizard-start.component';
import { ProjectWizardRoutingModule } from './project-wizard-routing.module';

@NgModule({
    imports: [
        CommonModule,
        ProjectWizardRoutingModule
    ],
    declarations: [ProjectWizardStartComponent]
})
export class ProjectWizardModule { }
