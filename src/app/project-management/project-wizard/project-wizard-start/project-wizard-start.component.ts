import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'eos-project-wizard-start',
  templateUrl: './project-wizard-start.component.html',
  styleUrls: ['./project-wizard-start.component.css']
})
export class ProjectWizardStartComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
