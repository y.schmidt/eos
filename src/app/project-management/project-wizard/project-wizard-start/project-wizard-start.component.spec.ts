import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectWizardStartComponent } from './project-wizard-start.component';

describe('ProjectWizardStartComponent', () => {
  let component: ProjectWizardStartComponent;
  let fixture: ComponentFixture<ProjectWizardStartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectWizardStartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectWizardStartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
