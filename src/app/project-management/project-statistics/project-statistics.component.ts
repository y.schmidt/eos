import { Component, OnInit } from '@angular/core';
import { StatisticsService } from '../../shared/services/statistics.service';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from '../../shared/services/notification.service';
import { Register } from '../../shared/models/register';
import { Event } from '../../shared/models/event';
import { AnnouncementService } from '../../shared/services/announcement.service';
import { AttendanceService } from '../../shared/services/attendance.service';
import { EventService } from '../../shared/services/event.service';
import { User } from '../../shared/models/user';
import { colorSchemes } from '../../globals';
import { ProjectService } from '../../shared/services/project.service';
import { Project } from '../../shared/models/project';


@Component({
    selector: 'eos-project-statistics',
    templateUrl: './project-statistics.component.html',
    styleUrls: ['./project-statistics.component.css']
})
export class ProjectStatisticsComponent implements OnInit {

    ensembleId: string;
    projectId: string;
    project: Project;
    events: Event[];
    attendances: User[];
    eventAnnouncements: User[];

    constructor(private statsService: StatisticsService,
                private route: ActivatedRoute,
                private projectService: ProjectService,
                private announcementService: AnnouncementService,
                private attendanceService: AttendanceService,
                private eventService: EventService,
                private notificationService: NotificationService) { }

    ngOnInit() {
        this.ensembleId = this.route.snapshot.paramMap.get('ensembleId');
        this.projectId = this.route.snapshot.paramMap.get('projectId');
        this.getProject();
        this.getEvents();
        this.getEventAnnouncements();
        this.getEventAttendance();
    }

    getProject(){
        this.projectService.getProjectById(this.ensembleId, this.projectId)
            .subscribe((project) => {
                this.project = project
            })
    }

    getEvents(){
        this.eventService.getEventsForProject(this.ensembleId, this.projectId, '?sort=ASC')
            .subscribe((events) => {
                this.events = events;
            }, error => this.notificationService.setErrorNotification(error));
    }

    getEventAnnouncements(){
        this.announcementService.getAnnouncementsForProjectEvents(this.ensembleId, this.projectId)
            .subscribe((userAnnouncements) => {
                this.eventAnnouncements = userAnnouncements;
            }, error => this.notificationService.setErrorNotification(error));
    }

    getEventAttendance(){
        this.attendanceService.getAttendanceForProjectEvents(this.ensembleId, this.projectId)
            .subscribe((userAttendance) => {
                this.attendances = userAttendance;
            }, error => this.notificationService.setErrorNotification(error));
    }
}
