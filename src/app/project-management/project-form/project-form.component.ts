import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Project } from '../../shared/models/project';
import { NgForm } from '@angular/forms';
import { FormService } from '../../shared/services/form.service';
import { UserService } from '../../shared/services/user.service';
import { EnsembleUser } from '../../shared/models/ensembleUser';
import { NotificationService } from '../../shared/services/notification.service';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';

@Component({
    selector: 'eos-project-form',
    templateUrl: './project-form.component.html',
    styleUrls: ['./project-form.component.css']
})
export class ProjectFormComponent implements OnInit {

    @Input()
    project: Project;

    @Input()
    showEnsembleChoice: boolean = true;

    @ViewChild('f')
    form: NgForm;

    noEnsembleLock: boolean = true;
    ensembleUsers: EnsembleUser[] = [];

    constructor(private formService: FormService,
                private userService: UserService,
                private notificationService: NotificationService,
                private localeService: BsLocaleService) { }

    ngOnInit() {
        this.localeService.use('de');
        this.getUserEnsembles();
    }

    markAsTouched(){
        this.formService.markFormGroupTouched(this.form);
    }

    getUserEnsembles(){
        this.userService.userGetEnsembles(this.userService.getUserId())
            .subscribe((ensembleUsers) => {
                    ensembleUsers.forEach((ensemble) => {
                        if(ensemble.role.name == 'administrator'){
                            this.ensembleUsers.push(ensemble);
                            this.noEnsembleLock = false;
                        }
                    });
                    if(this.ensembleUsers.length == 1) {
                        this.project.ensembles.push(this.ensembleUsers[0].ensemble);
                    }
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    rangeChanged(range){
        if(range && !this.project.deadline){
            this.project.deadline = range[0];
        }
        if(range && this.project.deadline && this.project.deadline < range[0]){
            this.project.deadline = range[0];
        }
    }

    checkTime(date: Date){
        if(date.getHours() == 0){
            date.setHours(12);
        }
    }

    descriptionChanged(data: string){
        this.project.description = data;
    }
}
