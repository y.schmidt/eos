import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectComponent } from './project/project.component';
import { MyProjectsComponent } from './my-projects/my-projects.component';
import { EventGroupComponent } from './event-group/event-group.component';
import { AuthGuardService } from '../shared/services/auth-guard.service';
import { ProjectStatisticsComponent } from './project-statistics/project-statistics.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { EventgroupStatisticsComponent } from './eventgroup-statistics/eventgroup-statistics.component';
import { MyPastProjectsComponent } from './my-past-projects/my-past-projects.component';
import { ProjectSettingsComponent } from './project-settings/project-settings.component';
import { EventGroupSettingsComponent } from './event-group-settings/event-group-settings.component';

const routes: Routes = [
    { path: '', component: MyProjectsComponent, canActivate:[AuthGuardService] },
    { path: 'past', component: MyPastProjectsComponent, canActivate:[AuthGuardService] },
    { path: ':projectId', component: ProjectComponent, canActivate:[AuthGuardService] },
    { path: ':projectId/eventgroups/:eventgroupId', component: EventGroupComponent, canActivate:[AuthGuardService] },
    { path: ':projectId/ensembles/:ensembleId', component: ProjectComponent, canActivate:[AuthGuardService] },
    { path: ':projectId/ensembles/:ensembleId/settings', component: ProjectSettingsComponent, canActivate:[AuthGuardService] },
    { path: ':projectId/ensembles/:ensembleId/stats', component: ProjectStatisticsComponent, canActivate:[AuthGuardService] },
    { path: ':projectId/ensembles/:ensembleId/eventgroups/:eventgroupId', component: EventGroupComponent, canActivate:[AuthGuardService] },
    { path: ':projectId/ensembles/:ensembleId/eventgroups/:eventgroupId/settings', component: EventGroupSettingsComponent, canActivate:[AuthGuardService] },
    { path: ':projectId/ensembles/:ensembleId/eventgroups/:eventgroupId/stats', component: EventgroupStatisticsComponent, canActivate:[AuthGuardService] },
    { path: ':projectId/ensembles/:ensembleId/events/:eventId', component: EventDetailsComponent, canActivate:[AuthGuardService] },
    { path: 'projectwizard', loadChildren: () => import('src/app/project-management/project-wizard/project-wizard.module').then(m => m.ProjectWizardModule)},
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class ProjectRoutingModule { }
