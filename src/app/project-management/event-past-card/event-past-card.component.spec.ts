import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventPastCardComponent } from './event-past-card.component';

describe('EventPastCardComponent', () => {
  let component: EventPastCardComponent;
  let fixture: ComponentFixture<EventPastCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventPastCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventPastCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
