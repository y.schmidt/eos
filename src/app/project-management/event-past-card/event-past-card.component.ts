import { Component, Input, OnInit } from '@angular/core';
import { Event } from '../../shared/models/event';
import { Project } from '../../shared/models/project';
import { Eventgroup } from '../../shared/models/eventgroup';
import { ActivatedRoute } from '@angular/router';
import { AuthGuardService } from '../../shared/services/auth-guard.service';

@Component({
  selector: 'eos-event-past-card',
  templateUrl: './event-past-card.component.html',
  styleUrls: ['./event-past-card.component.css']
})
export class EventPastCardComponent implements OnInit {

    @Input()
    events: Event[];

    @Input()
    project: Project;

    @Input()
    eventgroup: Eventgroup;

    ensembleId: string;

    constructor(private route: ActivatedRoute) { }

    ngOnInit() {
        this.ensembleId = this.route.snapshot.paramMap.get('ensembleId');
    }

    deleteEvent(event: Event){
        const index = this.events.findIndex((elt) => (elt === event));
        if (index !== -1) {
            this.events.splice(index, 1);
        }
    }

}
