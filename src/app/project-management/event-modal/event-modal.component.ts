import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Event } from '../../shared/models/event';
import { EventService } from '../../shared/services/event.service';
import { NotificationService } from '../../shared/services/notification.service';
import { EventFormComponent } from '../event-form/event-form.component';
import { Project } from '../../shared/models/project';
import { Register } from '../../shared/models/register';

@Component({
    selector: 'app-event-modal',
    templateUrl: './event-modal.component.html',
    styleUrls: ['./event-modal.component.css']
})
export class EventModalComponent implements OnInit {

    @Input()
    event: Event;

    @Output()
    newEvent: EventEmitter<any> = new EventEmitter();

    @Output()
    updateMultipleEventsEmitter: EventEmitter<any> = new EventEmitter();

    @Output()
    updateEventEmitter: EventEmitter<any> = new EventEmitter();

    @ViewChild(EventFormComponent)
    eventFormComp: EventFormComponent;

    dateMode: 'project' | 'eventgroup' = 'project';
    mode: string;
    ensembleId: string;
    project: Project;


    constructor(public bsModalRef: BsModalRef,
                private eventService: EventService,
                private notificationService: NotificationService) { }

    ngOnInit() {
        if(this.mode == 'duplicate'){
            if(this.event.address){
                delete this.event.address.id;
            }
            this.event.title += ' (copy)';
        }
        if(this.mode != 'duplicate'){
            this.event.project = this.project;
        }
    }

    submit() {
        if(this.eventFormComp.form.valid){
            if(this.event.start.getHours() == 0 && this.event.end.getHours() < 12){
                this.event.start.setHours(12);
            }
            this.event.project = this.project;
            if(this.mode == 'add' && this.event.serial){
                this.eventService.createSerialEvent(this.event, this.ensembleId)
                    .subscribe((events) => {
                        events.forEach((event) => {
                            this.newEvent.emit(event);
                        });
                        this.bsModalRef.hide();
                        this.notificationService.setSuccessNotification('Serienevent erfolgreich angelegt')
                        },
                        error => this.notificationService.setErrorNotification(error)
                    );
            } else if(this.mode == 'add' || this.mode == 'duplicate') {
                this.eventService.createEvent(this.event, this.ensembleId)
                    .subscribe((event) => {
                            this.event = event;
                            this.newEvent.emit(event);
                            this.bsModalRef.hide();
                            this.notificationService.setSuccessNotification('Event erfolgreich angelegt')
                        },
                        error => this.notificationService.setErrorNotification(error)
                    );
            } else if (this.mode == 'edit') {
                this.eventService.updateEvent(this.event, this.ensembleId)
                    .subscribe((event) => {
                        if(this.event.editRepeat){
                            this.updateMultipleEventsEmitter.emit(event);
                        } else {
                            this.updateEventEmitter.emit(event);
                        }
                        this.event = event;
                        this.bsModalRef.hide();
                        this.notificationService.setSuccessNotification('Event erfolgreich gespeichert')
                    }, error => this.notificationService.setErrorNotification(error));
            }
        } else {
            this.eventFormComp.markAsTouched();
        }
    }

    changeTarget(registers: Register[]){
        this.event.registers = registers.slice();
    }
}
