import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Event } from '../../shared/models/event';
import { Eventgroup } from '../../shared/models/eventgroup';
import { Project } from '../../shared/models/project';
import { ProjectService } from '../../shared/services/project.service';
import { Register } from '../../shared/models/register';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
    selector: 'eos-target-group',
    templateUrl: './target-group.component.html',
    styleUrls: ['./target-group.component.css']
})
export class TargetGroupComponent implements OnInit {

    @Input()
    event: Event;

    @Input()
    eventgroup: Eventgroup;

    @Input()
    project: Project;

    @Input()
    ensembleId: string;

    @Input()
    target: string;

    @Input()
    mode: string = 'edit';

    @Output()
    changeTargetEmitter: EventEmitter<any> = new EventEmitter<any>();

    subject: Event | Eventgroup | Project;

    registers: Register[];

    constructor(private projectService: ProjectService,
                private notificationService: NotificationService) { }

    ngOnInit() {
        this.getRegister();
        switch(this.target) {
            case 'event':
                this.subject = this.event;
                break;
            case 'eventgroup':
                this.subject = this.eventgroup;
                break;
            case 'project':
                this.subject = this.project;
                break;
        }
    }

    getRegister(){
        this.projectService.getProjectRegisters(this.ensembleId, this.project.id)
            .subscribe((registers) => {
                this.registers = registers;
                this.setRegister();
            }, error => this.notificationService.setErrorNotification(error));
    }

    setRegister(){
        if(this.mode == 'add'){
            this.subject.registers = this.registers;
        }
    }

    changeRegister(register: Register){
        if(this.subject.registers) {
            const index = this.subject.registers.findIndex((elt) => (elt.id === register.id));
            if (index !== -1) {
                this.subject.registers.splice(index, 1);
            } else {
                this.subject.registers.push(register);
            }
        } else {
            this.subject.registers = [];
            this.subject.registers.push(register);
        }
        this.changeTargetEmitter.emit(this.subject.registers);
    }

    isRegisterInSubject(register: Register){
        if(this.subject.registers == undefined){
            return false;
        } else {
            let response = false;
            this.subject.registers.forEach((subjectRegister) => {
                if(subjectRegister.id == register.id){
                    response = true;
                }
            });
            return response;
        }
    }

}
