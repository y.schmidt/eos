import { Component, Input, OnInit } from '@angular/core';
import { Eventgroup } from '../../shared/models/eventgroup';
import { Project } from '../../shared/models/project';
import { User } from '../../shared/models/user';
import { StatisticsService } from '../../shared/services/statistics.service';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
  selector: 'eos-announcement-user-percent',
  templateUrl: './announcement-user-percent.component.html',
  styleUrls: ['./announcement-user-percent.component.css']
})
export class AnnouncementUserPercentComponent implements OnInit {

  @Input()
  mode: 'project' | 'eventgroup';

  @Input()
  eventgroup: Eventgroup;

  @Input()
  project: Project;

  @Input()
  ensembleId: string;

  userAnnouncementStats: User[];
  constructor(private statsService: StatisticsService,
              private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.getUserAnnouncementStats();
  }

  getUserAnnouncementStats(){
    if(this.mode == 'eventgroup'){
      this.statsService.getEventgroupUserAnnouncementStats(this.ensembleId, this.eventgroup.id)
          .subscribe((stats) => {
            this.userAnnouncementStats = stats;
          }, error => this.notificationService.setErrorNotification(error));
    } else {
      this.statsService.getProjectUserAnnouncementStats(this.ensembleId, this.project.id)
          .subscribe((stats) => {
            this.userAnnouncementStats = stats;
          }, error => this.notificationService.setErrorNotification(error));
    }

  }

  getProgressbar(user){
    const announce = this.getPercent(user.eventCounter, user.announce) + '%';
    const cancel = this.getPercent(user.eventCounter, user.cancel) + '%';
    const warning = this.getPercent(user.eventCounter, user.eventCounter-user.announce-user.cancel) + '%';
    return [
      {value: user.announce, type: 'success', label: announce },
      {value: user.cancel, type: 'danger', label: cancel},
      {value: user.eventCounter-user.announce-user.cancel, type: 'warning', label: warning}
    ];
  }

  getEventCounter(user){
    return user.eventCounter;
  }

  getPercent(sum: number, part: number): number {
    return Math.round(part / sum * 100);
  }

}
