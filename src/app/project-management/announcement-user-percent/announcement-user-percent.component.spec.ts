import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnouncementUserPercentComponent } from './announcement-user-percent.component';

describe('AnnouncementUserPercentComponent', () => {
  let component: AnnouncementUserPercentComponent;
  let fixture: ComponentFixture<AnnouncementUserPercentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnouncementUserPercentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnouncementUserPercentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
