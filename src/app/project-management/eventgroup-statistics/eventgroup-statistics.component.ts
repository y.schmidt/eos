import { Component, OnInit } from '@angular/core';
import { Register } from '../../shared/models/register';
import { Event } from '../../shared/models/event';
import { StatisticsService } from '../../shared/services/statistics.service';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from '../../shared/services/notification.service';
import { AnnouncementService } from '../../shared/services/announcement.service';
import { User } from '../../shared/models/user';
import { EventgroupService } from '../../shared/services/eventgroup.service';
import { Eventgroup } from '../../shared/models/eventgroup';
import { AttendanceService } from '../../shared/services/attendance.service';
import { EventService } from '../../shared/services/event.service';
import saveAs from 'file-saver';
import { colorSchemes } from '../../globals';


@Component({
  selector: 'eos-eventgroup-statistics',
  templateUrl: './eventgroup-statistics.component.html',
  styleUrls: ['./eventgroup-statistics.component.css']
})
export class EventgroupStatisticsComponent implements OnInit {

  ensembleId: string;
  eventgroupId: string;
  projectId: string;
  eventgroup: Eventgroup;
  events: Event[];
  eventAnnouncements: User[];
  announcements: User[];
  attendance: User[];

  constructor(private statsService: StatisticsService,
              private route: ActivatedRoute,
              private eventgroupService: EventgroupService,
              private attendanceService: AttendanceService,
              private eventService: EventService,
              private announcementService: AnnouncementService,
              private notificationService: NotificationService) { }

  ngOnInit() {
    this.ensembleId = this.route.snapshot.paramMap.get('ensembleId');
    this.projectId = this.route.snapshot.paramMap.get('projectId');
    this.eventgroupId = this.route.snapshot.paramMap.get('eventgroupId');
    this.getEventgroup();
    this.getEvents();
    this.getEventAnnouncements();
    this.getEventAttendance();
  }

  getEventgroup(){
    this.eventgroupService.getEventgroupById(this.ensembleId, this.eventgroupId)
        .subscribe((eventgroup) => {
          this.eventgroup = eventgroup;
        });
  }

  getEvents(){
    this.eventService.getEventsForEventgroup(this.ensembleId, this.eventgroupId, '?sort=ASC')
        .subscribe((events) => {
          this.events = events;
        }, error => this.notificationService.setErrorNotification(error));
  }

  getEventAnnouncements(){
    this.announcementService.getAnnouncementsForEventgroupEvents(this.ensembleId, this.eventgroupId)
        .subscribe((announcements) => {
          this.eventAnnouncements = announcements;
        }, error => this.notificationService.setErrorNotification(error));
  }

  getEventAttendance(){
    this.attendanceService.getAttendanceForEventgroupEvents(this.ensembleId, this.eventgroupId)
        .subscribe((userAttendance) => {
          this.attendance = userAttendance;
        }, error => this.notificationService.setErrorNotification(error));
  }
}
