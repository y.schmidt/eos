import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventgroupStatisticsComponent } from './eventgroup-statistics.component';

describe('EventgroupStatisticsComponent', () => {
  let component: EventgroupStatisticsComponent;
  let fixture: ComponentFixture<EventgroupStatisticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventgroupStatisticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventgroupStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
