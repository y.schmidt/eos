import { Component, Input, OnInit } from '@angular/core';
import { StatisticsService } from '../../shared/services/statistics.service';
import { ProjectService } from '../../shared/services/project.service';
import { NotificationService } from '../../shared/services/notification.service';
import saveAs from 'file-saver';

@Component({
  selector: 'eos-event-type-by-user-overview',
  templateUrl: './event-type-by-user-overview.component.html',
  styleUrls: ['./event-type-by-user-overview.component.css']
})
export class EventTypeByUserOverviewComponent implements OnInit {

  @Input()
  ensembleId: string;

  @Input()
  targetId: string;

  @Input()
  mode: 'eventgroup' | 'project';

  users:[];

  constructor(private statsService: StatisticsService,
              private projectService: ProjectService,
              private notificationService: NotificationService) { }

  ngOnInit() {
    this.getStats();
  }

  getStats(){
    this.statsService.countAnnouncementsFromUserByType(this.ensembleId, this.mode, this.targetId)
        .subscribe((users) => {
          this.users = users;
        })
  }

  getPdf() {
    this.projectService.getProjectTypeOverviewExport(this.ensembleId, this.mode, this.targetId)
        .subscribe(res => {
          saveAs(res, 'Typenübersicht.pdf');
        });
  }
}
