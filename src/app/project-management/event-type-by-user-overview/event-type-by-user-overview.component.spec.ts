import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventTypeByUserOverviewComponent } from './event-type-by-user-overview.component';

describe('EventTypeByUserOverviewComponent', () => {
  let component: EventTypeByUserOverviewComponent;
  let fixture: ComponentFixture<EventTypeByUserOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventTypeByUserOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventTypeByUserOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
