import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NotificationService } from '../../shared/services/notification.service';
import { ProjectService } from '../../shared/services/project.service';
import { EventgroupService } from '../../shared/services/eventgroup.service';
import { Project } from '../../shared/models/project';
import { Eventgroup } from '../../shared/models/eventgroup';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Mail } from '../../shared/models/mail';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { NgForm } from '@angular/forms';
import { type } from 'os';

@Component({
  selector: 'eos-send-reminder-modal',
  templateUrl: './send-reminder-modal.component.html',
  styleUrls: ['./send-reminder-modal.component.css']
})
export class SendReminderModalComponent implements OnInit {

  @Input()
  mode: 'project' | 'eventgroup';

  @Input()
  ensembleId: string;

  @Input()
  target: Project | Eventgroup;

  title: string;

  constructor(private notificationService: NotificationService,
              private projectService: ProjectService,
              private eventgroupService: EventgroupService,
              public bsModalRef: BsModalRef) { }

  ngOnInit() {
      if(this.mode == 'eventgroup'){
          //@ts-ignore
          this.title = this.target.title;
      } else {
          //@ts-ignore
          this.title = this.target.name;
      }
  }

  sendReminder(){
    if(this.mode == 'project'){
      this.projectService.sendProjectReminder(this.ensembleId, this.target.id)
          .subscribe(() => {
                this.notificationService.setSuccessNotification('Reminder gesendet');
                this.bsModalRef.hide();
              },
              error => this.notificationService.setErrorNotification(error))
    } else if (this.mode == 'eventgroup') {
      this.eventgroupService.sendEventgroupReminder(this.ensembleId, this.target.id)
          .subscribe(() => {
                this.notificationService.setSuccessNotification('Reminder gesendet');
                this.bsModalRef.hide();
              },
              error => this.notificationService.setErrorNotification(error))
    }
  }

}
