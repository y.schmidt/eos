import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendReminderModalComponent } from './send-reminder-modal.component';

describe('SendReminderModalComponent', () => {
  let component: SendReminderModalComponent;
  let fixture: ComponentFixture<SendReminderModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendReminderModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendReminderModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
