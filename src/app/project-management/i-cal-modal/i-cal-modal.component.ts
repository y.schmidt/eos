import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ProjectService } from '../../shared/services/project.service';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
  selector: 'eos-i-cal-modal',
  templateUrl: './i-cal-modal.component.html',
  styleUrls: ['./i-cal-modal.component.css']
})
export class ICalModalComponent implements OnInit {

  projectId: string;
  ensembleId: string;

  link: any;

  constructor(public bsModalRef: BsModalRef,
              private projectService: ProjectService,
              private notificationService: NotificationService) { }

  ngOnInit() {
    this.getIcalLink();
  }

  getIcalLink(){
    this.projectService.getProjectIcal(this.ensembleId, this.projectId)
        .subscribe((link) => {
          this.link = link;
        }, error => this.notificationService.setErrorNotification(error));
  }

  copyMessage(val: string){
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

}
