import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ICalModalComponent } from './i-cal-modal.component';

describe('ICalModalComponent', () => {
  let component: ICalModalComponent;
  let fixture: ComponentFixture<ICalModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ICalModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ICalModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
