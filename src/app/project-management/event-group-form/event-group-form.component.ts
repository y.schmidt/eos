import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Eventgroup } from '../../shared/models/eventgroup';
import { NgForm } from '@angular/forms';
import { FormService } from '../../shared/services/form.service';
import { Address } from '../../shared/models/address';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';

@Component({
    selector: 'eos-event-group-form',
    templateUrl: './event-group-form.component.html',
    styleUrls: ['./event-group-form.component.css']
})
export class EventGroupFormComponent implements OnInit {

    @Input()
    eventgroup: Eventgroup;

    @ViewChild('f')
    form: NgForm;

    editAddress: boolean;

    constructor(private formService: FormService,
                private localeService: BsLocaleService) { }

    ngOnInit() {
        this.editAddress = !this.isAddressEmpty();
        this.localeService.use('de');
        if (this.eventgroup.start != undefined) {
            this.eventgroup.range = [ new Date(this.eventgroup.start), new Date(this.eventgroup.end) ];
        }
    }

    markAsTouched(){
        this.formService.markFormGroupTouched(this.form);
    }

    isAddressEmpty(): boolean {
        if(this.eventgroup.address != undefined){
            return !(this.eventgroup.address.city || this.eventgroup.address.number || this.eventgroup.address.street || this.eventgroup.address.postalCode);
        } else {
            return true;
        }
    }

    addAddress(): void{
        this.eventgroup.address = new Address();
        this.editAddress = true;
    }

    checkTime(date: Date){
        if(date.getHours() == 0){
            date.setHours(12);
        }
    }

    descriptionChanged(data: string){
        this.eventgroup.description = data;
    }

    dateChanged(dates: Date[]){
        if(dates != null){
            this.eventgroup.start = dates[0];
            this.eventgroup.end = dates[1];
        }
    }

}
