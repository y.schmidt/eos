import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventGroupFormComponent } from './event-group-form.component';

describe('EventGroupFormComponent', () => {
  let component: EventGroupFormComponent;
  let fixture: ComponentFixture<EventGroupFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventGroupFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventGroupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
