import { Component, OnInit } from '@angular/core';
import { Event } from '../../shared/models/event';
import { ActivatedRoute } from '@angular/router';
import { EventService } from '../../shared/services/event.service';
import { NotificationService } from '../../shared/services/notification.service';
import { AnnouncementService } from '../../shared/services/announcement.service';
import { User } from '../../shared/models/user';
import { AttendanceService } from '../../shared/services/attendance.service';
import { Attendance } from '../../shared/models/attendance';
import { Register } from '../../shared/models/register';
import { StatisticsService } from '../../shared/services/statistics.service';
import { colorSchemes } from '../../globals';

@Component({
    selector: 'eos-event-details',
    templateUrl: './event-details.component.html',
    styleUrls: ['./event-details.component.css']
})
export class EventDetailsComponent implements OnInit {

    event: Event;
    eventId: string;
    ensembleId: string;
    projectId: string;
    users: User[];
    eventRegisters: Register[];
    registerPieChart = [];
    registerCounter: number = 0;
    announcementStats: any;
    announcementChart = [];

    colorScheme = colorSchemes.announcements;
    constructor(private route: ActivatedRoute,
                private eventService: EventService,
                private statsService: StatisticsService,
                private notificationService: NotificationService,
                private announcementService: AnnouncementService,
                private attendanceService: AttendanceService) { }

    ngOnInit() {
        this.eventId = this.route.snapshot.paramMap.get('eventId');
        this.ensembleId = this.route.snapshot.paramMap.get('ensembleId');
        this.projectId = this.route.snapshot.paramMap.get('projectId');
        this.getEvent();
    }

    getEvent(){
        this.eventService.getEventById(this.ensembleId, this.eventId)
            .subscribe((event) => {
                    this.event = event;
                    this.getAttendance();
                    this.getRegisterStats();
                    this.getAnnouncementStats();
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    getAttendance(){
        this.attendanceService.getAttendanceForEvent(this.ensembleId, this.eventId)
            .subscribe((attendance) => {
                    this.users = attendance;
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    changeAttendance(user: User, status: boolean){
        if(user.attendance[0] != null){
            let attendance = user.attendance[0];
            attendance.status = status;
            attendance.user = new User();
            attendance.user.id = user.id;
            this.attendanceService.updateAttendance(attendance, this.ensembleId)
                .subscribe((responseAttendance) => {
                    user.attendance[0] = responseAttendance;
                },
            error => this.notificationService.setErrorNotification(error));

        } else {
            let attendance = new Attendance(status);
            attendance.user.id = user.id;
            attendance.event = this.event;
            this.attendanceService.createAttendance(attendance, this.ensembleId)
                .subscribe((responseAttendance) => {
                    user.attendance[0] = responseAttendance;
                },
            error => this.notificationService.setErrorNotification(error));
        }
    }

    getRegisterStats(){
        this.statsService.getEventRegistersStats(this.ensembleId, this.eventId)
            .subscribe((registers) => {
                this.eventRegisters = registers;
                this.convertRegisterToPieChartData();
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    getAnnouncementStats(){
        this.statsService.getEventAnnouncementStats(this.ensembleId, this.eventId)
            .subscribe((announcementStats) => {
                this.announcementStats = announcementStats[0];
                this.convertAnnouncementsToBarChartData();
            },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    convertRegisterToPieChartData(){
        this.eventRegisters.forEach((register) => {
            if(register.userCounter == undefined){
                register.userCounter = 0;
            }
            this.registerCounter = this.registerCounter + register.userCounter;
            this.registerPieChart.push({name: register.name, value: register.userCounter})
        });
    }

    convertAnnouncementsToBarChartData(){
        const noResponse = this.announcementStats.projectAnnounce - this.announcementStats.announce - this.announcementStats.canceled;
        this.announcementChart = [
            {
                "name": "Angemeldet",
                "value": this.announcementStats.announce
            },
            {
                "name": "Abgemeldet",
                "value": this.announcementStats.canceled
            },
            {
                "name": "ohne Rückmeldung",
                "value": noResponse
            }
        ];
    }

}
