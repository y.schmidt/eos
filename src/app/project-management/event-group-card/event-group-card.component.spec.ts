import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventGroupCardComponent } from './event-group-card.component';

describe('EventGroupCardComponent', () => {
  let component: EventGroupCardComponent;
  let fixture: ComponentFixture<EventGroupCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventGroupCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventGroupCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
