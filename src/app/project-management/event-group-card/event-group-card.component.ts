import {Component, Input, OnInit} from '@angular/core';
import {Eventgroup} from '../../shared/models/eventgroup';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { EventGroupModalComponent } from '../event-group-modal/event-group-modal.component';
import { ActivatedRoute } from '@angular/router';
import { Project } from '../../shared/models/project';
import { ConfirmModalComponent } from '../../shared/components/confirm-modal/confirm-modal.component';
import { NotificationService } from '../../shared/services/notification.service';
import { EventgroupService } from '../../shared/services/eventgroup.service';
import { Announcement } from '../../shared/models/announcement';
import { AnnouncementService } from '../../shared/services/announcement.service';
import { UserService } from '../../shared/services/user.service';
import { AuthGuardService } from '../../shared/services/auth-guard.service';

@Component({
    selector: 'app-event-group-card',
    templateUrl: './event-group-card.component.html',
    styleUrls: ['./event-group-card.component.css']
})
export class EventGroupCardComponent implements OnInit {

    @Input()
    eventgroups: Eventgroup[];

    @Input()
    project: Project;

    ensembleId: string;
    modalRef: BsModalRef;
    isProjectEditor: boolean = false;
    isProjectManager: boolean = false;

    constructor(private modalService: BsModalService,
                private route: ActivatedRoute,
                private notificationService: NotificationService,
                private eventgroupService: EventgroupService,
                private announcementService: AnnouncementService,
                private userService: UserService,
                private authGuard: AuthGuardService) { }

    ngOnInit() {
        this.ensembleId = this.route.snapshot.paramMap.get('ensembleId');
        this.isProjectEditor = this.authGuard.hasUserPermission('projectEditor', this.ensembleId);
        this.isProjectManager = this.authGuard.hasUserPermission('projectManager', this.ensembleId);
    }

    announce(status: boolean, eventgroup: Eventgroup) {
        if(eventgroup.announcements.length > 0) {
            eventgroup.announcements[0].status = status;
            this.announcementService.updateAnnouncement(eventgroup.announcements[0], this.ensembleId)
                .subscribe(announcement => {
                    eventgroup.announcements[0] = announcement;
                    this.notificationService.setSuccessNotification('Anmeldung erfolgreich');
                }, error => this.notificationService.setErrorNotification(error));
        } else {
            let announcement = new Announcement(status);
            announcement.eventgroup = eventgroup;
            announcement.user.id = this.userService.getUserId();
            this.announcementService.createAnnouncement(announcement, this.ensembleId)
                .subscribe(announcement => {
                    if(eventgroup.announcements == undefined){
                        eventgroup.announcements = [announcement];
                    } else {
                        eventgroup.announcements.push(announcement);
                    }
                    this.notificationService.setSuccessNotification('Anmeldung erfolgreich');
                }, error => this.notificationService.setErrorNotification(error));
        }
    }

    addEventgroupModal(){
        this.modalRef = this.modalService.show(EventGroupModalComponent, {
            ignoreBackdropClick: true,
            initialState: {
                mode: 'add',
                eventgroup: new Eventgroup(),
                ensembleId: this.ensembleId,
                project: this.project
            }
        });
        this.modalRef.content.newEventgroup
            .subscribe((eventgroup) => this.eventgroups.push(eventgroup));
    }

    duplicateEventgroupModal(eventgroup: Eventgroup){
        this.modalRef = this.modalService.show(EventGroupModalComponent, {
            ignoreBackdropClick: true,
            initialState: {
                mode: 'duplicate',
                ensembleId: this.ensembleId,
                eventgroup: Object.assign( {}, eventgroup),
                project: this.project
            }
        });
        this.modalRef.content.newEventgroup
            .subscribe((eventgroup) => this.eventgroups.push(eventgroup));
    }
}
