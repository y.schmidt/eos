import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Event } from '../../shared/models/event';
import { EventModalComponent } from '../event-modal/event-modal.component';
import { EventListModalComponent } from '../event-list-modal/event-list-modal.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Project } from '../../shared/models/project';
import { Eventgroup } from '../../shared/models/eventgroup';
import { ActivatedRoute } from '@angular/router';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { EventService } from '../../shared/services/event.service';
import { NotificationService } from '../../shared/services/notification.service';
import { EventDeleteConfirmModalComponent } from '../event-delete-confirm-modal/event-delete-confirm-modal.component';

@Component({
    selector: 'eos-event',
    templateUrl: './event.component.html',
    styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

    @Input()
    event: Event;

    @Input()
    project: Project;

    @Input()
    eventgroup: Eventgroup;

    @Input()
    controls: boolean = false;

    @Input()
    ensembleId: string;

    @Output()
    newEvent: EventEmitter<any> = new EventEmitter();

    @Output()
    updateMultipleEvents: EventEmitter<any> = new EventEmitter();

    @Output()
    eventDeleted: EventEmitter<any> = new EventEmitter();

    isProjectManager: boolean = false;
    isProjectEditor: boolean = false;
    modalRef: BsModalRef;
    dateMode: 'project' | 'eventgroup' = 'project';

    constructor(private modalService: BsModalService,
                private route: ActivatedRoute,
                private authGuard: AuthGuardService,
                private eventService: EventService,
                private notificationService: NotificationService) { }

    ngOnInit() {
        if(!this.ensembleId){
            this.ensembleId = this.route.snapshot.paramMap.get('ensembleId');
        }
        if(this.eventgroup){
            this.dateMode = 'eventgroup';
        }
        this.isProjectEditor = this.authGuard.hasUserPermission('projectEditor', this.ensembleId);
        this.isProjectManager = this.authGuard.hasUserPermission('projectManager', this.ensembleId);
    }

    openEditEventModal(event: Event) {
        this.modalRef = this.modalService.show(EventModalComponent,{
            initialState: {
                mode: 'edit',
                event: Object.assign({}, event),
                ensembleId: this.ensembleId,
                project: this.project,
                dateMode: this.dateMode
            }
        });
        this.modalRef.content.updateMultipleEventsEmitter
            .subscribe(() => this.updateMultipleEvents.emit());
        this.modalRef.content.updateEventEmitter
            .subscribe((event) => this.event = event);
    }

    openDuplicateEventModal(event: Event) {
        const newEvent = Object.assign( {}, event);
        newEvent.announcements = [];
        this.modalRef = this.modalService.show(EventModalComponent, {
            initialState: {
                mode: 'duplicate',
                ensembleId: this.ensembleId,
                event: newEvent,
                project: this.project,
                dateMode: this.dateMode
            }
        });
        this.modalRef.content.newEvent.subscribe(newevent => this.newEvent.emit(newevent));
    }

    openListEventModal(event: Event){
        this.modalRef = this.modalService.show(EventListModalComponent, {
            initialState: {
                event: event,
                ensembleId: this.ensembleId
            }
        })
    }

    deleteEvent(response: any) {
        if(response.deleteSerial){
            this.eventService.deleteSerialEvent(response.event.id, this.ensembleId)
                .subscribe(() => {
                        this.modalRef.hide();
                        this.notificationService.setSuccessNotification('Events gelöscht!');
                        this.updateMultipleEvents.emit();
                    },
                    error => this.notificationService.setErrorNotification(error)
                );
        } else {
            this.eventService.deleteEvent(response.event.id, this.ensembleId)
                .subscribe(() => {
                        this.modalRef.hide();
                        this.notificationService.setSuccessNotification('Event gelöscht!');
                        this.eventDeleted.emit(response.event);
                    },
                    error => this.notificationService.setErrorNotification(error)
                );
        }

    }

    openConfirmModal(event: Event){
        this.modalRef = this.modalService.show(EventDeleteConfirmModalComponent, {initialState:
                {event: event}});
        this.modalRef.content.confirmEvent
            .subscribe((event) => {
                this.deleteEvent(event);
            });
    }

    getClass(type: string){
        switch(type){
            case 'rehearsal': return 'badge-success';
            case 'concert': return 'badge-warning';
            case 'travel': return 'badge-info';
            case 'breakfast': return 'badge-secondary';
            case 'lunch': return 'badge-secondary';
            case 'dinner': return 'badge-secondary';
            case 'other': return 'badge-info';
            case 'overnight': return 'badge-info';
            default: return 'badge-info';
        }
    }
}
