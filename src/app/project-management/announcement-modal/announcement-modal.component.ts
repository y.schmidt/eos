import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Announcement } from '../../shared/models/announcement';
import { Eventgroup } from '../../shared/models/eventgroup';
import { NgForm } from '@angular/forms';
import { FormService } from '../../shared/services/form.service';

@Component({
  selector: 'eos-announcement-modal',
  templateUrl: './announcement-modal.component.html',
  styleUrls: ['./announcement-modal.component.css']
})
export class AnnouncementModalComponent implements OnInit {

  announcement: Announcement;
  subject: Eventgroup;
  mode: 'announce' | 'edit';

  @ViewChild('f')
  form: NgForm;

  @Output()
  announcedEmitter: EventEmitter<any> = new EventEmitter<any>();

  constructor(public bsModalRef: BsModalRef,
              private formService: FormService) { }

  ngOnInit() {
  }

  announce(){
    if(this.form.form.valid){
      this.announcedEmitter.emit(this.announcement);
      this.bsModalRef.hide();
    } else {
      this.markAsTouched();
    }
  }

  markAsTouched(){
    this.formService.markFormGroupTouched(this.form);
  }


}
