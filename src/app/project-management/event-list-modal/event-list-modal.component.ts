import { Component, Input, OnInit } from '@angular/core';
import { AnnouncementService } from '../../shared/services/announcement.service';
import { NotificationService } from '../../shared/services/notification.service';
import { Event } from '../../shared/models/event';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { User } from '../../shared/models/user';
import { AttendanceService } from '../../shared/services/attendance.service';
import { Attendance } from '../../shared/models/attendance';

@Component({
    selector: 'eos-event-list-modal',
    templateUrl: './event-list-modal.component.html',
    styleUrls: ['./event-list-modal.component.css']
})
export class EventListModalComponent implements OnInit {

    @Input()
    event: Event;

    @Input()
    ensembleId: string;

    users: User[];
    constructor(public bsModalRef: BsModalRef,
                private announcementService: AnnouncementService,
                private notificationService: NotificationService,
                private attendanceService: AttendanceService) { }

    ngOnInit() {
        this.getAttendance();
    }

    getAttendance(){
        this.attendanceService.getAttendanceForEvent(this.ensembleId, this.event.id)
            .subscribe((attendance) => {
                    this.users = attendance;
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    getAnnouncements(){
        this.announcementService.getAnnouncementsForEvent(this.ensembleId, this.event.id)
            .subscribe((announcements) => {
                this.users = announcements;
            }, error => this.notificationService.setErrorNotification(error));
    }

    changeAttendance(user: User, status: boolean){
        if(user.attendance[0] != null){
            let attendance = user.attendance[0];
            attendance.status = status;
            attendance.user = new User();
            attendance.user.id = user.id;
            this.attendanceService.updateAttendance(attendance, this.ensembleId)
                .subscribe((responseAttendance) => {
                        user.attendance[0] = responseAttendance;
                    },
                    error => this.notificationService.setErrorNotification(error));

        } else {
            let attendance = new Attendance(status);
            attendance.user.id = user.id;
            attendance.event = this.event;
            this.attendanceService.createAttendance(attendance, this.ensembleId)
                .subscribe((responseAttendance) => {
                        user.attendance[0] = responseAttendance;
                    },
                    error => this.notificationService.setErrorNotification(error));
        }
    }

}
