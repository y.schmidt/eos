import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPastProjectsComponent } from './my-past-projects.component';

describe('MyPastProjectsComponent', () => {
  let component: MyPastProjectsComponent;
  let fixture: ComponentFixture<MyPastProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyPastProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPastProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
