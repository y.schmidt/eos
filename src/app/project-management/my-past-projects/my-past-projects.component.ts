import { Component, OnInit } from '@angular/core';
import { Project } from '../../shared/models/project';
import { ProjectService } from '../../shared/services/project.service';
import { UserService } from '../../shared/services/user.service';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
  selector: 'eos-my-past-projects',
  templateUrl: './my-past-projects.component.html',
  styleUrls: ['./my-past-projects.component.css']
})
export class MyPastProjectsComponent implements OnInit {

  projects: Project[];
  constructor(private projectService: ProjectService,
              private userService: UserService,
              private notificationService: NotificationService) { }

  ngOnInit() {
    this.getProjects();
  }

  getProjects() {
    this.projectService.getProjects(this.userService.getUserId(), true)
        .subscribe(projects => {
              this.projects = projects;
            },
            error => this.notificationService.setErrorNotification(error));
  }
}
