import { Component, OnInit } from '@angular/core';
import { Project } from '../../shared/models/project';
import { ProjectService } from '../../shared/services/project.service';
import { UserService } from '../../shared/services/user.service';
import { NotificationService } from '../../shared/services/notification.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ProjectAddComponent } from '../project-add/project-add.component';

@Component({
    selector: 'app-projects',
    templateUrl: './my-projects.component.html',
    styleUrls: ['./my-projects.component.css']
})
export class MyProjectsComponent implements OnInit {

    projects: Project[];
    modalRef: BsModalRef;
    constructor(private projectService: ProjectService,
                private userService: UserService,
                private notificationService: NotificationService,
                private modalService: BsModalService) { }

    ngOnInit() {
        this.getProjects();
    }

    getProjects() {
        this.projectService.getProjects(this.userService.getUserId())
            .subscribe(projects => {
                this.projects = projects;
            },
                error => this.notificationService.setErrorNotification(error));
    }

    addProjectModal(){
        this.modalRef = this.modalService.show(ProjectAddComponent, {
            ignoreBackdropClick: true
        });
        this.modalRef.content.newProject.subscribe((project) => this.projects.push(project));
    }

}
