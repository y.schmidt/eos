import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnouncementRegistersComponent } from './announcement-registers.component';

describe('AnnouncementRegistersComponent', () => {
  let component: AnnouncementRegistersComponent;
  let fixture: ComponentFixture<AnnouncementRegistersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnouncementRegistersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnouncementRegistersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
