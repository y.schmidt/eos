import { Component, Input, OnInit } from '@angular/core';
import { Register } from '../../shared/models/register';
import { Eventgroup } from '../../shared/models/eventgroup';
import { Project } from '../../shared/models/project';
import { StatisticsService } from '../../shared/services/statistics.service';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
  selector: 'eos-announcement-registers',
  templateUrl: './announcement-registers.component.html',
  styleUrls: ['./announcement-registers.component.css']
})
export class AnnouncementRegistersComponent implements OnInit {

  @Input()
  mode: 'project' | 'eventgroup';

  @Input()
  eventgroup: Eventgroup;

  @Input()
  project: Project;

  @Input()
  ensembleId: string;

  registers: Register[];
  registerPieChart = [];

  constructor(private statsService: StatisticsService,
              private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.getRegisters();
  }

  getRegisters(){
    if(this.mode == 'eventgroup'){
      this.statsService.getEventgroupRegistersStats(this.ensembleId, this.eventgroup.id)
          .subscribe((registers) => {
            this.registers = registers;
            this.convertRegisterToPieChartData();
          }, error => this.notificationService.setErrorNotification(error));
    } else {
      this.statsService.getProjectRegistersStats(this.ensembleId, this.project.id)
          .subscribe((registers) => {
            this.registers = registers;
            this.convertRegisterToPieChartData();
          }, error => this.notificationService.setErrorNotification(error));
    }
  }

  convertRegisterToPieChartData(){
    this.registers.forEach((register) => {
      if(register.userCounter == undefined){
        register.userCounter = 0;
      }
      this.registerPieChart.push({name: register.name, value: register.userCounter})
    });
  }


}
