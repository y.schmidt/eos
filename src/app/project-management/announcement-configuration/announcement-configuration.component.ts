import { Component, Input, OnInit } from '@angular/core';
import { Eventgroup } from '../../shared/models/eventgroup';

@Component({
  selector: 'eos-announcement-configuration',
  templateUrl: './announcement-configuration.component.html',
  styleUrls: ['./announcement-configuration.component.css']
})
export class AnnouncementConfigurationComponent implements OnInit {

  @Input()
  eventgroup: Eventgroup;

  constructor() { }

  ngOnInit() {
  }
}
