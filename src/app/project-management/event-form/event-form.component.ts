import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Event } from '../../shared/models/event';
import { Address } from '../../shared/models/address';
import { FormService } from '../../shared/services/form.service';
import { NgForm } from '@angular/forms';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';


@Component({
    selector: 'app-event-form',
    templateUrl: './event-form.component.html',
    styleUrls: ['./event-form.component.css']
})
export class EventFormComponent implements OnInit {

    @Input()
    event: Event;

    @Input()
    showRepeat: boolean = false;

    @Input()
    dateMode: 'project' | 'eventgroup' = 'project';

    @ViewChild('f')
    form: NgForm;

    editAddress: boolean;

    constructor(private formService: FormService,
                private localeService: BsLocaleService,) { }

    ngOnInit() {
        this.localeService.use('de');
        this.editAddress = !this.isAddressEmpty();
        this.event.editRepeat = false;
        //this.setDateLimits();
    }

    isAddressEmpty(): boolean {
        if(this.event.address){
            if(this.event.address.city || this.event.address.number || this.event.address.street || this.event.address.postalCode) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    setDateLimits(){
        if(this.event.eventgroup != undefined){
            this.dateMode = 'eventgroup';
        }
    }

    setEndDate(date){
        if(date != null){
            this.event.end = new Date(date);
            this.event.end.setHours(this.event.end.getHours() + 2);
        }
    }

    addAddress(): void{
        this.event.address = new Address();
        this.editAddress = true;
    }

    markAsTouched(): void{
        this.formService.markFormGroupTouched(this.form);
    }

    checkTime(date: Date){
        if(date.getHours() == 0){
            date.setHours(12);
        }
    }
}
