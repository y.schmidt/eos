import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NotificationService } from '../../shared/services/notification.service';
import { Address } from '../../shared/models/address';
import { Project } from '../../shared/models/project';
import { ProjectService } from '../../shared/services/project.service';
import { ProjectFormComponent } from '../project-form/project-form.component';
import { AuthGuardService } from '../../shared/services/auth-guard.service';

@Component({
    selector: 'eos-project-add',
    templateUrl: './project-add.component.html',
    styleUrls: ['./project-add.component.css']
})
export class ProjectAddComponent implements OnInit {

    project: Project = new Project();
    error: string;
    isAdmin: boolean = false;
    @Output()
    newProject: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild(ProjectFormComponent)
    private formComp: ProjectFormComponent;

    constructor(public bsModalRef: BsModalRef,
                private projectService: ProjectService,
                private notificationService: NotificationService,
                private authGuardService: AuthGuardService
    ) { }

    ngOnInit() {
        this.project.address = new Address();
        this.isAdmin = this.authGuardService.isUserAdminInEnsemble();

    }

    createProject(){
        if(this.formComp.form.form.valid){
            this.project.start = this.project.range[0];
            this.project.end = this.project.range[1];
            this.projectService.createProject(this.project, this.project.ensembles[0].id)
                .subscribe((project) => {
                    this.newProject.emit(project);
                    this.bsModalRef.hide();
                }, (error) => {
                    this.notificationService.setErrorNotification(error);
                });
        } else {
            this.formComp.markAsTouched();
        }
    }

}
