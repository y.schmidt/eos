import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventSerialModalComponent } from './event-serial-modal.component';

describe('EventSerialModalComponent', () => {
  let component: EventSerialModalComponent;
  let fixture: ComponentFixture<EventSerialModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventSerialModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventSerialModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
