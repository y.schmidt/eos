import { Component, OnInit } from '@angular/core';
import { Eventgroup } from '../../shared/models/eventgroup';
import { Event } from '../../shared/models/event';
import { EventgroupService } from '../../shared/services/eventgroup.service';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from '../../shared/services/notification.service';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { EventService } from '../../shared/services/event.service';
import { MapsService } from '../../shared/services/maps.service';
import { SendReminderModalComponent } from '../send-reminder-modal/send-reminder-modal.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { mockupEventgroup, mockupEvents } from '../../globals';

@Component({
    selector: 'app-event-group',
    templateUrl: './event-group.component.html',
    styleUrls: ['./event-group.component.css']
})
export class EventGroupComponent implements OnInit {

    eventgroup: Eventgroup;

    events: Event[];
    ensembleId: string;

    isProjectEditor: boolean = false;
    isProjectManager: boolean = false;
    modalRef: BsModalRef;

    constructor(private route: ActivatedRoute,
                private eventgroupService: EventgroupService,
                private notificationService: NotificationService,
                private authGuard: AuthGuardService,
                private eventService: EventService,
                private modalService: BsModalService) { }

    ngOnInit() {
        this.ensembleId = this.route.snapshot.paramMap.get('ensembleId');
        this.isProjectEditor = this.authGuard.hasUserPermission('projectEditor', this.ensembleId);
        this.isProjectManager = this.authGuard.hasUserPermission('projectManager', this.ensembleId);
        this.getEventgroup();
    }

    getEventgroup() {
        const eventgroupId = this.route.snapshot.paramMap.get('eventgroupId');
        if(eventgroupId == 'example-eventgroup'){
            this.eventgroup = mockupEventgroup[0];
            this.events = mockupEvents;
        } else {
            this.eventgroupService.getEventgroupById(this.ensembleId, eventgroupId)
                .subscribe((eventgroup) => {
                        this.eventgroup = eventgroup;
                        this.getEvents();
                        // this.getCoords();
                    },
                    error => this.notificationService.setErrorNotification(error)
                );
        }
    }

    getEvents(){
        this.eventService.getEventsForEventgroup(this.ensembleId, this.eventgroup.id)
            .subscribe((events) => {
                    //this.eventgroup.events = events;
                    this.events = events;
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    announcementChanged(){
        this.getEventgroup();
    }

    getPdf() {

    }

    sendReminder(){
        this.modalRef = this.modalService.show(SendReminderModalComponent, {
            initialState: {
                ensembleId: this.ensembleId,
                target: this.eventgroup,
                mode: 'eventgroup'
            }
        });
    }
}
