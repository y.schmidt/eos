import { Component, Input, OnInit } from '@angular/core';
import { StatisticsService } from '../../shared/services/statistics.service';
import { NotificationService } from '../../shared/services/notification.service';
import { ProjectService } from '../../shared/services/project.service';
import saveAs from 'file-saver';

@Component({
  selector: 'eos-event-type-overview',
  templateUrl: './event-type-overview.component.html',
  styleUrls: ['./event-type-overview.component.css']
})
export class EventTypeOverviewComponent implements OnInit {

  @Input()
  ensembleId: string;

  @Input()
  targetId: string;

  @Input()
  mode: 'eventgroup' | 'project';

  stats:[];

  constructor(private statsService: StatisticsService,
              private projectService: ProjectService,
              private notificationService: NotificationService) { }

  ngOnInit() {
    this.getStats();
  }

  getStats(){
    this.statsService.countAnnouncementsByType(this.ensembleId, this.mode, this.targetId)
        .subscribe((stats) => {
          this.stats = stats;
        })
  }

  getPdf() {
    this.projectService.getProjectTypeOverviewExport(this.ensembleId, this.mode, this.targetId)
        .subscribe(res => {
          saveAs(res, 'Typenübersicht.pdf');
        });
  }
}
