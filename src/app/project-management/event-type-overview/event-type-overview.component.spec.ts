import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventTypeOverviewComponent } from './event-type-overview.component';

describe('EventTypeOverviewComponent', () => {
  let component: EventTypeOverviewComponent;
  let fixture: ComponentFixture<EventTypeOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventTypeOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventTypeOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
