import { Component, Input, OnInit } from '@angular/core';
import { AnnouncementService } from '../../shared/services/announcement.service';
import { Eventgroup } from '../../shared/models/eventgroup';
import { Project } from '../../shared/models/project';
import { User } from '../../shared/models/user';
import { NotificationService } from '../../shared/services/notification.service';
import saveAs from 'file-saver';

@Component({
  selector: 'eos-announcement-list',
  templateUrl: './announcement-list.component.html',
  styleUrls: ['./announcement-list.component.css']
})
export class AnnouncementListComponent implements OnInit {

  @Input()
  mode: 'project' | 'eventgroup';

  @Input()
  eventgroup: Eventgroup;

  @Input()
  project: Project;

  @Input()
  ensembleId: string;

  target: Eventgroup | Project;

  announcements: User[];
  constructor(private announcementService: AnnouncementService,
              private notificationService: NotificationService) { }

  ngOnInit(): void {
    if(this.mode == 'eventgroup'){
      this.target = this.eventgroup;
    } else {
      this.target = this.project
    }
    this.getAnnouncements();
  }

  getAnnouncements(){
    if(this.mode == 'eventgroup') {
      this.announcementService.getAnnouncementsForEventgroup(this.ensembleId, this.eventgroup.id)
          .subscribe((announcements) => {
            this.announcements = announcements;
          }, error => this.notificationService.setErrorNotification(error));
    } else {
      this.announcementService.getAnnouncementsForProject(this.ensembleId, this.project.id)
          .subscribe((announcements) => {
            this.announcements = announcements;
          }, error => this.notificationService.setErrorNotification(error));
    }
  }

  getPdf() {
    if(this.mode == 'eventgroup') {
      this.announcementService.getEventgroupAnnouncementsPdfExport(this.ensembleId, this.eventgroup.id)
          .subscribe(res => {
            saveAs(res, this.eventgroup.title + ' - Anmeldeliste.pdf');
          });
    } else {
      this.announcementService.getProjectAnnouncementsPdfExport(this.ensembleId, this.project.id)
          .subscribe(res => {
            saveAs(res, this.project.name + ' - Anmeldeliste.pdf');
          });
    }

  }

  getCsv() {
    if(this.mode == 'eventgroup'){
      this.announcementService.getEventgroupAnnouncementsCsvExport(this.ensembleId, this.eventgroup.id)
          .subscribe(res => {
            saveAs(res, this.eventgroup.title + ' - Anmeldeliste.csv');
          });
    } else {
      this.announcementService.getProjectAnnouncementsCsvExport(this.ensembleId, this.project.id)
          .subscribe(res => {
            saveAs(res, this.project.name + ' - Anmeldeliste.csv');
          });
    }
  }
}
