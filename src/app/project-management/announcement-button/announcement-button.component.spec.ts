import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnouncementButtonComponent } from './announcement-button.component';

describe('AnnouncementButtonComponent', () => {
  let component: AnnouncementButtonComponent;
  let fixture: ComponentFixture<AnnouncementButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnouncementButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnouncementButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
