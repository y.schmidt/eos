import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { Eventgroup } from '../../shared/models/eventgroup';
import { Project } from '../../shared/models/project';
import { AnnouncementService } from '../../shared/services/announcement.service';
import { UserService } from '../../shared/services/user.service';
import { NotificationService } from '../../shared/services/notification.service';
import { Event } from '../../shared/models/event';
import { Announcement } from '../../shared/models/announcement';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { User } from '../../shared/models/user';
import { AnnouncementModalComponent } from '../announcement-modal/announcement-modal.component';
import { Angulartics2 } from 'angulartics2';

@Component({
    selector: 'eos-announcement-button',
    templateUrl: './announcement-button.component.html',
    styleUrls: ['./announcement-button.component.css']
})
export class AnnouncementButtonComponent implements OnInit {

    @Input()
    event: Event;

    @Input()
    eventgroup: Eventgroup;

    @Input()
    project: Project;

    @Input()
    mode: string;

    @Input()
    ensembleId: string;

    subject: Event | Eventgroup | Project;

    modalRef: BsModalRef;

    newAnnouncement: Announcement = new Announcement(null);

    @Output()
    announcementChanged: EventEmitter<any> = new EventEmitter();

    user: User;
    constructor(private announcementService: AnnouncementService,
                private userService: UserService,
                private notificationService: NotificationService,
                private modalService: BsModalService,
                private angulartics2: Angulartics2,
                private authService: AuthGuardService) { }

    ngOnInit() {
        this.user = this.authService.getAuthedUser();
        switch (this.mode) {
            case 'event':
                this.subject = this.event;
                break;
            case 'eventgroup':
                this.subject = this.eventgroup;
                break;
            case 'project':
                this.subject = this.project;
                break;
        }
    }

    checkForAnnouncementInformations(status: boolean){
        if (this.mode === 'eventgroup' && this.subject.enableInformation) {
            this.openAnnouncementModal('announce');
        } else {
            this.announce(status);
        }
    }

    announce(status: boolean) {
        if(this.subject.announcements.length > 0) {
            this.subject.announcements[0].status = status;
            this.announcementService.updateAnnouncement(this.subject.announcements[0], this.ensembleId)
                .subscribe(announcement => {
                    this.subject.announcements[0] = announcement;
                    this.announcementChanged.emit(announcement);
                    this.notificationService.setSuccessNotification('Anmeldung erfolgreich');
                }, error => {
                    this.subject.announcements[0].status = !status;
                    this.notificationService.setErrorNotification(error)
                });
        } else {
            this.newAnnouncement.status = status;
            switch (this.mode) {
                case 'event':
                    // @ts-ignore
                    this.newAnnouncement.event = this.subject;
                    break;
                case 'eventgroup':
                    // @ts-ignore
                    this.newAnnouncement.eventgroup = this.subject;
                    break;
                case 'project':
                    // @ts-ignore
                    this.newAnnouncement.project = this.subject;
                    break;
            }
            this.newAnnouncement.user.id = this.userService.getUserId();
            this.announcementService.createAnnouncement(this.newAnnouncement, this.ensembleId)
                .subscribe(announcement => {
                    if(this.subject.announcements == undefined){
                        this.subject.announcements = [announcement];
                    } else {
                        this.subject.announcements.push(announcement);
                    }
                    this.announcementChanged.emit(announcement);
                    this.notificationService.setSuccessNotification('Anmeldung erfolgreich');
                }, error => {
                    this.subject.announcements[0].status = !status;
                    this.notificationService.setErrorNotification(error)
                });
        }
    }

    isAnnouncedToProject(){
        switch (this.mode) {
            case 'event':
                if(this.eventgroup != undefined && this.eventgroup.announcements && this.eventgroup.announcements.length > 0 && this.project.announcements.length > 0){
                    return this.eventgroup.announcements[0].status && this.project.announcements[0].status;
                } else if(this.eventgroup && this.eventgroup.announcements && this.eventgroup.announcements.length == 0) {
                    return false
                } else {
                    if(this.project.announcements.length > 0){
                        return this.project.announcements[0].status;
                    } else {
                        return false;
                    }
                }
            case 'eventgroup':
                if(this.project.announcements.length > 0){
                    return this.project.announcements[0].status;
                } else {
                    return false;
                }
            case 'project':
                return true;
        }
    }

    isInPast(){
        return new Date() > this.subject.end;
    }

    isRunning(){
        switch (this.mode) {
            case 'event':
            case 'eventgroup':
                // @ts-ignore
                return new Date() > this.subject.start && new Date() < this.subject.end;
            case 'project':
                return false;
        }
    }

    openAnnouncementModal(mode: 'announce' | 'edit'){
        let announcement;
        if(this.subject.announcements.length > 0) {
            announcement = this.subject.announcements[0];
        }
        else {
            announcement = new Announcement(true);
        }
        this.modalRef = this.modalService.show(AnnouncementModalComponent, {
            ignoreBackdropClick: true,
            initialState: {
                subject: this.subject,
                mode: mode,
                announcement: announcement
            }
        });
        this.modalRef.content.announcedEmitter
            .subscribe((announcement) => {
                this.newAnnouncement = announcement;
                this.announce(true);
            });
    }

    saveComment(){
        this.announcementService.updateAnnouncement(this.subject.announcements[0], this.ensembleId)
            .subscribe((announcement) => {
                this.subject.announcements[0] = announcement;
                this.modalRef.hide();
                this.notificationService.setSuccessNotification('Kommentar gespeichert');
            }, error => this.notificationService.setErrorNotification(error));
    }

    isDeadline(){
        if(this.subject.deadline != null){
            return this.subject.deadline < new Date();
        } else {
            return false;
        }

    }

    isUserInTargetGroup(){
        switch (this.mode) {
            case 'event':
            case 'eventgroup':
                if(this.subject.id.includes('example')){
                    return true;
                } else {
                    let response = false;
                    if(this.user.registers && this.user.registers.length > 0){
                        this.user.registers.forEach((register) => {
                            const index = this.subject.registers.find((elt) => (elt.id === register.id));
                            if (index !== undefined) {
                                response = true;
                            }
                        });
                    }
                    return response;
                }
            case 'project':
                return true;
        }
    }
}
