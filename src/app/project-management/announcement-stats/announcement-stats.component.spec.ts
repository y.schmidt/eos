import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnouncementStatsComponent } from './announcement-stats.component';

describe('AnnouncementStatsComponent', () => {
  let component: AnnouncementStatsComponent;
  let fixture: ComponentFixture<AnnouncementStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnouncementStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnouncementStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
