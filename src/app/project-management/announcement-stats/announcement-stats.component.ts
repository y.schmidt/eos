import { Component, Input, OnInit } from '@angular/core';
import { Eventgroup } from '../../shared/models/eventgroup';
import { Project } from '../../shared/models/project';
import { StatisticsService } from '../../shared/services/statistics.service';
import { NotificationService } from '../../shared/services/notification.service';
import { colorSchemes } from '../../globals';

@Component({
  selector: 'eos-announcement-stats',
  templateUrl: './announcement-stats.component.html',
  styleUrls: ['./announcement-stats.component.css']
})
export class AnnouncementStatsComponent implements OnInit {

  @Input()
  mode: 'project' | 'eventgroup';

  @Input()
  eventgroup: Eventgroup;

  @Input()
  project: Project;

  @Input()
  ensembleId: string;

  announcementStats: any;
  announcementChart = [];

  announcementScheme = colorSchemes.announcements;
  constructor(private statsService: StatisticsService,
              private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.getAnnouncementStats();
  }

  getAnnouncementStats(){
    if(this.mode == 'eventgroup'){
      this.statsService.getEventgroupAnnouncementStats(this.ensembleId, this.eventgroup.id)
          .subscribe((stats) => {
            this.announcementStats = stats[0];
            this.convertAnnouncementsToBarChartData();
          }, error => this.notificationService.setErrorNotification(error));
    } else {
      this.statsService.getProjectAnnouncementStats(this.ensembleId, this.project.id)
          .subscribe((stats) => {
            this.announcementStats = stats[0];
            this.convertAnnouncementsToBarChartData();
          }, error => this.notificationService.setErrorNotification(error));
    }
  }

  convertAnnouncementsToBarChartData(){
    let noResponse;
    if(this.mode == 'eventgroup'){
      noResponse = this.announcementStats.projectAnnounce - this.announcementStats.announce - this.announcementStats.canceled;
    } else {
      noResponse = this.announcementStats.ensembleUsers - this.announcementStats.announce - this.announcementStats.canceled;
    }
    this.announcementChart = [
      {
        "name": "Angemeldet",
        "value": this.announcementStats.announce
      },
      {
        "name": "Abgemeldet",
        "value": this.announcementStats.canceled
      },
      {
        "name": "ohne Rückmeldung",
        "value": noResponse
      }
    ];
  }

}
