import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { UserRoutingModule } from './user-routing.module';
import { UserFormComponent } from './user-form/user-form.component';
import { SharedModule } from '../shared/shared.module';
import { UserChangeLoginDataComponent } from './user-change-login-data/user-change-login-data.component';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { UserDataComponent } from './user-data/user-data.component';
import { UserPrivacySettingsComponent } from './user-privacy-settings/user-privacy-settings.component';

@NgModule({
    imports: [
        CommonModule,
        UserRoutingModule,
        SharedModule
    ],
    declarations: [ProfileComponent, UserFormComponent, UserChangeLoginDataComponent, UserSettingsComponent, UserDataComponent, UserPrivacySettingsComponent]
})
export class UserManagementModule { }
