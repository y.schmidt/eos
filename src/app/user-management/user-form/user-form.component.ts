import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../shared/models/user';
import { Address } from '../../shared/models/address';

@Component({
    selector: 'eos-user-form',
    templateUrl: './user-form.component.html',
    styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

    @Input()
    user: User;
    constructor() { }

    ngOnInit() {
        if(this.user.address == null){
            this.user.address = new Address();
        }
    }

}
