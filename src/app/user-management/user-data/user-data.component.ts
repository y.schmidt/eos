import { Component, OnInit } from '@angular/core';
import { User } from '../../shared/models/user';
import { UserService } from '../../shared/services/user.service';
import { NotificationService } from '../../shared/services/notification.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { PictureUploadComponent } from '../../shared/components/picture-upload/picture-upload.component';

@Component({
    selector: 'eos-user-data',
    templateUrl: './user-data.component.html',
    styleUrls: ['./user-data.component.css']
})
export class UserDataComponent implements OnInit {

    public user: User;
    modalRef: BsModalRef;
    profilePicture: string;
    constructor(private userService: UserService,
                private modalService: BsModalService,
                private notificationService: NotificationService) { }

    ngOnInit() {
        this.profilePicture = './assets/img/user.jpg';
        this.getUser();
        this.userService.profilePictureEmitter
            .subscribe(() => {
                this.getProfilePicture();
            });
    }

    getUser(){
        this.userService.getUserById(this.userService.getUserId(), 'join=address')
            .subscribe(
                user => {
                    if(user != undefined){
                        this.user = user;
                        this.getProfilePicture();
                    }
                },
                err => this.notificationService.setErrorNotification(err)
            );
    }

    getProfilePicture(){
        this.userService.userGetProfilePicture(this.user.id)
            .subscribe((pic) => {
                this.profilePicture = 'data:image/webp;base64,' + pic.picture;
            }, err => this.notificationService.setErrorNotification(err));
    }

    save(){
        this.userService.updateUser(this.user)
            .subscribe(user => {
                    this.user = user;
                    this.notificationService.setSuccessNotification('Benutzer erfolgreich gespeichert');
                },
                err => this.notificationService.setErrorNotification(err)
            );
    }

    openFileUploadModal(){
        this.modalRef = this.modalService.show(PictureUploadComponent, {
            initialState: {
                mode: 'profilePicture',
                target: this.user
            }
        });
    }
}
