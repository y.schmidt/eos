import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserChangeLoginDataComponent } from './user-change-login-data.component';

describe('UserChangeLoginDataComponent', () => {
  let component: UserChangeLoginDataComponent;
  let fixture: ComponentFixture<UserChangeLoginDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserChangeLoginDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserChangeLoginDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
