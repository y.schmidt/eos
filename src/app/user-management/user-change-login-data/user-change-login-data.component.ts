import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { User } from '../../shared/models/user';
import { NotificationService } from '../../shared/services/notification.service';
import { UserService } from '../../shared/services/user.service';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'eos-user-change-login-data',
    templateUrl: './user-change-login-data.component.html',
    styleUrls: ['./user-change-login-data.component.css']
})
export class UserChangeLoginDataComponent implements OnInit {

    @Input()
    user: User;

    @ViewChild('passwordForm')
    passwordForm: NgForm;

    @ViewChild('mailForm')
    mailForm: NgForm;

    password: any = {};

    newMail: any = {};
    constructor(private notificationService: NotificationService,
                private userService: UserService) { }

    ngOnInit() {}

    changePassword(){
        if(this.password.new == this.password.discard && this.passwordForm.form.valid){
            this.password.userId = this.user.id;
            this.userService.userChangePassword(this.password)
                .subscribe(() => {
                    this.notificationService.setSuccessNotification('Passwort wurde geändert');
                    this.passwordForm.reset();
                },
                    error => {
                    this.notificationService.setErrorNotification(error);
                    this.passwordForm.reset();
                });
        } else {
            this.notificationService.setManualErrorNotification('Passwörter stimmen nicht überein');
        }
    }

    changeMail(){
        if(this.mailForm.form.valid){
            this.newMail.userId = this.user.id;
            this.newMail.actual = this.user.mail;
            this.userService.userChangeMail(this.newMail)
                .subscribe(() => {
                        this.user.mail = this.newMail.new;
                        this.notificationService.setSuccessNotification('E-Mail wurde geändert');
                        this.mailForm.controls['passwordInput'].reset();
                        this.mailForm.controls['newMailInput'].reset();
                    },
                    error => {
                        this.notificationService.setErrorNotification(error);
                        this.mailForm.reset();
                    });
        }
    }
}
