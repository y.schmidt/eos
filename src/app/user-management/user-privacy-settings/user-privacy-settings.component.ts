import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../shared/models/user';
import { UserService } from '../../shared/services/user.service';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
  selector: 'eos-user-privacy-settings',
  templateUrl: './user-privacy-settings.component.html',
  styleUrls: ['./user-privacy-settings.component.css']
})
export class UserPrivacySettingsComponent implements OnInit {

  @Input()
  user: User;

  constructor(private userService: UserService,
              private notificationService: NotificationService) { }

  ngOnInit() {
  }

  save(){
    this.userService.updateUser(this.user)
        .subscribe(user => {
              this.user = user;
              this.notificationService.setSuccessNotification('Änderungen erfolgreich gespeichert');
            },
            err => this.notificationService.setErrorNotification(err)
        );
  }

}
