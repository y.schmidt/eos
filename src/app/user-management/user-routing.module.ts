import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuardService } from '../shared/services/auth-guard.service';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { UserDataComponent } from './user-data/user-data.component';

const routes: Routes = [
    { path: 'profile', component: ProfileComponent, canActivate:[AuthGuardService], children: [
            { path: '', redirectTo: 'data', pathMatch: 'full' },
            { path: 'settings', component: UserSettingsComponent, canActivate:[AuthGuardService] },
            { path: 'data', component: UserDataComponent, canActivate:[AuthGuardService] },
        ]},
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class UserRoutingModule { }
