import { Component, OnInit } from '@angular/core';
import { ConfirmModalAdvancedComponent } from '../../shared/components/confirm-modal-advanced/confirm-modal-advanced.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UserService } from '../../shared/services/user.service';
import { User } from '../../shared/models/user';
import { NotificationService } from '../../shared/services/notification.service';
import { Router } from '@angular/router';

@Component({
    selector: 'eos-user-settings',
    templateUrl: './user-settings.component.html',
    styleUrls: ['./user-settings.component.css']
})
export class UserSettingsComponent implements OnInit {

    public user: User;
    private modalRef: BsModalRef;
    constructor(private modalService: BsModalService,
                private userService: UserService,
                private notificationService: NotificationService,
                private router: Router) { }

    ngOnInit() {
        this.getUser();
    }

    getUser(){
        this.userService.getUserById(this.userService.getUserId(), 'join=address')
            .subscribe(
                user => this.user = user,
                err => this.notificationService.setErrorNotification(err)
            );
    }

    openDeleteConfirmModal(){
        this.modalRef = this.modalService.show(ConfirmModalAdvancedComponent,{
            initialState: {
                object: this.user,
                name: this.user.firstName
            }
        });
        this.modalRef.content.confirmEvent.subscribe((user) => {
            this.userService.userDelete(user.id)
                .subscribe(() => {
                    this.router.navigate(['/start']);
                    this.modalRef.hide();
                }, error => {
                    this.notificationService.setErrorNotification(error);
                    this.modalRef.hide();
                });

        });
    }

}
