import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/services/user.service';

@Component({
    selector: 'eos-forget-password',
    templateUrl: './forget-password.component.html',
    styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {

    model: any = {};
    loading = false;
    errorMessage: string;
    error = false;
    success = false;

    constructor(private userService: UserService) { }

    ngOnInit() {
    }

    resetPassword(){
        this.userService.forgetPassword(this.model.mail)
            .subscribe(() => {
                this.success = true;
            });
    }

}
