import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../shared/services/user.service';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
    selector: 'eos-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

    private token;
    private mail;
    resetSuccess: boolean = false;
    password: any = {};
    constructor(private route: ActivatedRoute,
                private userService: UserService,
                private notificationService: NotificationService) { }

    ngOnInit() {
        this.token = this.route.snapshot.paramMap.get('token');
        this.mail = this.route.snapshot.paramMap.get('mail');
    }

    resetPassword(){
        this.userService.resetPassword(this.mail, this.token, this.password)
            .subscribe(() => {
                this.resetSuccess = true;
            },
                error => this.notificationService.setErrorNotification(error));
    }

}
