import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationDeletFailedComponent } from './registration-delet-failed.component';

describe('RegistrationDeletFailedComponent', () => {
  let component: RegistrationDeletFailedComponent;
  let fixture: ComponentFixture<RegistrationDeletFailedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationDeletFailedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationDeletFailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
