import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../../shared/models/user';
import { UserService } from '../../shared/services/user.service';
import { UserSystem } from '../../shared/models/userSystem';
import { NgForm } from '@angular/forms';
import { FormService } from '../../shared/services/form.service';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'eos-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

    user: User;
    registerSuccess = false;
    loading = false;
    errorMessage: string;
    error = false;
    @ViewChild('f')
    form: NgForm;

    constructor(private userService: UserService,
                private formService: FormService,
                private authGuardService: AuthGuardService,
                private route: ActivatedRoute) { }

    ngOnInit() {
        this.user = new User();
        this.route.queryParams.subscribe((params) => {
            this.user.mail = params['mail'];
        });
        this.user.userSystem = new UserSystem();
        this.authGuardService.destroyAuth();
    }

    register() {
        if(!this.form.form.valid){
            this.formService.markFormGroupTouched(this.form);
        } else {
            this.userService.register(this.user)
                .subscribe(() => {
                        this.registerSuccess = true;
                    },
                    error => {
                        console.log(error);
                        this.errorMessage = error.error;
                        this.error = true;
                        this.loading = false;
                    });
        }
    }
}