import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthGuardService } from '../../shared/services/auth-guard.service';

@Component({
    selector: 'eos-registration-success',
    templateUrl: './registration-success.component.html',
    styleUrls: ['./registration-success.component.css']
})
export class RegistrationSuccessComponent implements OnInit {

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                private authGuard: AuthGuardService) {}

    ngOnInit() {
        this.activatedRoute.queryParams.subscribe(params => {
            const auth = {
                token: params['token'],
                refreshToken: params['refreshToken']
            };
            this.authGuard.setAuth(auth);
            this.router.navigate(['/start-tour']);
        });
    }

}
