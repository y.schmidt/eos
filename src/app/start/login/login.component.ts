import { Component, OnInit } from '@angular/core';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { Router } from '@angular/router';

@Component({
    selector: 'eos-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    loading = false;
    model: any = {};
    errorMessage: string;
    error = false;

    constructor(private authGuardService: AuthGuardService,
                private router: Router) { }

    ngOnInit() {
        if(this.authGuardService.isLoggedIn()){
            this.router.navigate(['/dashboard']);
        } else {
            this.authGuardService.destroyAuth();
        }
    }

    login() {
        this.error = false;
        this.loading = true;
        this.authGuardService.login(this.model.user, this.model.password)
            .subscribe(
                () => {
                    this.router.navigate(['/dashboard']);
                },
                error => {
                    this.errorMessage = error.error;
                    this.error = true;
                    this.loading = false;
                });
    }
}
