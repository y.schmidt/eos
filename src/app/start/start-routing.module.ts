import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { RegistrationSuccessComponent } from './registration-success/registration-success.component';
import { RegistrationFailedComponent } from './registration-failed/registration-failed.component';
import { RegistrationDeletedComponent } from './registration-deleted/registration-deleted.component';
import { RegistrationDeletFailedComponent } from './registration-delet-failed/registration-delet-failed.component';

const routes: Routes = [
    { path: '', component: LandingPageComponent, children: [
            { path: '', redirectTo: 'login', pathMatch: 'full' },
            { path: 'login', component: LoginComponent },
            { path: 'register', component: RegisterComponent },
            { path: 'forgetPassword', component: ForgetPasswordComponent },
            { path: 'resetPassword/:token/:mail', component: ResetPasswordComponent },
            { path: 'registrationSuccess', component: RegistrationSuccessComponent },
            { path: 'registrationFailed', component: RegistrationFailedComponent },
            { path: 'registrationDeleted', component: RegistrationDeletedComponent },
            { path: 'registrationDeleteFailed', component: RegistrationDeletFailedComponent },
            { path: '**', redirectTo: 'login' }
        ]},

];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})

export class StartRoutingModule {}
