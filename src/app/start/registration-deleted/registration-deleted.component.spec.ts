import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationDeletedComponent } from './registration-deleted.component';

describe('RegistrationDeletedComponent', () => {
  let component: RegistrationDeletedComponent;
  let fixture: ComponentFixture<RegistrationDeletedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationDeletedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationDeletedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
