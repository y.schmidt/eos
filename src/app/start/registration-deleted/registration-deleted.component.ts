import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'eos-registration-deleted',
  templateUrl: './registration-deleted.component.html',
  styleUrls: ['./registration-deleted.component.css']
})
export class RegistrationDeletedComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
