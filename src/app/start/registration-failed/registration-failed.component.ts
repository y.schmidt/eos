import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'eos-registration-failed',
  templateUrl: './registration-failed.component.html',
  styleUrls: ['./registration-failed.component.css']
})
export class RegistrationFailedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
