import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { StartRoutingModule } from './start-routing.module';
import { SharedModule } from '../shared/shared.module';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { RegisterComponent } from './register/register.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { RegistrationSuccessComponent } from './registration-success/registration-success.component';
import { RegistrationFailedComponent } from './registration-failed/registration-failed.component';
import { RegistrationDeletedComponent } from './registration-deleted/registration-deleted.component';
import { RegistrationDeletFailedComponent } from './registration-delet-failed/registration-delet-failed.component';

@NgModule({
    imports: [
        CommonModule,
        StartRoutingModule,
        SharedModule,
    ],
    declarations: [
        LoginComponent,
        LandingPageComponent,
        RegisterComponent,
        ForgetPasswordComponent,
        ResetPasswordComponent,
        RegistrationSuccessComponent,
        RegistrationFailedComponent,
        RegistrationDeletedComponent,
        RegistrationDeletFailedComponent
    ],
    exports: [
        LoginComponent
    ]
})

export class StartModule { }
