import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { InformationComponent } from './information/information.component';
import { AuthGuardService } from '../shared/services/auth-guard.service';

const routes: Routes = [
  { path: '', component: InformationComponent, canActivate:[AuthGuardService]}
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})

export class DashboardRoutingModule {}



