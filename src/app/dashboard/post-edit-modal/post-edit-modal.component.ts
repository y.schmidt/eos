import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Post } from '../../shared/models/post';
import { PostService } from '../../shared/services/post.service';
import { NotificationService } from '../../shared/services/notification.service';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'eos-post-edit-modal',
  templateUrl: './post-edit-modal.component.html',
  styleUrls: ['./post-edit-modal.component.css']
})
export class PostEditModalComponent implements OnInit {

  post: Post;

  @Output()
  postUpdated: EventEmitter<any> = new EventEmitter<any>();
  constructor(public bsModalRef: BsModalRef,
              private postService: PostService,
              private notificationService: NotificationService) { }

  ngOnInit() {
  }

  savePost(){
    this.postService.editPost(this.post)
        .subscribe((post) => {
              this.post = post;
              this.postUpdated.emit(post);
              this.bsModalRef.hide();
            },
            error => this.notificationService.setErrorNotification(error)
        );
  }

    changePostValue(data: string){
        this.post.content = data;
    }

}
