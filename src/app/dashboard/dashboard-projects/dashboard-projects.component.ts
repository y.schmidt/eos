import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../../shared/services/project.service';
import { UserService } from '../../shared/services/user.service';
import { Project } from '../../shared/models/project';
import { NotificationService } from '../../shared/services/notification.service';
import saveAs from 'file-saver';

@Component({
    selector: 'eos-dashboard-projects',
    templateUrl: './dashboard-projects.component.html',
    styleUrls: ['./dashboard-projects.component.css']
})
export class DashboardProjectsComponent implements OnInit {

    projects: Project[];
    projectsLoaded: boolean = false;

    constructor(private projectService: ProjectService,
                private userService: UserService,
                private notificationService: NotificationService) { }

    ngOnInit() {
        this.getProjects();
    }

    getProjects(){
        this.projectService.getProjects(this.userService.getUserId(), false)
            .subscribe(projects => {
                    this.projects = projects;
                    this.projectsLoaded = true;
                },
                error => this.notificationService.setErrorNotification(error));
    }

    getPdf(project: Project) {
        this.projectService.getProjectPdfExport(project.ensembles[0].id, project.id)
            .subscribe(res => {
                saveAs(res, project.name + ' - Probenplan.pdf');
            });
    }

}
