import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Post } from '../../shared/models/post';
import { PostComment } from '../../shared/models/postComment';
import { PostService } from '../../shared/services/post.service';
import { NotificationService } from '../../shared/services/notification.service';
import { User } from '../../shared/models/user';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { PostEditModalComponent } from '../post-edit-modal/post-edit-modal.component';


@Component({
    selector: 'eos-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

    @Input()
    post: Post;

    @Input()
    currentUser: User;

    @Output()
    postDeleted: EventEmitter<any> = new EventEmitter();

    commentsLoaded: boolean = false;
    showComments: boolean = false;
    newComment: PostComment;
    commentCounter: number;
    modalRef: BsModalRef;


    constructor(private modalService: BsModalService,
                private postService: PostService,
                private notificationService: NotificationService) { }

    ngOnInit() {
        this.countComments();
        this.newComment = new PostComment(this.post);
    }

    getComments(){
        if(this.commentsLoaded){
            this.showComments = true;
        } else {
            this.postService.getCommentsForPost(this.post.id)
                .subscribe((comments) => {
                        this.post.comments = comments;
                        this.showComments = true;
                    },
                    error => this.notificationService.setErrorNotification(error)
                );
        }
    }

    createNewComment(){
        if(this.newComment.content && this.newComment.content != ''){
            this.newComment.post = this.post;
            this.postService.createPostComment(this.newComment)
                .subscribe((comment) => {
                        this.post.comments.unshift(comment);
                        this.commentCounter++;
                        this.newComment = new PostComment(this.post);
                    },
                    error => this.notificationService.setErrorNotification(error)
                );
        }
    }

    countComments(){
        this.postService.countPostComments(this.post.id)
            .subscribe((counter) => {
                this.commentCounter = counter.comments;
            },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    deletePost(post: Post){
        this.postService.deletePost(post.id)
            .subscribe(() => {
                this.postDeleted.emit(post);
                this.notificationService.setSuccessNotification('Post gelöscht');
            },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    commentDeleted(comment: PostComment){
        const index = this.post.comments.findIndex((elt) => (elt === comment));
        if (index !== -1) {
            this.post.comments.splice(index, 1);
        }
    }

    openEditModal(){
        this.modalRef = this.modalService.show(PostEditModalComponent, {
            ignoreBackdropClick: true,
            initialState: {
                post: Object.assign({}, this.post),
            }
        });
        this.modalRef.content.postUpdated
            .subscribe((updatedPost) => {
                this.post = updatedPost;
            });
    }

    onReady(event: any) {
        event.sourceElement.focus();
    }

}
