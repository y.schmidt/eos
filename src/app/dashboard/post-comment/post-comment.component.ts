import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { PostComment } from '../../shared/models/postComment';
import { User } from '../../shared/models/user';
import { PostService } from '../../shared/services/post.service';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
  selector: 'eos-post-comment',
  templateUrl: './post-comment.component.html',
  styleUrls: ['./post-comment.component.css']
})
export class PostCommentComponent implements OnInit {

  @Input()
  comment: PostComment;

  @Input()
  currentUser: User;

  @Output()
  commentDeleted: EventEmitter<any> = new EventEmitter();

  @ViewChild('commentInput')
  commentInput: ElementRef;

  editable: boolean = false;
  currentValue: string;

  constructor(private postService: PostService,
              private notificationService: NotificationService) { }

  ngOnInit() {
  }

  deleteComment(postComment: PostComment){
    console.log(postComment);
    this.postService.deleteComment(postComment.id)
        .subscribe(() => {
              this.commentDeleted.emit(postComment);
              this.notificationService.setSuccessNotification('Kommentar gelöscht');
            },
            error => this.notificationService.setErrorNotification(error)
        );
  }

  saveComment(){
    this.postService.editComment(this.comment)
        .subscribe((comment) => {
          this.comment = comment;
          this.editable = false;
        },
            error => this.notificationService.setErrorNotification(error)
        );
  }

  setEditable(){
    this.currentValue = this.comment.content;
    this.editable = true;
    setTimeout(()=>{ // this will make the execution after the above boolean has changed
      this.commentInput.nativeElement.focus();
    },0);
  }

  abortEdit(){
    this.comment.content = this.currentValue;
    this.editable = false;
  }

}
