import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InformationComponent } from './information/information.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ProjectManagementModule } from '../project-management/project-management.module';
import { DashboardProjectsComponent } from './dashboard-projects/dashboard-projects.component';
import { PinboardComponent } from './pinboard/pinboard.component';
import { PostComponent } from './post/post.component';
import { PostCommentComponent } from './post-comment/post-comment.component';
import { PostEditModalComponent } from './post-edit-modal/post-edit-modal.component';

@NgModule({
    imports: [
        CommonModule,
        DashboardRoutingModule,
        SharedModule,
        ProjectManagementModule
    ],
    declarations: [ InformationComponent, DashboardProjectsComponent, PinboardComponent, PostComponent, PostCommentComponent, PostEditModalComponent ],
    entryComponents: [
        PostEditModalComponent
    ]
})
export class DashboardModule { }
