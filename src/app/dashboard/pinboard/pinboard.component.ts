import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PostService } from '../../shared/services/post.service';
import { Post } from '../../shared/models/post';
import { NotificationService } from '../../shared/services/notification.service';
import { UserService } from '../../shared/services/user.service';
import { ProjectService } from '../../shared/services/project.service';
import { User } from '../../shared/models/user';
import { AuthGuardService } from '../../shared/services/auth-guard.service';

@Component({
    selector: 'eos-pinboard',
    templateUrl: './pinboard.component.html',
    styleUrls: ['./pinboard.component.css']
})
export class PinboardComponent implements OnInit {

    public editable: boolean = false;
    public newPost = new Post();
    posts: Post[];
    limit: number = 5;
    showMoreButton: boolean = true;
    currentUser: User;
    postsLoaded: boolean = false;

    selectedTarget: any;
    targetGroup: string;
    targetGroupName: string;
    targets: any[] = new Array<any>();

    constructor(private postService: PostService,
                private notificationService: NotificationService,
                private userService: UserService,
                private projectService: ProjectService,
                private authService: AuthGuardService) { }

    ngOnInit() {
        this.authService.authedUser.subscribe((user) => {
            this.currentUser = user;
        });
        this.getPosts();
    }

    getPosts(){
        this.postService.getPosts(this.limit)
            .subscribe((posts) => {
                    if(posts.length < this.limit){
                        this.showMoreButton = false;
                    }
                    this.posts = posts;
                    this.postsLoaded = true;
                }, error => this.notificationService.setErrorNotification(error)
            );
    }

    createPost(post: Post){
        if(post.content != ''){
            this.setPostTargetGroup(post);
            this.postService.createPost(post)
                .subscribe((post) => {
                        this.posts.unshift(post);
                        this.newPost = new Post();
                        this.editable = false;
                        this.notificationService.setSuccessNotification('Post erfolgreich erstellt');
                    }, error => this.notificationService.setErrorNotification(error)
                );
        }

    }

    setPostTargetGroup(post: Post){
        post.register = null;
        post.organization = null;
        post.ensemble = null;
        post.project = null;
        post.orgaGroup = null;
        switch(this.targetGroup){
            case 'register':
                post.register = this.selectedTarget;
                break;
            case 'project':
                post.project = this.selectedTarget;
                break;
            case 'ensemble':
                post.ensemble = this.selectedTarget;
                break;
            case 'organization':
                post.organization = this.selectedTarget;
                break;
            case 'orgaGroup':
                post.orgaGroup = this.selectedTarget;
                break;
        }
    }

    targetGroupChange(event){
        /*
        this.targetGroup = event.target.value;
        this.targetGroupName = event.target[event.target.selectedIndex].label;
        switch (event.target.value) {
          case 'register':
            this.targets = this.userService.userGetRegisters(this.userId);
            break;
          case 'project':
            this.targets = this.projectService.getProjects(this.userId);
            break;
          case 'ensemble':
            this.userService.userGetEnsembles(this.userId)
                .subscribe((ensembles) => {
                  const ensembleArray = new Array<any>();
                  ensembles.forEach((ensemble) => {
                    ensembleArray.push(ensemble.ensemble);
                  });
                });
            break;
        }
        this.selectedTarget = null;
        */
        this.targetGroupName = event.target[event.target.selectedIndex].label;
        this.targetGroup = event.target.value;
        this.targets = new Array<any>();
        switch (event.target.value) {
            case 'ensemble':
                this.userService.userGetEnsembles(this.currentUser.id)
                    .subscribe((ensembles) => {
                        ensembles.forEach((ensemble) => {
                            this.targets.push(ensemble.ensemble);
                        });
                    });
                break;
            case 'organization':
                this.userService.userGetOrganizations(this.currentUser.id)
                    .subscribe((orgas) => {
                        orgas.forEach((orga) => {
                            this.targets.push(orga.organization);
                        })
                    });
                break;
            case 'register':
                this.userService.userGetRegisters(this.currentUser.id)
                    .subscribe((registers) => {
                        this.targets = registers;
                    });
                break;
            case 'project':
                this.projectService.getProjects(this.currentUser.id)
                    .subscribe((projects) => {
                        this.targets = projects;
                    });
                break;
            case 'orgaGroup':
                this.userService.userGetOrgaGroups(this.currentUser.id)
                    .subscribe((orgaGroups) => {
                        this.targets = orgaGroups;
                        console.log(orgaGroups);
                    });
                break;
        }
        this.selectedTarget = null;
    }

    setLimit(){
        this.limit = this.limit+5;
        this.getPosts();
    }

    postDeleted(post: Post){
        const index = this.posts.findIndex((elt) => (elt === post));
        if (index !== -1) {
            this.posts.splice(index, 1);
        }
    }

    changePostValue(data: string){
        this.newPost.content = data;
    }
}