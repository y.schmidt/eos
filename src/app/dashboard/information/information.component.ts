import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../shared/services/notification.service';
import { Notification } from '../../shared/components/notification/notification';
import { EventService } from '../../shared/services/event.service';
import { Event } from '../../shared/models/event';

@Component({
    selector: 'app-information',
    templateUrl: './information.component.html',
    styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit {

    events: Event[];
    eventsLoaded: boolean = false;
    constructor(private notificationService: NotificationService,
                private eventService: EventService) { }

    ngOnInit() {
        this.getEvents();
    }

    getEvents(){
        this.eventService.getEventsForDashboard()
            .subscribe((events) => {
                this.events = events;
                this.eventsLoaded = true;
            },
                error => this.notificationService.setErrorNotification(error));
    }

    setMessage() {
        this.notificationService.setNotification(new Notification('danger', 'Someting went wrong', 'Test'));
    }

}
