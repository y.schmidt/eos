import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyOrganizationsComponent } from './my-organizations/my-organizations.component';
import { OrganizationRoutingModule } from './organization-routing.module';
import { OrganizationFormComponent } from './organization-form/organization-form.component';
import { SharedModule } from '../shared/shared.module';
import { OrganizationAddComponent } from './organization-add/organization-add.component';
import { OrganizationDashboardComponent } from './organization-dashboard/organization-dashboard.component';
import { OrganizationUsersComponent } from './organization-users/organization-users.component';
import { OrganizationCardComponent } from './organization-card/organization-card.component';
import { OrganizationSettingsComponent } from './organization-settings/organization-settings.component';
import { OrganizationGroupsComponent } from './organization-groups/organization-groups.component';
import { OrganizationOfficersComponent } from './organization-officers/organization-officers.component';
import { ShowProductsComponent } from './show-products/show-products.component';
import { CheckoutAbonnementComponent } from './checkout-abonnement/checkout-abonnement.component';
import { OrganizationAboComponent } from './organization-abo/organization-abo.component';
import { OrganizationAboChangeComponent } from './organization-abo-change/organization-abo-change.component';
import { CheckoutSuccessComponent } from './checkout-success/checkout-success.component';

@NgModule({
    imports: [
        CommonModule,
        OrganizationRoutingModule,
        SharedModule,
    ],
    declarations: [
        MyOrganizationsComponent,
        OrganizationFormComponent,
        OrganizationAddComponent,
        OrganizationDashboardComponent,
        OrganizationUsersComponent,
        OrganizationCardComponent,
        OrganizationSettingsComponent,
        OrganizationGroupsComponent,
        OrganizationOfficersComponent,
        ShowProductsComponent,
        CheckoutAbonnementComponent,
        OrganizationAboComponent,
        OrganizationAboChangeComponent,
        CheckoutSuccessComponent,
    ],
    exports: [
        OrganizationFormComponent
    ],
    entryComponents: [
        OrganizationAddComponent
    ]
})
export class OrganizationManagementModule { }
