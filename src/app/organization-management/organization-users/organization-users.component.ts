import { Component, OnInit, ViewChild } from '@angular/core';
import { Organization } from '../../shared/models/organization';
import { OrganizationService } from '../../shared/services/organization.service';
import { OrganizationUser } from '../../shared/models/organizationUser';
import { NotificationService } from '../../shared/services/notification.service';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AbonnementService } from '../../shared/services/abonnement.service';
import { Role } from '../../shared/models/role';

@Component({
    selector: 'eos-organization-users',
    templateUrl: './organization-users.component.html',
    styleUrls: ['./organization-users.component.css']
})
export class OrganizationUsersComponent implements OnInit {

    organization: Organization;
    abos: any;
    organizationUsers: OrganizationUser[];
    searchText: string;
    roles: Role[];

    @ViewChild('f')
    form: NgForm;
    constructor(private route: ActivatedRoute,
                private aboService: AbonnementService,
                private organizationService: OrganizationService,
                private notificationService: NotificationService) { }

    ngOnInit() {
        this.getOrganization();
    }

    getOrganization() {
        const id = this.route.parent.snapshot.paramMap.get('orgaId');
        this.organizationService.getOrganizationById(id)
            .subscribe((organization) => {
                this.organization = organization;
                this.getUsers();
                this.getOrganizationAbos();
                this.getOrganizationRoles();
            }, (error) => {
                this.notificationService.setErrorNotification(error);
            });
    }

    getUsers(){
        this.organizationService.getUserForOrganization(this.organization.id)
            .subscribe(
            organizationUsers => {
                this.organizationUsers = organizationUsers;
                },
                    error => this.notificationService.setErrorNotification(error)
            );
    }

    getOrganizationRoles(){
        this.organizationService.getOrganizationRoles(this.organization.id)
            .subscribe((roles) => {
                    this.roles = roles;
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    getOrganizationAbos(){
        this.aboService.getAbonnementsForOrganization(this.organization.id)
            .subscribe((abos) => {
                this.abos = abos;
            }, (error) => {
                this.notificationService.setErrorNotification(error);
            });
    }

    isOwner(organizationUser: OrganizationUser) {
        return this.organization.owner.id == organizationUser.user.id;
    }

    newAssignedUser(organizationUser: OrganizationUser){
        this.organizationUsers.push(organizationUser);
    }


    unassignUser(organizationUser: OrganizationUser){
        this.organizationService.unassignUser(this.organization.id, organizationUser.user.id)
            .subscribe(
            () => {
                const index = this.organizationUsers.findIndex((el) => (el.user.id === organizationUser.user.id));
                if (index !== -1) {
                    this.organizationUsers.splice(index, 1);
                }
                this.notificationService.setSuccessNotification('Benutzer wurde gelöscht');
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    changeRole(organizationUser: OrganizationUser){
        organizationUser.organization = this.organization;
        console.log(organizationUser);
        this.organizationService.updateOrganizationUser(organizationUser)
            .subscribe( () =>
                this.notificationService.setSuccessNotification('Benutzerrolle wurde geändert'),
                error => {
                this.notificationService.setErrorNotification(error);
                }
            );
    }

    getUserCount(){
        if(this.organizationUsers){
            return this.organizationUsers.length;
        }
    }

    getMaxUsers(){
        if(this.abos.name == 'Starter Abo'){
            return this.abos.maxUsers;
        } else {
            return this.abos[0].plan.product.metadata.maxUsers;
        }
    }
}
