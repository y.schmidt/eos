import { Component, Input, OnInit } from '@angular/core';
import { Organization } from '../../shared/models/organization';
import { AuthGuardService } from '../../shared/services/auth-guard.service';

@Component({
    selector: 'eos-organization-card',
    templateUrl: './organization-card.component.html',
    styleUrls: ['./organization-card.component.css']
})
export class OrganizationCardComponent implements OnInit {

    @Input()
    organization: Organization;

    isAdmin: boolean = false;

    constructor(private authGuard: AuthGuardService) { }

    ngOnInit() {
        this.isAdmin = this.authGuard.hasUserPermission('organizationAdmin', this.organization.id);
    }

}
