import { Component, OnInit } from '@angular/core';
import { User } from '../../shared/models/user';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from '../../shared/services/notification.service';
import { Organization } from '../../shared/models/organization';
import { OrganizationGroup } from '../../shared/models/organizationGroup';
import { OrganizationService } from '../../shared/services/organization.service';
import { OrganizationGroupService } from '../../shared/services/organization-group.service';

@Component({
  selector: 'eos-organization-groups',
  templateUrl: './organization-groups.component.html',
  styleUrls: ['./organization-groups.component.css']
})
export class OrganizationGroupsComponent implements OnInit {

    organization: Organization;

    organizationGroupUsers: User[];

    organizationGroups: OrganizationGroup[];
    newOrganizationGroup = new OrganizationGroup();
    constructor(private organizationGroupService: OrganizationGroupService,
                private route: ActivatedRoute,
                private organizationService: OrganizationService,
                private notificationService: NotificationService) { }

    ngOnInit() {
        this.getOrganization();
    }

    getOrganization() {
        const id = this.route.parent.snapshot.paramMap.get('orgaId');
        this.organizationService.getOrganizationById(id)
            .subscribe(
                organization => {
                    this.organization = organization;
                    this.getOrganizationGroups();
                    this.getOrganizationGroupUsers();
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    getOrganizationGroups(){
        this.organizationGroupService.getOrganizationGroupsForOrganization(this.organization.id)
            .subscribe((organizationGroups) => {
                this.organizationGroups = organizationGroups;
            });
    }

    getOrganizationGroupUsers(){
        this.organizationGroupService.getUsersForOrganizationGroups(this.organization.id)
            .subscribe((orgaGroupUsers) => {
                this.organizationGroupUsers = orgaGroupUsers;
            });
    }

    createOrganizationGroup(){
        this.newOrganizationGroup.organization = this.organization;
        console.log(this.newOrganizationGroup);
        this.organizationGroupService.createOrganizationGroup(this.newOrganizationGroup)
            .subscribe((organizationGroup) => {
                    this.organizationGroups.push(organizationGroup);
                    this.notificationService.setSuccessNotification('Gruppe erfolgreich angelegt');
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    updateOrganizationGroup(organizationGroup: OrganizationGroup, notification = true){
        organizationGroup.organization = this.organization;
        this.organizationGroupService.updateOrganizationGroup(organizationGroup)
            .subscribe((organizationGroup) => {
                    organizationGroup = organizationGroup;
                    if(notification){
                        this.notificationService.setSuccessNotification('Gruppe erfolgreich geändert');
                    }
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    removeOrganizationGroup(organizationGroup: OrganizationGroup){
        organizationGroup.organization = this.organization;
        this.organizationGroupService.removeOrganizationGroup(organizationGroup)
            .subscribe(() => {
                    const index = this.organizationGroups.findIndex((elt) => (elt === organizationGroup));
                    if (index !== -1) {
                        this.organizationGroups.splice(index, 1);
                    }
                    this.notificationService.setSuccessNotification('Gruppe erfolgreich gelöscht');
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    changeUserOrganizationGroup(user: User, organizationGroup: OrganizationGroup){
        if(this.isUserInOrganizationGroup(user.organizationGroups, organizationGroup)){
            this.organizationGroupService.unassignUser(user.id, organizationGroup.id, this.organization.id)
                .subscribe( () => {
                        const index = user.organizationGroups.findIndex((elt) => (elt === organizationGroup));
                        if (index !== -1) {
                            user.organizationGroups.splice(index, 1);
                        }
                        this.notificationService.setSuccessNotification('Benutzer erfolgreich aus Gruppe entfernt');
                    },
                    error => this.notificationService.setErrorNotification(error));
        } else {
            this.organizationGroupService.assignUser(user.id, organizationGroup.id, this.organization.id)
                .subscribe( () => {
                        if(user.organizationGroups == undefined){
                            user.organizationGroups = [organizationGroup];
                        } else {
                            user.organizationGroups.push(organizationGroup);

                        }
                        this.notificationService.setSuccessNotification('Benutzer erfolgreich zu Gruppe hinzugefügt');
                    },
                    error => this.notificationService.setErrorNotification(error));
        }
    }

    isUserInOrganizationGroup(userOrganizationGroups: OrganizationGroup[], organizationGroup: OrganizationGroup){
        if(userOrganizationGroups == undefined){
            return false;
        } else {
            let response = false;
            userOrganizationGroups.forEach((userOrganizationGroup) => {
                if(userOrganizationGroup.id == organizationGroup.id){
                    response = true;
                }
            });
            return response;
        }


    }


}
