import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Organization } from '../../shared/models/organization';
import { NgForm } from '@angular/forms';
import { FormService } from '../../shared/services/form.service';

@Component({
    selector: 'eos-organization-form',
    templateUrl: './organization-form.component.html',
    styleUrls: ['./organization-form.component.css']
})
export class OrganizationFormComponent implements OnInit {

    @Input()
    organization: Organization;

    @ViewChild('f', {static: false})
    form: NgForm;

    initialName: string;
    constructor(private formService: FormService) { }

    ngOnInit() {
        this.initialName = this.organization.name;
    }

    @Input()
    set touched(value: boolean){
        if(value){
            this.markAsTouched();
        }
    }

    markAsTouched(){
        this.formService.markFormGroupTouched(this.form);
    }
}
