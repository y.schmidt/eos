import { OrganizationManagementModule } from './organization-management.module';

describe('OrganizationManagementModule', () => {
  let organizationManagementModule: OrganizationManagementModule;

  beforeEach(() => {
    organizationManagementModule = new OrganizationManagementModule();
  });

  it('should create an instance', () => {
    expect(organizationManagementModule).toBeTruthy();
  });
});
