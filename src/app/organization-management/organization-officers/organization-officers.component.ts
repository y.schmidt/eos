import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { Ensemble } from '../../shared/models/ensemble';
import { EnsembleUser } from '../../shared/models/ensembleUser';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { User } from '../../shared/models/user';
import { EnsembleService } from '../../shared/services/ensemble.service';
import { NotificationService } from '../../shared/services/notification.service';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { Organization } from '../../shared/models/organization';
import { OrganizationUser } from '../../shared/models/organizationUser';
import { OrganizationService } from '../../shared/services/organization.service';

@Component({
  selector: 'eos-organization-officers',
  templateUrl: './organization-officers.component.html',
  styleUrls: ['./organization-officers.component.css']
})
export class OrganizationOfficersComponent implements OnInit {

    @Input()
    organization: Organization;

    organizationUsers: OrganizationUser[];
    modalRef: BsModalRef;
    confirmString: string;
    user: User;
    newOwner: User;
    constructor(private organizationService: OrganizationService,
                private modalService: BsModalService,
                private notificationService: NotificationService,
                private authService: AuthGuardService) { }

    ngOnInit() {
        this.getUsers();
        this.user = this.authService.getAuthedUser();
        this.newOwner = this.organization.owner;
    }

    getUsers(){
        this.organizationService.getUserForOrganization(this.organization.id)
            .subscribe((organizationUsers) => {
                    this.organizationUsers = organizationUsers;
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    changeOwner(){
        this.organization.owner = this.newOwner;
        this.organizationService.changeOwner(this.organization)
            .subscribe(() => {
                    this.notificationService.setSuccessNotification('Besitzer erfolgreich geändert!');
                    this.modalRef.hide();
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    openConfirmModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    compareFn(c1: any, c2: any): boolean {
        return c1 && c2 ? c1.id === c2.id : c1 === c2;
    }

}
