import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationOfficersComponent } from './organization-officers.component';

describe('OrganizationOfficersComponent', () => {
  let component: OrganizationOfficersComponent;
  let fixture: ComponentFixture<OrganizationOfficersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganizationOfficersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationOfficersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
