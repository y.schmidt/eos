import { Component, OnInit } from '@angular/core';
import { Organization } from '../../shared/models/organization';
import { ActivatedRoute } from '@angular/router';
import { OrganizationService } from '../../shared/services/organization.service';
import { NotificationService } from '../../shared/services/notification.service';
import { SharedService } from '../../shared/services/shared.service';
import { AbonnementService } from '../../shared/services/abonnement.service';

@Component({
    selector: 'eos-organization-dashboard',
    templateUrl: './organization-dashboard.component.html',
    styleUrls: ['./organization-dashboard.component.css']
})
export class OrganizationDashboardComponent implements OnInit {

    organizationId: string;
    organization: Organization;
    aboStatus: string;

    constructor(private route: ActivatedRoute,
                private organizationService: OrganizationService,
                private notificationService: NotificationService,
                private aboService: AbonnementService,
                private sharedService: SharedService) { }

    ngOnInit() {
        this.organizationId = this.route.snapshot.paramMap.get('orgaId');
        this.getOrganization();
        this.getAboStatus();
        this.sharedService.changeEmitted
            .subscribe((orga) => {
                this.organization = orga;
            });
    }

    getOrganization() {
        this.organizationService.getOrganizationById(this.organizationId)
            .subscribe((organization) => {
                this.organization = organization;
            }, (error) => {
                this.notificationService.setErrorNotification(error)
            });
    }

    getAboStatus(){
        this.aboService.getAboStatusForOrganization(this.organizationId)
            .subscribe((status) => {
                this.aboStatus = status.status;
            }, (error) => {
                this.notificationService.setErrorNotification(error)
            });
    }
}
