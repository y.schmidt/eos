import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbonnementService } from '../../shared/services/abonnement.service';
import { NotificationService } from '../../shared/services/notification.service';
import { Product } from '../../shared/models/product';
import { ActivatedRoute } from '@angular/router';
import { Abonnement } from '../../shared/models/abonnement';

@Component({
    selector: 'eos-show-products',
    templateUrl: './show-products.component.html',
    styleUrls: ['./show-products.component.css']
})
export class ShowProductsComponent implements OnInit {

    productPeriod: string = 'month';
    products: any[];

    organizationId: string;
    actualOrgaAbos: any[] | any;
    actualMetadata: any;

    isNewAbo: boolean = false;

    @Output()
    aboCheckoutEmitter: EventEmitter<any> = new EventEmitter<any>();

    constructor(private aboService: AbonnementService,
                private notificationService: NotificationService,
                private route: ActivatedRoute) { }

    ngOnInit() {
        this.organizationId = this.route.parent.snapshot.paramMap.get('orgaId');
        this.getProducts();
        this.getOrganizationAbo();
        this.getOrganizationMetadata();
    }

    getProducts(){
        this.aboService.getProducts()
            .subscribe((products) => {
                this.products = products;
                }, (error) => {
                this.notificationService.setErrorNotification(error);
            });
    }

    getOrganizationAbo(){
        this.aboService.getAbonnementsForOrganization(this.organizationId)
            .subscribe((abos) => {
                console.log(abos);
                this.actualOrgaAbos = abos;
                this.isNewAbo = this.actualOrgaAbos.name == 'Starter Abo'
            }, (error) => {
                this.notificationService.setErrorNotification(error);
            });
    }

    getOrganizationMetadata(){
        this.aboService.getActualAboMetadataForOrganization(this.organizationId)
            .subscribe((metadata) => {
                this.actualMetadata = metadata;
            }, (error) => {
                this.notificationService.setErrorNotification(error);
            });
    }

    aboCheckout(plan){
        this.aboCheckoutEmitter.emit(plan);
    }

    isActualPlan(plan): boolean{
        if(this.actualOrgaAbos.name == 'Starter Abo'){
            return false;
        } else {
            let result = false;
            for(let i = 0; this.actualOrgaAbos.length > i; i++){
                if(this.actualOrgaAbos[i].status == 'active' && plan.id == this.actualOrgaAbos[i].plan.id){
                    result = true;
                    break;
                }
            }
            return result;
        }
    }

    isAboPossible(productMetadata): boolean{
        return productMetadata.maxUsers >= this.actualMetadata.users && productMetadata.maxEnsembles >= this.actualMetadata.ensembles;
    }
}
