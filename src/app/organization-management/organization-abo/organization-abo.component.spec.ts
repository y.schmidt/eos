import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationAboComponent } from './organization-abo.component';

describe('ChangeAbonnementComponent', () => {
  let component: OrganizationAboComponent;
  let fixture: ComponentFixture<OrganizationAboComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganizationAboComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationAboComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
