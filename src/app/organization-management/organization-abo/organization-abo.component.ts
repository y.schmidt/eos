import { Component, OnInit, TemplateRef } from '@angular/core';
import { AbonnementService } from '../../shared/services/abonnement.service';
import { NotificationService } from '../../shared/services/notification.service';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
    selector: 'eos-organization-abo',
    templateUrl: './organization-abo.component.html',
    styleUrls: ['./organization-abo.component.css']
})
export class OrganizationAboComponent implements OnInit {

    organizationId: string;
    abos: any[] | any;
    futureAbo: any;
    invoices: any[];

    modalRef: BsModalRef;
    hasFutureAbo: boolean = false;

    constructor (private aboService: AbonnementService,
                 private route: ActivatedRoute,
                 private modalService: BsModalService,
                 private notificationService: NotificationService) {}

    ngOnInit () {
        this.organizationId = this.route.parent.snapshot.paramMap.get('orgaId');
        this.getOrganizationAbo();
        this.getOrganizationFutureAbos();
        this.getInvoices();
    }

    getOrganizationAbo () {
        this.aboService.getAbonnementsForOrganization(this.organizationId)
            .subscribe((abos) => {
                console.log(abos);
                this.abos = abos;
            }, (error) => {
                this.notificationService.setErrorNotification(error);
            });
    }

    getOrganizationFutureAbos () {
        this.aboService.getFutureAbosForOrganization(this.organizationId)
            .subscribe((abo) => {
                console.log(abo);
                this.futureAbo = abo;
                this.hasFutureAbo = this.futureAbo != null;
            }, (error) => {
                this.notificationService.setErrorNotification(error);
            });
    }

    getInvoices () {
        this.aboService.getInvoicesForOrganization(this.organizationId)
            .subscribe((invoices) => {
                console.log(invoices);
                if(invoices){
                    this.invoices = invoices;
                } else {
                    this.invoices = [];
                }
            }, (error) => {
                this.notificationService.setErrorNotification(error);
            });
    }

    getInvoiceForSubscription(subId: string){
        return this.invoices.find((invoice) => invoice.subscription == subId);
    }


    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    cancelAbo(){
        this.aboService.cancelStripeSubscription({ organizationId: this.organizationId })
            .subscribe(() => {
                this.getOrganizationAbo();
                this.modalRef.hide();
                this.notificationService.setSuccessNotification('Abo erfolgreich gekündigt');
            })
    }

    cancelCancellation(){
        this.aboService.cancelCancellationStripeSubscription({ organizationId: this.organizationId })
            .subscribe(() => {
                this.getOrganizationAbo();
                this.modalRef.hide();
                this.notificationService.setSuccessNotification('Kündigung storniert');
            })
    }

    cancelFutureAbo(){
        const subData = {
            organizationId: this.organizationId,
            scheduleId: this.futureAbo.id
        }
        this.aboService.cancelStripeSubscriptionSchedule(subData)
            .subscribe(() => {
                this.getOrganizationAbo();
                this.getOrganizationFutureAbos();
                this.modalRef.hide();
            })
    }
}