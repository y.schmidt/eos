import { Component, OnInit } from '@angular/core';
import { StepperService } from '../../shared/services/stepper.service';
import { Organization } from '../../shared/models/organization';
import { ActivatedRoute } from '@angular/router';
import { OrganizationService } from '../../shared/services/organization.service';
import { NotificationService } from '../../shared/services/notification.service';
import { AbonnementService } from '../../shared/services/abonnement.service';
import { Observable } from 'rxjs';
import { DiscardChangesComponent } from '../../shared/components/discard-changes/discard-changes.component';
import { CanComponentDeactivate } from '../../shared/services/discard-changes.guard';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'eos-organization-abo-change',
  templateUrl: './organization-abo-change.component.html',
  styleUrls: ['./organization-abo-change.component.css']
})
export class OrganizationAboChangeComponent implements OnInit, CanComponentDeactivate {

  organizationId: string;
  organization: Organization;
  actualOrgaAbos: any[] | any;

  selectedPlan: any;
  payedSubscription: any;

  modalRef: BsModalRef;

  constructor(private stepperService: StepperService,
              private route: ActivatedRoute,
              private modalService: BsModalService,
              private orgaService: OrganizationService,
              private notificationService: NotificationService,
              private aboService: AbonnementService
  ) { }

  ngOnInit(): void {
    this.organizationId = this.route.parent.snapshot.paramMap.get('orgaId');
    this.getOrganization();
    this.getOrganizationAbo();
  }

  aboCheckout(plan){
    this.selectedPlan = plan;
    this.stepperService.nextStep();
  }

  subscriptionPayed(subscription){
    this.payedSubscription = subscription;
  }

  getOrganization(){
    this.orgaService.getOrganizationById(this.organizationId)
        .subscribe((orga) => {
          this.organization = orga;
        }, (error) => {
          this.notificationService.setErrorNotification(error)
        });
  }

  getOrganizationAbo(){
    this.aboService.getAbonnementsForOrganization(this.organizationId)
        .subscribe((abos) => {
          this.actualOrgaAbos = abos;
        }, (error) => {
          this.notificationService.setErrorNotification(error);
        });
  }

  canDeactivate(): Observable<boolean> | boolean {
    if(this.selectedPlan == null || this.payedSubscription != null){
      return true;
    } else {
      this.modalRef = this.modalService.show(DiscardChangesComponent);
      this.modalRef.content.confirmEvent.subscribe(() => {
        this.modalRef.hide();
      });
      return this.modalRef.content.confirmEvent;
    }
  }

}
