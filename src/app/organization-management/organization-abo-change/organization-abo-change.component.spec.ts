import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationAboChangeComponent } from './organization-abo-change.component';

describe('OrganizationAboChangeComponent', () => {
  let component: OrganizationAboChangeComponent;
  let fixture: ComponentFixture<OrganizationAboChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganizationAboChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationAboChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
