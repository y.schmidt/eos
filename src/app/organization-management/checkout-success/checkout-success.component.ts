import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'eos-checkout-success',
  templateUrl: './checkout-success.component.html',
  styleUrls: ['./checkout-success.component.css']
})
export class CheckoutSuccessComponent implements OnInit {

  @Input()
  payedSubscription: any;

  constructor() { }

  ngOnInit(): void {
  }

}
