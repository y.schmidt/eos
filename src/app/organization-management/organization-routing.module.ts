import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../shared/services/auth-guard.service';
import { MyOrganizationsComponent } from './my-organizations/my-organizations.component';
import { OrganizationDashboardComponent } from './organization-dashboard/organization-dashboard.component';
import { OrganizationSettingsComponent } from './organization-settings/organization-settings.component';
import { OrganizationUsersComponent } from './organization-users/organization-users.component';
import { OrganizationGroupsComponent } from './organization-groups/organization-groups.component';
import { ShowProductsComponent } from './show-products/show-products.component';
import { OrganizationAboComponent } from './organization-abo/organization-abo.component';
import { OrganizationAboChangeComponent } from './organization-abo-change/organization-abo-change.component';
import { CheckoutSuccessComponent } from './checkout-success/checkout-success.component';
import { DiscardChangesGuard } from '../shared/services/discard-changes.guard';

const routes: Routes = [
    { path: '', component: MyOrganizationsComponent, canActivate:[AuthGuardService] },
    { path: ':orgaId', component: OrganizationDashboardComponent, canActivate:[AuthGuardService], children: [
        { path: '', redirectTo: 'members', pathMatch: 'full' },
        { path: 'settings', component: OrganizationSettingsComponent, canActivate:[AuthGuardService] },
        { path: 'members', component: OrganizationUsersComponent, canActivate:[AuthGuardService] },
        { path: 'groups', component: OrganizationGroupsComponent, canActivate:[AuthGuardService] },
        { path: 'abo', component: OrganizationAboComponent, canActivate:[AuthGuardService] },
        { path: 'checkoutSuccess', component: CheckoutSuccessComponent, canActivate:[AuthGuardService] },
        { path: 'abo/add', component: ShowProductsComponent, canActivate:[AuthGuardService] },
        { path: 'abo/change', component: OrganizationAboChangeComponent, canActivate:[AuthGuardService], canDeactivate:[DiscardChangesGuard] },
        ]},
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class OrganizationRoutingModule { }
