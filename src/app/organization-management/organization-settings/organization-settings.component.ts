import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Organization } from '../../shared/models/organization';
import { NotificationService } from '../../shared/services/notification.service';
import { OrganizationService } from '../../shared/services/organization.service';
import { ActivatedRoute, Router } from '@angular/router';
import { OrganizationFormComponent } from '../organization-form/organization-form.component';
import { SharedService } from '../../shared/services/shared.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ConfirmModalAdvancedComponent } from '../../shared/components/confirm-modal-advanced/confirm-modal-advanced.component';

@Component({
    selector: 'eos-organization-settings',
    templateUrl: './organization-settings.component.html',
    styleUrls: ['./organization-settings.component.css']
})
export class OrganizationSettingsComponent implements OnInit {

    @Input()
    organization: Organization;

    @ViewChild(OrganizationFormComponent)
    private formComp: OrganizationFormComponent;
    private modalRef: BsModalRef;

    constructor(private route: ActivatedRoute,
                private notificationService: NotificationService,
                private organizationService: OrganizationService,
                private sharedService: SharedService,
                private modalService: BsModalService,
                private router: Router) { }

    ngOnInit() {
        this.getOrganization();
    }

    getOrganization() {
        const id = this.route.parent.snapshot.paramMap.get('orgaId');
        this.organizationService.getOrganizationById(id)
            .subscribe((organization) => {
                this.organization = organization;
            }, (error) => {
                this.notificationService.setErrorNotification(error)
            });
    }

    save() {
        if(this.formComp.form.form.valid){
            this.organizationService.updateOrganization(this.organization)
                .subscribe((organization) => {
                    this.organization = organization;
                    this.sharedService.emitChange(organization);
                    this.notificationService.setSuccessNotification('Organisation erfolgreich gespeichert');
                }, (error) => {
                    this.notificationService.setErrorNotification(error);
                });

        } else {
           this.formComp.markAsTouched();
        }
    }

    openDeleteConfirmModal(){
        this.modalRef = this.modalService.show(ConfirmModalAdvancedComponent,{
            initialState: {
                object: this.organization,
            }
        });
        this.modalRef.content.confirmEvent.subscribe((organization) => {
            this.organizationService.deleteOrganization(organization.id)
                .subscribe(() => {
                    this.router.navigate(['/organizations']);
                    this.modalRef.hide();
                });

        });
    }

}
