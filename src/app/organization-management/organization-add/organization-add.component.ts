import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Organization } from '../../shared/models/organization';
import { Address } from '../../shared/models/address';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { OrganizationService } from '../../shared/services/organization.service';
import { OrganizationFormComponent } from '../organization-form/organization-form.component';
import { NotificationService } from '../../shared/services/notification.service';
import { AuthGuardService } from '../../shared/services/auth-guard.service';

@Component({
    selector: 'eos-organization-add',
    templateUrl: './organization-add.component.html',
    styleUrls: ['./organization-add.component.css']
})
export class OrganizationAddComponent implements OnInit {

    organization: Organization = new Organization();
    error: string;
    @Output()
    newOrganization: EventEmitter<any> = new EventEmitter<any>();


    @ViewChild(OrganizationFormComponent)
    private formComp: OrganizationFormComponent;

    constructor(public bsModalRef: BsModalRef,
                private organizationService: OrganizationService,
                private notificationService: NotificationService,
                private authGuardService: AuthGuardService
                ) { }

    ngOnInit() {}

    createOrganization(){
        if(this.formComp.form.form.valid){
            this.organizationService.createOrganization(this.organization)
                .subscribe((organization) => {
                    this.authGuardService.renewToken()
                        .subscribe(() => {
                            this.newOrganization.emit(organization);
                            this.bsModalRef.hide();
                        },error => this.notificationService.setErrorNotification(error))
                }, (error) => {
                    this.notificationService.setErrorNotification(error);
                });
        } else {
            this.formComp.markAsTouched();
        }
    }
}