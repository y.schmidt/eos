import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { OrganizationAddComponent } from '../organization-add/organization-add.component';
import { NotificationService } from '../../shared/services/notification.service';
import { Organization } from '../../shared/models/organization';

@Component({
    selector: 'eos-organization-dashboard',
    templateUrl: './my-organizations.component.html',
    styleUrls: ['./my-organizations.component.css']
})
export class MyOrganizationsComponent implements OnInit {

    organizations: Organization[];
    modalRef: BsModalRef;

    constructor(private userService: UserService,
                private modalService: BsModalService,
                private notificationService: NotificationService) { }

    ngOnInit() {
        this.getUserOrganizations();
    }

    getUserOrganizations(){
        this.userService.userGetOrganizationsWithStats(this.userService.getUserId())
            .subscribe(
                organizations => this.organizations = organizations,
                error => this.notificationService.setErrorNotification(error)
            );
    }

    addOrganizationModal(){
        this.modalRef = this.modalService.show(OrganizationAddComponent);
        this.modalRef.content.newOrganization.subscribe(() => this.getUserOrganizations());
    }

}
