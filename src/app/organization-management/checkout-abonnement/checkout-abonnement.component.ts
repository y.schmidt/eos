import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AbonnementService } from '../../shared/services/abonnement.service';
import { Organization } from '../../shared/models/organization';
import { OrganizationService } from '../../shared/services/organization.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '../../shared/services/notification.service';
import { Invoice } from '../../shared/models/invoice';
import { loadStripe } from '@stripe/stripe-js/pure';
import { Stripe } from '@stripe/stripe-js';
import { AuthGuardService } from '../../shared/services/auth-guard.service';
import { environment } from '../../../environments/environment';
import differenceInDays from 'date-fns/differenceInDays'
import { StepperService } from '../../shared/services/stepper.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { DiscardChangesComponent } from '../../shared/components/discard-changes/discard-changes.component';

@Component({
    selector: 'eos-checkout-abonnement',
    templateUrl: './checkout-abonnement.component.html',
    styleUrls: ['./checkout-abonnement.component.css']
})
export class CheckoutAbonnementComponent implements OnInit, AfterViewInit {

    @Input()
    productPlan: any;

    @Input()
    organization: Organization;

    @Input()
    actualOrgaAbo: any[] | any;

    invoice: Invoice = new Invoice();

    @ViewChild('cardElementRef')
    cardElementRef: ElementRef;

    @ViewChild('ibanElementRef')
    ibanElementRef: ElementRef;

    @ViewChild('aboErrorModal')
    aboErrorModal: ElementRef;

    @Output()
    subscriptionEmitter: EventEmitter<any> = new EventEmitter<any>();

    checkoutError: any;

    stripe: Stripe;
    elements;
    cardElement;
    ibanElement;


    paymentMethods: any[];
    selectedPaymentMethod: any;
    checkoutLoading: boolean = false;
    agbCheck: boolean = false;

    style = {
        base: {
            iconColor: '#422134',
            color: '#31325f',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4',
            },
            ':-webkit-autofill': {
                color: '#422134',
            },
        },
    };

    modalRef: BsModalRef;

    constructor(private aboService: AbonnementService,
                private orgaService: OrganizationService,
                private route: ActivatedRoute,
                private elementRef: ElementRef,
                private authGuard: AuthGuardService,
                private stepperService: StepperService,
                private modalService: BsModalService,
                private notificationService: NotificationService) { }

    ngOnInit(): void {
        this.setInvoiceData(this.organization);
        this.getPaymentMethods();
    }

    ngAfterViewInit (): void {
        this.initStripe();
    }

    getPaymentMethods(){
        this.aboService.getPaymentMethods(this.organization.id)
            .subscribe((methods) => {
                this.paymentMethods = methods;
                console.log(methods);
            })
    }

    setInvoiceData(organization: Organization){
        this.invoice.firstName = organization.owner.firstName;
        this.invoice.lastName = organization.owner.lastName;
        this.invoice.address = Object.assign( {}, organization.address);
        delete this.invoice.address.id;
        this.invoice.organization = organization;
        this.invoice.productPlan = this.productPlan;
    }

    lastStep(){
        this.stepperService.lastStep();
    }

    calculateAboDifference(){
        const daysRemaining = differenceInDays(new Date(this.actualOrgaAbo.current_period_end * 1000), new Date());
    }

    async initStripe () {
        this.stripe = await loadStripe(environment.stripeApiKey);
        this.elements = this.stripe.elements();
        this.cardElement = this.elements.create("card", { style: this.style });
        this.cardElement.mount(this.cardElementRef.nativeElement);
        /*this.ibanElement = this.elements.create('iban', { style: this.style, supportedCountries: ['SEPA']});
        this.ibanElement.mount(this.ibanElementRef.nativeElement);*/
        this.cardElement.addEventListener('change', function(event) {
            const displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });
        /*
        this.ibanElement.addEventListener('change', function(event) {
            const displayError = document.getElementById('iban-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

         */

    }

    async submitStripeSubscription(){
        this.checkoutLoading = true;
        const paymentMethod = await this.createPaymentMethod();
        if(paymentMethod.error){
            this.checkoutError = paymentMethod.error;
            this.modalRef = this.modalService.show(this.aboErrorModal);
        } else {
            this.setupStripePayment(paymentMethod);
        }
    }

    setupStripePayment(paymentMethod){
        const subData = {
            organizationId: this.organization.id,
            plan: this.productPlan,
            customer: this.getCustomer(paymentMethod),
            paymentMethod: paymentMethod.paymentMethod
        };
        this.aboService.setupStripePayment(subData)
            .subscribe(
                (subscription) => this.paymentSuccess(subscription),
                (error) => this.paymentError(error)
            );
    }

    createPaymentMethod(){
        if(this.selectedPaymentMethod == 'new' || this.selectedPaymentMethod == null){
            return this.stripe.createPaymentMethod({
                type: 'card',
                card: this.cardElement,
                billing_details: {
                    name: `${this.invoice.firstName} ${this.invoice.lastName}`,
                    email: this.authGuard.getAuthedUser().mail,
                    address: {
                        city: this.invoice.address.city,
                        postal_code: this.invoice.address.postalCode,
                        line1: `${this.invoice.address.street} ${this.invoice.address.number}`,
                        line2: this.invoice.address.custom
                    }
                },
            });
        } else {
            return { paymentMethod: this.selectedPaymentMethod, error: null };
        }
    }

    paymentSuccess(paymentResult){
        this.checkoutLoading = false;
        let latest_invoice, payment_intent;
        switch (paymentResult.object) {
            case 'subscription':
                latest_invoice = paymentResult.latest_invoice;
                payment_intent = latest_invoice.payment_intent;
                break;
            case 'invoice':
                latest_invoice = paymentResult;
                payment_intent = latest_invoice.payment_intent;
                break;
            case 'subscription_schedule':
                payment_intent = null;
        }

        if (payment_intent) {
            const { status, client_secret} = payment_intent;
            switch (status) {
                case 'requires_action':
                    this.stripe.confirmCardPayment(client_secret)
                        .then((result) => {
                            if (result.error) {
                                this.checkoutError = result.error;
                                this.modalRef = this.modalService.show(this.aboErrorModal);
                            } else {
                                this.subscriptionEmitter.emit(paymentResult);
                                this.stepperService.nextStep();
                            }
                        });
                    break;
                case 'requires_payment_method':
                    this.checkoutError = 'Leider wurde deine Zahlung abgelehnt. Bitte versuche es noch einmal mit anderen Daten.';
                    this.modalRef = this.modalService.show(this.aboErrorModal);
                    break;
                default:
                    this.subscriptionEmitter.emit(paymentResult);
                    this.stepperService.nextStep();
                    break;
            }
        } else {
            this.subscriptionEmitter.emit(paymentResult);
            this.stepperService.nextStep();
        }
    }

    paymentError(error){
        this.checkoutLoading = false;
        this.checkoutError = error;
        this.modalRef = this.modalService.show(this.aboErrorModal);
        this.notificationService.setErrorNotification(error)
    }

    getCustomer(input){
        if(this.organization.stripeCustomerId){
            return this.organization.stripeCustomerId;
        } else {
            return {
                name: input.paymentMethod.billing_details.name,
                email: input.paymentMethod.billing_details.email,
                address: input.paymentMethod.billing_details.address,
                description: this.organization.name
            }
        }
    }
}
