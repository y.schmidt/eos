import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Invite } from '../models/invite';

@Injectable({
    providedIn: 'root'
})
export class InviteService {

    private apiUrl = environment.apiUrl;
    constructor(private http: HttpClient) { }

    deleteInvite(organizationId: string, inviteId: string): Observable<any>{
        return this.http.delete<any>(`${this.apiUrl}/organizations/${organizationId}/invites/${inviteId}`);
    }

    reinviteUser(organizationId: string, invite: Invite): Observable<Invite>{
        return this.http.put<Invite>(`${this.apiUrl}/organizations/${organizationId}/invites`, invite);
    }

    getInvitesForOrganization(orgaId: string): Observable<Invite[]>{
        return this.http.get<Invite[]>(`${this.apiUrl}/organizations/${orgaId}/invites`);
    }

    getInvitesForEnsemble(ensembleId: string): Observable<Invite[]>{
        return this.http.get<Invite[]>(`${this.apiUrl}/ensembles/${ensembleId}/invites`);
    }

    inviteUser(invite: Invite): Observable<any>{
        return this.http.post<any>(`${this.apiUrl}/organizations/${invite.organization.id}/invites`, invite);
    }
}
