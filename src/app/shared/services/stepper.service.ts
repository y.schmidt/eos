import { EventEmitter, Injectable, Output } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StepperService {

  nextStepEmitter: EventEmitter<any> = new EventEmitter<any>();
  lastStepEmitter: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  nextStep(){
    this.nextStepEmitter.emit();
  }

  lastStep(){
    this.lastStepEmitter.emit();
  }
}
