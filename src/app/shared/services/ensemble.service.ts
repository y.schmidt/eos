import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Ensemble } from '../models/ensemble';
import { EnsembleUser } from '../models/ensembleUser';
import { User } from '../models/user';
import { environment } from '../../../environments/environment';
import { Role } from '../models/role';
import { Official } from '../models/official';

@Injectable({
    providedIn: 'root'
})
export class EnsembleService {

    private serviceUrl = environment.apiUrl + '/ensembles';
    private apiUrl = environment.apiUrl;

    constructor(private http: HttpClient) { }

    getEnsembleById(id: string, query?: string): Observable<Ensemble>{
        return this.http.get<Ensemble>(`${this.serviceUrl}/${id}?${query}`);
    }

    createEnsemble(ensemble: Ensemble): Observable<Ensemble>{
        return this.http.post<Ensemble>(`${this.apiUrl}/organizations/${ensemble.organization.id}/ensembles`, ensemble);
    }

    getUserForEnsemble(id: string): Observable<EnsembleUser[]>{
        return this.http.get<EnsembleUser[]>(`${this.serviceUrl}/${id}/users`);
    }

    getUserForOrganization(ensembleId: string, orgaId: string): Observable<User[]>{
        return this.http.get<User[]>(`${this.serviceUrl}/${ensembleId}/organizations/${orgaId}/users`);
    }

    assignUser(ensembleId: string, userId: string): Observable<EnsembleUser> {
        return this.http.post<EnsembleUser>(`${this.serviceUrl}/${ensembleId}/users/${userId}`, null);
    }

    unassignUser(ensembleId: string, userId: string): Observable<any> {
        return this.http.delete<any>(`${this.serviceUrl}/${ensembleId}/users/${userId}`);
    }

    updateEnsembleUser(ensembleUser: EnsembleUser): Observable<EnsembleUser>{
        return this.http.put<EnsembleUser>(`${this.serviceUrl}/${ensembleUser.ensemble.id}/users`, ensembleUser);
    }

    deleteEnsemble(ensembleId: string): Observable<any>{
        return this.http.delete(`${this.serviceUrl}/${ensembleId}`);
    }

    updateEnsemble(ensemble: Ensemble): Observable<Ensemble>{
        return this.http.put<Ensemble>(`${this.serviceUrl}`, ensemble);
    }

    changeOwner(ensemble: Ensemble): Observable<any>{
        return this.http.put<any>(`${this.serviceUrl}/owner`, ensemble);
    }

    getEnsembleRoles(ensembleId: string): Observable<Role[]>{
        return this.http.get<Role[]>(`${this.serviceUrl}/${ensembleId}/roles`);
    }

    getEnsembleOfficials(ensembleId: string): Observable<Official[]>{
        return this.http.get<Official[]>(`${this.serviceUrl}/${ensembleId}/officials`);
    }

    deleteEnsembleOfficial(ensembleId: string, officialId: string): Observable<any>{
        return this.http.delete(`${this.serviceUrl}/${ensembleId}/officials/${officialId}`);
    }

    createEnsembleOfficial(ensembleId: string, official: Official): Observable<Official>{
        return this.http.post<Official>(`${this.serviceUrl}/${ensembleId}/officials`, official);
    }

    updateEnsembleOfficial(ensembleId: string, official: Official): Observable<Official>{
        return this.http.put<Official>(`${this.serviceUrl}/${ensembleId}/officials`, official);
    }
}
