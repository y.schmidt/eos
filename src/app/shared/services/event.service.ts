import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Event } from '../models/event';
import { map } from 'rxjs/operators'
import { Register } from '../models/register';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class EventService {

    apiUrl = environment.apiUrl;

    constructor(private http: HttpClient) { }

    createEvent(event: Event, ensembleId: string): Observable<Event>{
        let url = `${this.apiUrl}/ensembles/${ensembleId}/projects/${event.project.id}`;
        if(event.eventgroup != undefined){
            url += `/eventgroup/${event.eventgroup.id}`
        }
        url += `/events`;
        return this.http.post<Event>(url, event)
            .pipe(map(this.convertEventDate));
    }

    createSerialEvent(event: Event, ensembleId: string): Observable<Event[]>{
        let url = `${this.apiUrl}/ensembles/${ensembleId}/projects/${event.project.id}`;
        if(event.eventgroup != undefined){
            url += `/eventgroup/${event.eventgroup.id}`
        }
        url += `/events/serial`;
        return this.http.post<Event[]>(url, event)
            .pipe(
                map(this.convertEventsDate)
            );
    }

    updateEvent(event: Event, ensembleId: string): Observable<Event>{
        return this.http.put<Event>(`${this.apiUrl}/ensembles/${ensembleId}/events`, event)
            .pipe(map(this.convertEventDate));
    }

    deleteEvent(eventId: string, ensembleId: string): Observable<any>{
        return this.http.delete<any>(`${this.apiUrl}/ensembles/${ensembleId}/events/${eventId}`);
    }

    deleteSerialEvent(eventId: string, ensembleId: string): Observable<any>{
        return this.http.delete<any>(`${this.apiUrl}/ensembles/${ensembleId}/events/${eventId}/serial`);
    }

    getEventsForProject(ensembleId: string, projectId: string, time: string = ''): Observable<Event[]>{
        return this.http.get<Event[]>(`${this.apiUrl}/ensembles/${ensembleId}/projects/${projectId}/events${time}`)
            .pipe(
                map(this.convertEventsDate)
            );
    }

    getEventsForDashboard(): Observable<Event[]>{
        return this.http.get<Event[]>(`${this.apiUrl}/events/dashboard`)
            .pipe(
                map(this.convertEventsDate)
            );
    }

    getEventsForEventgroup(ensembleId: string, eventgroupId: string, time: string = ''): Observable<Event[]>{
        return this.http.get<Event[]>(`${this.apiUrl}/ensembles/${ensembleId}/eventgroups/${eventgroupId}/events${time}`)
            .pipe(
                map(this.convertEventsDate)
            );
    }

    getEventRegisters(ensembleId: string, eventId: string): Observable<Register[]> {
        return this.http.get<Register[]>(`${this.apiUrl}/ensembles/${ensembleId}/events/${eventId}/registers`);
    }

    deleteRegisterFromEvent(registers: Register[], eventId: string, ensembleId: string): Observable<any> {
        return this.http.put<any>(`${this.apiUrl}/ensembles/${ensembleId}/events/${eventId}/registers`, registers);
    }

    getEventById(ensembleId: string, eventId: string): Observable<Event>{
        return this.http.get(`${this.apiUrl}/ensembles/${ensembleId}/events/${eventId}`)
            .pipe(
                map(this.convertEventDate)
            );
    }

    private convertEventsDate(events: Event[]): Event[]{
        return events.map((event: Event) => {
            event.start = new Date(event.start);
            event.end = new Date(event.end);
            if(event.deadline){
                event.deadline = new Date(event.deadline);
            }
            if(event.project && event.project.start && event.project.end){
                event.project.start = new Date(event.project.start);
                event.project.end = new Date(event.project.end);
            }
            if(event.eventgroup && event.eventgroup.start && event.eventgroup.end){
                event.eventgroup.start = new Date(event.eventgroup.start);
                event.eventgroup.end = new Date(event.eventgroup.end);
            }
            return event;
        });
    }

    private convertEventDate(event: Event): Event{
        event.start = new Date(event.start);
        event.end = new Date(event.end);
        if(event.deadline){
            event.deadline = new Date(event.deadline);
        }
        if(event.project && event.project.start && event.project.end){
            event.project.start = new Date(event.project.start);
            event.project.end = new Date(event.project.end);
        }
        if(event.eventgroup && event.eventgroup.start && event.eventgroup.end){
            event.eventgroup.start = new Date(event.eventgroup.start);
            event.eventgroup.end = new Date(event.eventgroup.end);
        }
        return event;
    }
}