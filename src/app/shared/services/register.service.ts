import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Register } from '../models/register';
import { User } from '../models/user';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class RegisterService {

    apiUrl = environment.apiUrl + '/ensembles';
    constructor(private http: HttpClient) { }

    getRegistersForEnsemble(ensembleId: string): Observable<Register[]> {
        return this.http.get<Register[]>(`${this.apiUrl}/${ensembleId}/registers`);
    }

    createRegister(register: Register): Observable<Register> {
        return this.http.post<Register>(`${this.apiUrl}/${register.ensemble.id}/registers`, register);
    }

    removeRegister(register: Register): Observable<any> {
        return this.http.delete(`${this.apiUrl}/${register.ensemble.id}/registers/${register.id}`);
    }

    updateRegister(register: Register): Observable<Register> {
        return this.http.put<Register>(`${this.apiUrl}/${register.ensemble.id}/registers`, register);
    }

    getUsersForEnsembleRegisters(ensembleId: string): Observable<User[]>{
        return this.http.get<User[]>(`${this.apiUrl}/${ensembleId}/registers/users`);
    }

    assignUser(userId: string, registerId: string, ensembleId: string): Observable<any>{
        return this.http.post<any>(`${this.apiUrl}/${ensembleId}/registers/${registerId}/users/${userId}`, null);
    }

    unassignUser(userId: string, registerId: string, ensembleId: string): Observable<any>{
        return this.http.delete<any>(`${this.apiUrl}/${ensembleId}/registers/${registerId}/users/${userId}`);
    }

}
