import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Announcement } from '../models/announcement';
import { User } from '../models/user';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AnnouncementService {

    apiUrl = `${environment.apiUrl}/ensembles`;

    constructor(private http: HttpClient) { }

    createAnnouncement(announcement: Announcement, ensembleId: string): Observable<Announcement> {
        return this.http.post<Announcement>(`${this.apiUrl}/${ensembleId}/announcements`, announcement);
    }

    updateAnnouncement(announcement: Announcement, ensembleId: string): Observable<Announcement> {
        return this.http.put<Announcement>(`${this.apiUrl}/${ensembleId}/announcements`, announcement);
    }

    getAnnouncementsForEvent(ensembleId: string, eventId: string): Observable<User[]>{
        return this.http.get<User[]>(`${this.apiUrl}/${ensembleId}/events/${eventId}/announcements`);
    }

    getAnnouncementsForEventgroupEvents(ensembleId: string, eventgroupId: string): Observable<User[]>{
        return this.http.get<User[]>(`${this.apiUrl}/${ensembleId}/eventgroups/${eventgroupId}/events/announcements`);
    }

    getAnnouncementsForEventgroup(ensembleId: string, eventgroupId: string): Observable<User[]>{
        return this.http.get<User[]>(`${this.apiUrl}/${ensembleId}/eventgroups/${eventgroupId}/announcements`);
    }

    getAnnouncementsForProject(ensembleId: string, projectId: string): Observable<User[]>{
        return this.http.get<User[]>(`${this.apiUrl}/${ensembleId}/projects/${projectId}/announcements`);
    }

    getAnnouncementsForProjectEvents(ensembleId: string, projectId: string): Observable<User[]>{
        return this.http.get<User[]>(`${this.apiUrl}/${ensembleId}/projects/${projectId}/events/announcements`);
    }

    getEventgroupAnnouncementsPdfExport(ensembleId: string, eventgroupId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/${ensembleId}/eventgroups/${eventgroupId}/announcements/pdf`, { responseType: 'blob' as 'json' });
    }

    getEventgroupAnnouncementsCsvExport(ensembleId: string, eventgroupId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/${ensembleId}/eventgroups/${eventgroupId}/announcements/csv`, { responseType: 'blob' as 'json' });
    }

    getProjectAnnouncementsPdfExport(ensembleId: string, projectId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/${ensembleId}/projects/${projectId}/announcements/pdf`, { responseType: 'blob' as 'json' });
    }

    getProjectAnnouncementsCsvExport(ensembleId: string, projectId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/${ensembleId}/projects/${projectId}/announcements/csv`, { responseType: 'blob' as 'json' });
    }

    deleteAllAnnouncementsForProject(ensembleId: string, projectId: string): Observable<any>{
        return this.http.delete<any>(`${this.apiUrl}/${ensembleId}/projects/${projectId}/announcements/delete`);
    }

    deleteAllAnnouncementsForEventgroup(ensembleId: string, eventgroupId: string): Observable<any>{
        return this.http.delete<any>(`${this.apiUrl}/${ensembleId}/eventgroups/${eventgroupId}/announcements/delete`);
    }

}
