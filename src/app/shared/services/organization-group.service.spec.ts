import { TestBed, inject } from '@angular/core/testing';

import { OrganizationGroupService } from './organization-group.service';

describe('OrganizationGroupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrganizationGroupService]
    });
  });

  it('should be created', inject([OrganizationGroupService], (service: OrganizationGroupService) => {
    expect(service).toBeTruthy();
  }));
});
