import { TestBed, inject } from '@angular/core/testing';

import { EventgroupService } from './eventgroup.service';

describe('EventgroupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EventgroupService]
    });
  });

  it('should be created', inject([EventgroupService], (service: EventgroupService) => {
    expect(service).toBeTruthy();
  }));
});
