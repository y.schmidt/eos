import { TestBed } from '@angular/core/testing';

import { UiWizardService } from './ui-wizard.service';

describe('UiWizardService', () => {
  let service: UiWizardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UiWizardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
