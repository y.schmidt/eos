import { take, map, tap, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { ReplaySubject ,  BehaviorSubject ,  Observable } from 'rxjs';

import { User } from '../models/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtService } from './jwt.service';
import { UserService } from './user.service';
import {
    ActivatedRouteSnapshot,
    CanActivate,
    Router,
    RouterStateSnapshot
} from '@angular/router';
import { NotificationService } from './notification.service';
import { environment } from '../../../environments/environment';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AcceptTermsOfUseModalComponent } from '../components/accept-terms-of-use-modal/accept-terms-of-use-modal.component';

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

    private apiUrl = environment.apiUrl;

    private authedUserSubject = new BehaviorSubject<User>(new User());
    public authedUser = this.authedUserSubject.asObservable();

    private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
    public isAuthenticated = this.isAuthenticatedSubject.asObservable();

    constructor(private http: HttpClient,
                private jwtService: JwtService,
                private userService: UserService,
                private router: Router,
                private modalService: BsModalService,
                private notificationService: NotificationService) {}

    populate() {
        if(this.isLoggedIn()) {
            this.isAuthenticatedSubject.next(true);
            this.userService.getUserById(this.jwtService.decodeToken().userId, 'join=registers')
                .subscribe(
                    user => {
                        this.checkTermsOfUse(user);
                        this.authedUserSubject.next(user);
                    },
                    err => this.notificationService.setErrorNotification(err))
        }
    }

    login(user: string, password: string) {
        return this.http.post<any>(`${this.apiUrl}/login`, { user: user, password: password }, httpOptions).pipe(
            map(authResult => {
                this.setAuth(authResult);
            }));
    }

    logout(){
        if(this.isLoggedIn()){
            this.http.post<any>(`${this.apiUrl}/logout`, this.getAuthedUser())
                .subscribe(() => {
                    this.destroyAuth();
                    this.router.navigateByUrl('/start/login');
                });
        } else {
            this.destroyAuth();
            this.router.navigateByUrl('/start/login');
        }
    }

    checkTermsOfUse(user: User){
        if(!user.acceptTermsOfUse){
            this.modalService.show(AcceptTermsOfUseModalComponent);
        }
    }

    renewToken() {
        return this.http.get<any>(`${this.apiUrl}/users/${this.authedUserSubject.value.id}/renewToken`)
            .pipe(
                map(authResult => {
                    this.setAuth(authResult);
            }));
    }

    refreshToken(){
        const token = this.jwtService.getRefreshToken();
        if(token != null){
            return this.http.get<any>(`${this.apiUrl}/refreshToken/${this.jwtService.getRefreshToken()}`)
                .pipe(
                    tap(authResult => {
                        this.jwtService.saveToken(authResult.token, 'token');
                        this.jwtService.saveToken(authResult.refreshToken, 'refreshToken');
                    }),
                    catchError((error) => {
                        this.destroyAuth();
                        this.router.navigateByUrl('/start/login');
                        return new Observable<never>();
                    }));
        } else {
            this.destroyAuth();
            this.router.navigateByUrl('/start/login');
            return new Observable<never>();
        }
    }

    destroyAuth(){
        this.jwtService.destroyToken();
        this.isAuthenticatedSubject.next(false);
        this.authedUserSubject.next(new User());
    }

    getAuthedUser(): User {
        return this.authedUserSubject.value;
    }

    setAuth(authResult) {
        try{
            this.jwtService.saveToken(authResult.token, 'token');
            this.jwtService.saveToken(authResult.refreshToken, 'refreshToken');
            this.isAuthenticatedSubject.next(true);
            this.userService.getUserById(this.jwtService.decodeToken().userId, 'join=registers')
                .subscribe((user) => {
                    this.checkTermsOfUse(user);
                    this.authedUserSubject.next(user);
                }, err => this.notificationService.setErrorNotification(err));
        } catch(error){
            this.destroyAuth();
            this.router.navigateByUrl('/start/login');
        }
    }

    isLoggedIn(): boolean {
        return this.jwtService.getToken() != undefined;
        //return this.jwtService.getToken() && !this.jwtService.isTokenExpired();
    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> {
        if(!this.isLoggedIn()) {
            this.router.navigateByUrl('/start/login');
        }
        return this.isAuthenticated.pipe(take(1));
    }

    isUserAdminInOrganization(): boolean {
        return this.jwtService.decodeToken().credentials
            .find(
                (role) => role.permissions.includes('organizationAdmin') == true
            ) !== undefined;
    }

    isUserAdminInEnsemble(): boolean {
        return this.jwtService.decodeToken().credentials
            .find(
                (role) => role.permissions.includes('ensembleAdmin') == true
            ) !== undefined;
    }

    hasUserPermission(permission: string, context: string): boolean {
        const role = this.jwtService.decodeToken().credentials.find( role => role.context == context);
        if(role) {
            return role.permissions.includes(permission);
        } else {
            return false;
        }

    }
}
