import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Organization } from '../models/organization';
import { Observable } from 'rxjs';
import { OrganizationUser } from '../models/organizationUser';
import { environment } from '../../../environments/environment';
import { Role } from '../models/role';
import { Official } from '../models/official';

@Injectable({
    providedIn: 'root'
})
export class OrganizationService {

    apiUrl = environment.apiUrl + '/organizations';
    constructor(private http: HttpClient) { }

    createOrganization(organization: Organization): Observable<Organization> {
        return this.http.post<Organization>(this.apiUrl, organization);
    }

    existOrganization(name: string): Observable<boolean> {
        return this.http.get<boolean>(`${this.apiUrl}/exists?name=${name}`);
    }

    getOrganizationById(id: string): Observable<Organization> {
        return this.http.get<Organization>(`${this.apiUrl}/${id}`);
    }

    updateOrganization(organization: Organization): Observable<Organization> {
        return this.http.put<Organization>(`${this.apiUrl}`, organization);
    }

    getUserForOrganization(orgaId: string): Observable<OrganizationUser[]> {
        return this.http.get<OrganizationUser[]>(`${this.apiUrl}/${orgaId}/users`);
    }

    unassignUser(orgaId: string, userId: string): Observable<any> {
        return this.http.delete(`${this.apiUrl}/${orgaId}/users/${userId}`);
    }

    updateOrganizationUser(organizationUser: OrganizationUser): Observable<OrganizationUser>{
        return this.http.put<OrganizationUser>(`${this.apiUrl}/${organizationUser.organization.id}/users`, organizationUser);
    }

    deleteOrganization(orgaId: string): Observable<any>{
        return this.http.delete(`${this.apiUrl}/${orgaId}`);
    }

    changeOwner(organization: Organization): Observable<any>{
        return this.http.put<any>(`${this.apiUrl}/owner`, organization);
    }

    getOrganizationRoles(organizationId: string): Observable<Role[]>{
        return this.http.get<Role[]>(`${this.apiUrl}/${organizationId}/roles`);
    }

    getOrganizationOfficials(organizationId: string): Observable<Official[]>{
        return this.http.get<Official[]>(`${this.apiUrl}/${organizationId}/officials`);
    }

    deleteOrganizationOfficial(orgaId: string, officialId: string): Observable<any>{
        return this.http.delete(`${this.apiUrl}/${orgaId}/officials/${officialId}`);
    }

    createOrganizationOfficial(orgaId: string, official: Official): Observable<Official>{
        return this.http.post<Official>(`${this.apiUrl}/${orgaId}/officials`, official);
    }

    updateOrganizationOfficial(orgaId: string, official: Official): Observable<Official>{
        return this.http.put<Official>(`${this.apiUrl}/${orgaId}/officials`, official);
    }
}
