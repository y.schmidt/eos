import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { JwtService } from './jwt.service';
import { OrganizationUser } from '../models/organizationUser';
import { EnsembleUser } from '../models/ensembleUser';
import { environment } from '../../../environments/environment';
import { Organization } from '../models/organization';
import { Ensemble } from '../models/ensemble';
import { Register } from '../models/register';
import { OrganizationGroup } from '../models/organizationGroup';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private serviceUrl = environment.apiUrl + '/users';
    public profilePictureEmitter: EventEmitter<any> = new EventEmitter<any>();

    apiUrl = environment.apiUrl;
    constructor(private http: HttpClient,
                private  jwtService: JwtService) { }

    getUserById(id: string, query?: string): Observable<User>{
        return this.http.get<User>(`${this.serviceUrl}/${id}?${query}`);
    }

    getUserId(): string {
        return this.jwtService.decodeToken().userId;
    }

    updateUser(user: User): Observable<User>{
        return this.http.put<User>(this.serviceUrl, user);
    }

    register(user: User): Observable<User> {
        return this.http.post<User>(`${this.serviceUrl}`, user);
    }

    existMail(mail: string): Observable<boolean> {
        return this.http.get<boolean>(`${this.apiUrl}/mail?mail=${mail}`);
    }

    userGetOrganizations(userId: string): Observable<OrganizationUser[]> {
        return this.http.get<OrganizationUser[]>(`${this.serviceUrl}/${userId}/organizations`);
    }

    userGetOrganizationsWithStats(userId: string): Observable<Organization[]> {
        return this.http.get<Organization[]>(`${this.serviceUrl}/${userId}/organizations/stats`);
    }

    userGetAdminOrganizations(userId: string): Observable<Organization[]> {
        return this.http.get<Organization[]>(`${this.serviceUrl}/${userId}/organizations/admin`);
    }

    userGetEnsembles(userId: string): Observable<EnsembleUser[]> {
        return this.http.get<EnsembleUser[]>(`${this.serviceUrl}/${userId}/ensembles`);
    }

    userGetRegisters(userId: string): Observable<Register[]> {
        return this.http.get<Register[]>(`${this.serviceUrl}/${userId}/registers`);
    }

    userGetOrgaGroups(userId: string): Observable<OrganizationGroup[]> {
        return this.http.get<OrganizationGroup[]>(`${this.serviceUrl}/${userId}/orgaGroups`);
    }

    userGetEnsembleWithStats(userId: string): Observable<Ensemble[]> {
        return this.http.get<Ensemble[]>(`${this.serviceUrl}/${userId}/ensembles/stats`);
    }

    forgetPassword(mail: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/forgetPassword/${mail}`);
    }

    resetPassword(mail: string, token: string, password: any): Observable<any> {
        return this.http.post<any>(`${this.apiUrl}/resetPassword/${token}/${mail}`, password);
    }

    userChangePassword(password: any): Observable<any>{
        return this.http.put<any>(`${this.serviceUrl}/${password.userId}/changePassword`, password);
    }

    userChangeMail(mail: any): Observable<any>{
        return this.http.put<any>(`${this.serviceUrl}/${mail.userId}/changeMail`, mail);
    }

    userDelete(userId: string): Observable<any>{
        return this.http.delete<any>(`${this.serviceUrl}/${userId}`);
    }

    userGetProfilePicture(userId: string): Observable<any> {
        return this.http.get<any>(`${ this.serviceUrl }/${ userId }/profilePicture`);
    }

    sendFeedback(feedback: any): Observable<any>{
        return this.http.post<any>(`${this.apiUrl}/feedback`, feedback);
    }


}
