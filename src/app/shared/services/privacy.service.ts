import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { NgcCookieConsentService, NgcStatusChangeEvent } from 'ngx-cookieconsent';
import { Router } from '@angular/router';
import { NotificationService } from './notification.service';

@Injectable({
    providedIn: 'root'
})
export class PrivacyService {

    constructor(private cookieService: CookieService,
                private ccService: NgcCookieConsentService,
                private router: Router,
                private notificationService: NotificationService) { }

    setCookieWorkflow(){
        this.subscribeCookieConsent();
        if(!this.getCookieConsent()){
            this.ccService.open();
        }
    }

    removeCookies(){
        this.cookieService.deleteAll();
    }

    private getCookieConsent(){
        return this.cookieService.get('cookieConsent');
    }

    private setCookie(name: string, value){
        this.cookieService.set(name, value);
    }

    private subscribeCookieConsent(){
        this.ccService.statusChange$.subscribe(
            (event: NgcStatusChangeEvent) => {
                if(event.status != 'deny'){
                    this.setCookie('cookieConsent', true);
                    this.setCookie('cookieStatus', event.status);
                }
                if(event.status == 'deny'){
                    this.router.navigate(['/start/login']);
                    this.notificationService.setManualErrorNotification('Um dich einloggen zu können müssen Cookies aktiviert sein.');
                    this.removeCookies();
                }
            }
        );
    }

}
