import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Organization } from '../models/organization';
import { Ensemble } from '../models/ensemble';
import { Invite } from '../models/invite';

@Injectable({
  providedIn: 'root'
})
export class UiWizardService {

  private apiUrl = environment.apiUrl + '/wizard';
  constructor(private http: HttpClient) { }

  organisatorStart(data: { organization: Organization, ensemble: Ensemble, invites: Invite[] }): Observable<{ organization: Organization, ensemble: Ensemble }>{
    return this.http.post<{ organization: Organization, ensemble: Ensemble }>(`${this.apiUrl}/organisator`, data);
  }
}
