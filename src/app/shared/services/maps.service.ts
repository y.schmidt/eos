import { Injectable } from '@angular/core';
import { Address } from '../models/address';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class MapsService {

    private apiUrl = 'https://maps.googleapis.com/maps/api/geocode/json';
    private apiKey = environment.mapsApiKey;
    constructor(private http: HttpClient) { }

    getCoordinates(address: Address): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/?${this.getAddressString(address)}&key=${this.apiKey}`, {
            headers: new HttpHeaders().set('Access-Control-Allow-Origin', '*'),
        });
    }

    getAddressString(address: Address){
        return `address=${address.street}+${address.number}+${address.city}+${address.postalCode}`;
    }
}
