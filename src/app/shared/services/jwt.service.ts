import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable({
    providedIn: 'root'
})
export class JwtService {

    private jwtHelper = new JwtHelperService();

    constructor() { }

    getToken(): string {
        return localStorage.getItem('token');
    }

    getRefreshToken(): string {
        return localStorage.getItem('refreshToken');
    }

    isTokenExpired(): boolean {
        return this.jwtHelper.isTokenExpired(localStorage.getItem('token'));
    }

    saveToken(token: string, name: string) {
        localStorage.setItem(name, token);
    }

    destroyToken() {
        localStorage.clear();
    }

    decodeToken() {
        return this.jwtHelper.decodeToken(localStorage.getItem('token').substring(7));
    }
}
