import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from '../models/project';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Register } from '../models/register';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ProjectService {

    apiUrl = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getProjects(userId: string, getPast: boolean = false): Observable<Project[]> {
        const past = getPast ? '/past' : '';
        return this.http.get<Project[]>(`${this.apiUrl}/users/${userId}/projects${past}`)
            .pipe(
                map(this.convertProjectsDate)
            );
    }

    getProjectById(ensembleId: string, projectId: string): Observable<Project> {
        return this.http.get<Project>(`${this.apiUrl}/ensembles/${ensembleId}/projects/${projectId}`)
            .pipe(
                map(this.convertProjectDate)
            );
    }

    updateProject(project: Project, ensembleId: string): Observable<Project> {
        return this.http.put<Project>(`${this.apiUrl}/ensembles/${ensembleId}/projects`, project)
            .pipe(
                map(this.convertProjectDate)
            );
    }

    createProject(project: Project, ensembleId: string): Observable<Project> {
        console.log(project);
        return this.http.post<Project>(`${this.apiUrl}/ensembles/${ensembleId}/projects`, project)
            .pipe(
                map(this.convertProjectDate)
            );
    }

    deleteProject(ensembleId: string, projectId: string): Observable<any> {
        return this.http.delete<any>(`${this.apiUrl}/ensembles/${ensembleId}/projects/${projectId}`);
    }


    getProjectPdfExport(ensembleId: string, projectId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/ensembles/${ensembleId}/projects/${projectId}/export/pdf`, { responseType: 'blob' as 'json' });
    }

    getProjectTypeOverviewExport(ensembleId: string, target: string, targetId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/ensembles/${ensembleId}/${target}/${targetId}/countAnnouncementsByType/export`, { responseType: 'blob' as 'json' });
    }

    getProjectRegisters(ensembleId: string, projectId: string): Observable<Register[]> {
        return this.http.get<Register[]>(`${this.apiUrl}/ensembles/${ensembleId}/projects/${projectId}/registers`);
    }

    sendProjectReminder(ensembleId: string, projectId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/ensembles/${ensembleId}/projects/${projectId}/sendReminder`);
    }

    getProjectIcal(ensembleId: string, projectId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/ensembles/${ensembleId}/projects/${projectId}/ical`);
    }

    getProjectPicture(ensembleId: string, projectId: string): Observable<any> {
        return this.http.get<any>(`${ this.apiUrl }/ensembles/${ensembleId}/projects/${projectId}/picture`);
    }

    getExampleProject(): Observable<Project>{
        return this.http.get<Project>(`${this.apiUrl}/projects/example`);
    }

    private convertProjectsDate(projects: Project[]): Project[]{
        return projects.map((project: Project) => {
            project.start = new Date(project.start);
            project.end = new Date(project.end);
            project.range = [project.start, project.end];
            project.deadline = new Date(project.deadline);
            return project;
        });
    }

    private convertProjectDate(project: Project): Project{
        project.start = new Date(project.start);
        project.end = new Date(project.end);
        project.range = [project.start, project.end];
        project.deadline = new Date(project.deadline);
        return project;
    }
}
