import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Mail } from '../models/mail';
import { environment } from '../../../environments/environment';
import { User } from '../models/user';

@Injectable({
    providedIn: 'root'
})
export class CommunicationService {

    private selectedMailSubject = new BehaviorSubject<Mail>(new Mail());
    public selectedMail = this.selectedMailSubject.asObservable();

    private apiUrl = environment.apiUrl + '/communication';

    constructor(private http: HttpClient) { }

    sendMail(mail: Mail): Observable<any>{
        return this.http.post<any>(`${this.apiUrl}/mails`, mail);
    }

    getRecipientGroupForUser(userId: string, config: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/${userId}/recipients${config}`);
    }

    getRecipientsForUser(userId: string): Observable<User[]>{
        return this.http.get<User[]>(`${this.apiUrl}/${userId}/recipients`);
    }

    getMailsForUser(userId: string): Observable<Mail[]>{
        return this.http.get<Mail[]>(`${this.apiUrl}/${userId}/mails`);
    }

    getReminderForUser(userId: string): Observable<Mail[]>{
        return this.http.get<Mail[]>(`${this.apiUrl}/${userId}/mails/reminder`);
    }

    getDraftsForUser(userId: string): Observable<Mail[]>{
        return this.http.get<Mail[]>(`${this.apiUrl}/${userId}/mails/drafts`);
    }

    saveDrafts(userId: string, mail: Mail): Observable<Mail>{
        return this.http.post<Mail>(`${this.apiUrl}/${userId}/mails/drafts`, mail);
    }

    setSelectedMail(mail: Mail){
        this.selectedMailSubject.next(mail);
    }

    getMailById(userId: string, mailId: string): Observable<Mail>{
        return this.http.get<Mail>(`${this.apiUrl}/${userId}/mails/${mailId}`);
    }

}
