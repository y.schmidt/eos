import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { Notification } from '../components/notification/notification';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    private notificationSubject = new ReplaySubject<Notification>(3);
    public notifcation = this.notificationSubject.asObservable();

    constructor() { }

    setNotification(notification: Notification) {
        this.notificationSubject.next(notification);
    }

    setSuccessNotification(message: string) {
        this.setNotification(
            new Notification('success', 'Erfolg!', message, 3000)
        );
    }

    /**
     * Gets Error Object from Backend
     * @param error
     */
    setErrorNotification(error){
        this.setNotification(
            new Notification('danger', `Error ${error.status}: ${error.error}`, error.message)
        );
    }

    /**
     * Gets Error string
     * @param error
     */
    setManualErrorNotification(error: string){
        this.setNotification(
            new Notification('danger', `Error`, error)
        );
    }
}
