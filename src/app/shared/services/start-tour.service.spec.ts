import { TestBed } from '@angular/core/testing';

import { StartTourService } from './start-tour.service';

describe('StartTourService', () => {
  let service: StartTourService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StartTourService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
