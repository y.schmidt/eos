import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  private renderer: Renderer2;
  sidebarActive: boolean = false;

  menus = [
    {
      title: 'Dashboard',
      icon: 'fa fa-tachometer-alt',
      active: false,
      type: 'simple',
      routerLink: 'dashboard'
    },
    {
      title: 'Organisationen',
      icon: 'fas fa-users',
      active: false,
      type: 'dropdown',
      routerLink: 'organizations',
      submenus: [
        {
          title: 'Vereine',
          routerLink: 'organizations'
        },
        {
          title: 'Ensembles',
          routerLink: 'ensembles'
        }
      ]
    },
    {
      title: 'Meine Projekte',
      icon: 'fas fa-music',
      active: false,
      type: 'dropdown',
      routerLink: 'projects',
      submenus: [
        {
          title: 'Aktuelle Projekte',
          routerLink: 'projects'
        },
        {
          title: 'Vergangene Projekte',
          routerLink: 'projects/past'
        }
      ]
    },
     {
       title: 'Kalender',
       icon: 'far fa-calendar-alt',
       active: false,
       type: 'simple',
       routerLink: 'calendar'
    },
    {
      title: 'Kontakt',
      icon: 'fas fa-envelope',
      active: false,
      type: 'simple',
      routerLink: 'communication'
    }
  ];
  constructor(private rendererFactory: RendererFactory2) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  toggle() {
    if(window.innerWidth < environment.breakpoints.large + 1){
      this.sidebarActive = !this.sidebarActive;
      this.setSidebarStatus(this.sidebarActive);
    }
  }

  private setSidebarStatus(active: boolean){
    if(window.innerWidth < environment.breakpoints.large + 1){
      const containsMinisidebar = document.body.classList.contains('mini-sidebar');
      //medium inactive --> active
      if(active && containsMinisidebar){
        this.renderer.removeClass(document.body, 'mini-sidebar');
      }
      //medium active --> inactive
      if(!active && !containsMinisidebar){
        this.renderer.addClass(document.body, 'mini-sidebar');
        this.setMenusInactive();
      }
      //small inactive --> active
      if(active && !containsMinisidebar){
        this.renderer.addClass(document.body, 'mini-sidebar');
      }
      // small active --> inactive
      if(!active && containsMinisidebar){
        this.renderer.removeClass(document.body, 'mini-sidebar');
      }
    }
  }

  submenuClick(currentMenu){
    if(window.innerWidth < environment.breakpoints.large + 1 && this.sidebarActive){
      currentMenu.active = false;
      this.toggle();
    }
  }

  menuClick(currentMenu){
    if(window.innerWidth < environment.breakpoints.large + 1) {
      if((currentMenu.type === 'dropdown' && !this.sidebarActive)){
        this.toggle();
      }
      if(currentMenu.type !== 'dropdown' && this.sidebarActive){
        this.toggle();
        this.setMenusInactive();
      }
    }
  }

  setMenusInactive(){
    this.menus.forEach((menu) => {
      menu.active = false;
    });
  }

  getMenuList() {
    return this.menus;
  }

}
