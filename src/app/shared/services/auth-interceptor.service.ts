import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpErrorResponse
} from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { JwtService } from './jwt.service';
import { catchError, filter, switchMap, take } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { AuthGuardService } from './auth-guard.service';


/**
 * Injects auth-token from local storage to every http request
 */
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor (private jwtService: JwtService,
                 private authGuard: AuthGuardService) {}

    private isRefreshing = false;
    private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const jwt = this.jwtService.getToken();

        if (jwt) {
            const clonedRequest = this.addToken(req, jwt);
            return next.handle(clonedRequest)
                .pipe(catchError((error) => {
                    if (error instanceof HttpErrorResponse && error.status === 401) {
                        return this.handle401Error(clonedRequest, next);
                    } else {
                        return this.handleError(error, clonedRequest, next);
                    }
                }));
        } else {
            return next.handle(req);
        }
    }

    private addToken(req: HttpRequest<any>, jwt: string) {
        return req.clone({
            headers: req.headers.set('Authorization', jwt),
        });
    }

    private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
        if (!this.isRefreshing) {
            this.isRefreshing = true;
            this.refreshTokenSubject.next(null);

            return this.authGuard.refreshToken().pipe(
                switchMap((authResult: any) => {
                    this.isRefreshing = false;
                    if(authResult != null){
                        this.refreshTokenSubject.next(authResult.token);
                        return next.handle(this.addToken(request, authResult.token));
                    } else {
                        return;
                    }

                }));

        } else {
            return this.refreshTokenSubject.pipe(
                filter(token => token != null),
                take(1),
                switchMap(token => {
                    return next.handle(this.addToken(request, token));
                }));
        }
    }

    private handleError(error: HttpErrorResponse, request: HttpRequest<any>, next: HttpHandler): Observable<never> {
        if(!environment.production){
            if (error.error instanceof ErrorEvent) {
                // A client-side or network errorMessage occurred. Handle it accordingly.
                console.error('An errorMessage occurred:', error.error.message);
            } else {
                // The backend returned an unsuccessful response code.
                // The response body may contain clues as to what went wrong,
                console.error(
                    `Backend returned code ${error.status}, ` +
                    `body was: ${error.error}`);
            }
        }
        switch (error.status) {
            case 0:
                return throwError(new Error('No connection to Backend'));
            //case 403:
            //    window.location.href = "/start/login";
            //    break;
            default:
                return throwError(error.error);
        }
    }
}
