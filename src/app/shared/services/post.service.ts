import { Injectable } from '@angular/core';
import { Post } from '../models/post';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PostComment } from '../models/postComment';

@Injectable({
    providedIn: 'root'
})
export class PostService {

    apiUrl = `${environment.apiUrl}/posts`;

    constructor(private http: HttpClient) { }

    getPosts(limit?: number): Observable<Post[]>{
        const url = limit ? `${this.apiUrl}?limit=${limit}` : `${this.apiUrl}`;
        return this.http.get<Post[]>(`${url}`);
    }

    getCommentsForPost(postId: string): Observable<PostComment[]>{
        return this.http.get<PostComment[]>(`${this.apiUrl}/${postId}/comments`);
    }

    createPost(post: Post): Observable<Post>{
        return this.http.post<Post>(`${this.apiUrl}`, post);
    }

    createPostComment(comment: PostComment): Observable<PostComment>{
        return this.http.post<PostComment>(`${this.apiUrl}/comments`, comment);
    }

    countPostComments(postId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/${postId}/comments/count`);
    }

    deletePost(postId: string): Observable<any>{
        return this.http.delete<any>(`${this.apiUrl}/${postId}`);
    }

    deleteComment(commentId: string): Observable<any>{
        return this.http.delete<any>(`${this.apiUrl}/comments/${commentId}`);
    }

    editComment(comment: PostComment): Observable<PostComment>{
        return this.http.put<PostComment>(`${this.apiUrl}/comments`, comment);
    }

    editPost(post: Post): Observable<Post>{
        return this.http.put<Post>(`${this.apiUrl}`, post);
    }
}
