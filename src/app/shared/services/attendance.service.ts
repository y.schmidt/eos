import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { Attendance } from '../models/attendance';

@Injectable({
  providedIn: 'root'
})
export class AttendanceService {

  apiUrl = `${environment.apiUrl}/ensembles`;

  constructor(private http: HttpClient) { }

  createAttendance(attendance: Attendance, ensembleId: string): Observable<Attendance> {
    return this.http.post<Attendance>(`${this.apiUrl}/${ensembleId}/attendance`, attendance);
  }

  updateAttendance(attendance: Attendance, ensembleId: string): Observable<Attendance> {
    return this.http.put<Attendance>(`${this.apiUrl}/${ensembleId}/attendance`, attendance);
  }

  getAttendanceForEvent(ensembleId: string, eventId: string): Observable<User[]>{
    return this.http.get<User[]>(`${this.apiUrl}/${ensembleId}/events/${eventId}/attendance`);
  }

  getAttendanceForEventgroupEvents(ensembleId: string, eventgroupId: string): Observable<User[]>{
    return this.http.get<User[]>(`${this.apiUrl}/${ensembleId}/eventgroups/${eventgroupId}/attendance`);
  }

  getAttendanceForProjectEvents(ensembleId: string, projectId: string): Observable<User[]>{
    return this.http.get<User[]>(`${this.apiUrl}/${ensembleId}/projects/${projectId}/attendance`);
  }

}
