import { TestBed, async, inject } from '@angular/core/testing';

import { DiscardChangesGuard } from './discard-changes.guard';

describe('DiscardChangesGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DiscardChangesGuard]
    });
  });

  it('should ...', inject([DiscardChangesGuard], (guard: DiscardChangesGuard) => {
    expect(guard).toBeTruthy();
  }));
});
