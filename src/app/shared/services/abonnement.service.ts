import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Abonnement } from '../models/abonnement';
import { environment } from '../../../environments/environment';
import { Product } from '../models/product';
import { ProductPlan } from '../models/productPlan';
import { Invoice } from '../models/invoice';

@Injectable({
    providedIn: 'root'
})
export class AbonnementService {

    apiUrl = environment.apiUrl;

    constructor(private http: HttpClient) { }

    getProducts(): Observable<any[]> {
        return this.http.get<any[]>(`${this.apiUrl}/products`);
    }

    getProductPlanById(planId: string): Observable<ProductPlan> {
        return this.http.get<ProductPlan>(`${this.apiUrl}/productPlans/${planId}`);
    }

    getAbonnementsForOrganization(orgaId: string): Observable<any> {
        return this.http.get<any>(`${this.apiUrl}/organizations/${orgaId}/abonnements`);
    }

    getAboStatusForOrganization(orgaId: string): Observable<any> {
        return this.http.get<any>(`${this.apiUrl}/organizations/${orgaId}/abonnements/status`);
    }

    getFutureAbosForOrganization(orgaId: string): Observable<any> {
        return this.http.get<any>(`${this.apiUrl}/organizations/${orgaId}/abonnements/future`);
    }

    getInvoicesForOrganization(orgaId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/organizations/${orgaId}/invoices`);
    }

    getPaymentMethods(orgaId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/stripe/${orgaId}/paymentMethods`)
    }

    setupStripePayment(subData: any): Observable<any>{
        return this.http.put<any>(`${this.apiUrl}/subscription/stripe/setup`, subData)
    }

    cancelStripeSubscription(subData: any): Observable<any>{
        return this.http.put<any>(`${this.apiUrl}/subscription/stripe/cancel`, subData)
    }

    cancelCancellationStripeSubscription(subData: any): Observable<any>{
        return this.http.put<any>(`${this.apiUrl}/subscription/stripe/cancelCancellation`, subData)
    }

    getActualAboMetadataForOrganization(orgaId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/organizations/${orgaId}/metadata`)
    }

    cancelStripeSubscriptionSchedule(subData: any): Observable<any>{
        return this.http.put<any>(`${this.apiUrl}/subscriptionSchedule/stripe/cancel`, subData)
    }
}
