import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public uploadUserCsv(file: File, orgaId: string, mode: 'ensembles' | 'organizations', ensembleId?: string): Observable<any> {
    const formData = new FormData();
    formData.append('userCsv', file, 'userCsv');
    const apiRoute = mode == 'ensembles' ?
        `${this.apiUrl}/organizations/${orgaId}/ensembles/${ensembleId}/userCsv` :
        `${this.apiUrl}/organizations/${orgaId}/userCsv`;
    const req = new HttpRequest('POST', apiRoute, formData, {
      reportProgress: true,
    });
    return this.http.request(req);
  }

  public uploadProfilePicture(file: Blob, userId: string): Observable<any> {
    const formData = new FormData();
    formData.append('profilePicture', file, 'profilePicture');
    const req = new HttpRequest('POST', `${this.apiUrl}/users/${userId}/profilePicture`, formData, {
      reportProgress: true,
    });
    return this.http.request(req);
  }

  public uploadProjectPicture(file: Blob, ensembleId: string, projectId: string): Observable<any> {
    const formData = new FormData();
    formData.append('projectPicture', file, 'projectPicture');
    const req = new HttpRequest('POST', `${this.apiUrl}/ensembles/${ensembleId}/projects/${projectId}/picture`, formData, {
      reportProgress: true,
    });
    return this.http.request(req);
  }

  public uploadOrganizationPicture(file: Blob, orgaId: string): Observable<any> {
    const formData = new FormData();
    formData.append('organizationPicture', file, 'organizationPicture');
    const req = new HttpRequest('POST', `${this.apiUrl}/organizations/${orgaId}/organizationPicture`, formData, {
      reportProgress: true,
    });
    return this.http.request(req);
  }
}