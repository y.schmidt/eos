import { Injectable } from '@angular/core';
import { NgForm } from '@angular/forms';

@Injectable({
    providedIn: 'root'
})
export class FormService {

    constructor() { }

    /**
     * Marks all controls in a passwordForm group as touched
     * @param formGroup
     */
    markFormGroupTouched(formGroup: NgForm) {
        (<any>Object).values(formGroup.controls).forEach(control => {
            control.markAsTouched();
            if (control.controls) {
                this.markFormGroupTouched(control);
            }
        });
    }
}
