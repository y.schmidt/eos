import { EventEmitter, Injectable, Output } from '@angular/core';
import { GuidedTour, GuidedTourService, Orientation } from 'ngx-guided-tour';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { SidebarService } from './sidebar.service';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})
export class StartTourService {

    private memberTour: GuidedTour = {
        tourId: 'member-tour',
        steps: [
            {
                title: 'Willkommen zur onesemble Tour',
                content: 'Schön, dass du da bist. Wir erklären Dir in ein paar Schritten, wie du onesemble optimal nutzen kannst.',
                action: () => {
                    this.toggleSidebar();
                }
            },
            {
                title: 'Dashboard',
                selector: '#sidebar-dashboard',
                content: 'Hier findest du alle wichtigen Informationen für dich zusammengefasst.',
                action: () => {
                    this.router.navigateByUrl('/dashboard')
                },
                closeAction: () => {
                    this.toggleSidebar();
                },
                orientation: [
                    {
                        orientationDirection: Orientation.BottomLeft,
                        maximumSize: environment.breakpoints.medium
                    },
                    {
                        orientationDirection: Orientation.Right,
                    }
                ]
            },
            {
                title: 'Projekte',
                selector: '#dashboardProjects',
                content: 'Hier sind alle aktuellen Projekte aus deinen Ensembles',
                orientation: [
                    {
                        orientationDirection: Orientation.BottomLeft,
                        maximumSize: environment.breakpoints.small
                    },
                    {
                        orientationDirection: Orientation.Right,
                    }
                ]
            },
            {
                title: 'Termine',
                selector: '#dashboardEvents',
                content: 'Hier findest du deine nächsten vier Terminen aus den Projekten zu denen du dich angemeldet hast.',
                orientation: [
                    {
                        orientationDirection: Orientation.Top,
                        maximumSize: environment.breakpoints.small
                    },
                    {
                        orientationDirection: Orientation.Right,
                    }
                ]
            },
            {
                title: 'Pinnwand',
                selector: '#pinboard',
                content: 'Auf der Pinnwand kannst du schnell Informationen mit anderen Teilen und natürlich selbst lesen. Dabei kannst du einstellen, wer deinen Post lesen darf.',
                closeAction: () => {
                    this.toggleSidebar();
                },
                orientation: [
                    {
                        orientationDirection: Orientation.Top,
                        maximumSize: environment.breakpoints.small
                    },
                    {
                        orientationDirection: Orientation.Left,
                    }
                ]
            },
            {
                title: 'Profil',
                selector: '.sidebar-profile',
                content: 'In deinem Profil findest du alle deine persönlichen Daten und Einstellungen.',
                action: () => {
                    this.router.navigateByUrl('/users/profile/data')
                },
                orientation: [
                    {
                        orientationDirection: Orientation.Bottom,
                        maximumSize: environment.breakpoints.small
                    },
                    {
                        orientationDirection: Orientation.Right,
                    }
                ]
            },
            {
                title: 'Los gehts mit Projekten!',
                selector: '#sidebar-projects',
                content: 'Als nächstes geht es zu deinen Projekten',
                action: () => {
                    this.router.navigateByUrl('/projects/example')
                },
                closeAction: () => {
                    this.toggleSidebar();
                },
                orientation: [
                    {
                        orientationDirection: Orientation.Bottom,
                        maximumSize: environment.breakpoints.medium
                    },
                    {
                        orientationDirection: Orientation.Right,
                    }
                ]
            },
            {
                title: 'Projektdetails',
                selector: '#projectDetails',
                content: 'Hier findest du alle Daten zu einem Projekt',
                orientation: [
                    {
                        orientationDirection: Orientation.Bottom,
                        maximumSize: environment.breakpoints.medium
                    },
                    {
                        orientationDirection: Orientation.Right,
                    }
                ]
            },
            {
                title: 'Termine',
                selector: '#projectEvents',
                content: 'Alle kommenden Termine des Projekts findest du hier.',
                orientation: [
                    {
                        orientationDirection: Orientation.Top,
                        maximumSize: environment.breakpoints.medium
                    },
                    {
                        orientationDirection: Orientation.Left,
                    }
                ]
            },
            {
                title: 'Probenplan',
                selector: '#projectActions',
                content: 'Hier kannst du dir einen Probenplan als PDF und als iCal herunterladen.',
                skipStep: window.innerWidth < environment.breakpoints.small + 1,
                closeAction: () => {
                    this.toggleSidebar();
                },
                orientation: [
                    {
                        orientationDirection: Orientation.Bottom,
                        maximumSize: environment.breakpoints.medium
                    },
                    {
                        orientationDirection: Orientation.Left,
                    }
                ]
            },
            {
                title: 'Kalender',
                selector: '#sidebar-calendar',
                content: 'Hier findest du nochmal alle Termine aus deinen Projekten in eine übersichtlichen Kalenderansicht',
                closeAction: () => {
                    this.toggleSidebar();
                },
                orientation: [
                    {
                        orientationDirection: Orientation.Top,
                        maximumSize: environment.breakpoints.medium
                    },
                    {
                        orientationDirection: Orientation.Right,
                    }
                ]
            },
            {
                title: 'Es kann los gehen',
                content: 'Du hast jetzt alle wichtigen Funktionen gesehen. Starte jetzt und schau nach, ob es Projekte für dich gibt. Wenn du Fragen oder Feedback hast, dann nutze den Button in der Leiste oben oder schreibe uns eine Mail: support@onesemble.de',
                action: () => {
                    this.router.navigateByUrl('/dashboard')
                }
            }
        ]
    };

    private organisatorTour: GuidedTour = {
        tourId: 'organisator-tour',
        completeCallback: () => {
            this.cookieService.delete('os_organizatorTourPayload', '/');
        },
        steps: [
            {
                title: 'Organisator Tour',
                content: 'Zuletzt zeigen wir dir noch die wichtigsten Funktionen als Organisator*in eines Vereins.',
                action: () => {
                     this.router.navigateByUrl(`/organizations`)
                },
            },
            {
                title: 'Vereine und Ensembles',
                selector: '#sidebar-organizations',
                content: 'Hier findest du Deine Vereine und Ensembles',
                action: () => {
                    this.router.navigateByUrl(`/organizations/${this.getOrganisatorPayload().organization.id}/members`)
                },
                orientation: [
                    {
                        orientationDirection: Orientation.BottomLeft,
                        maximumSize: environment.breakpoints.small
                    },
                    {
                        orientationDirection: Orientation.Right,
                    }
                ]
            },
            {
                title: 'Mitglieder',
                selector: '#organizationUsers',
                content: 'Hier sind alle Vereinsmitglieder aufgelistet. Über das Dropdown-Menü kannst du die Rolle der Mitglieder ändern und über den Papierkorb aus dem Verein entfernen.',
                orientation: [
                    {
                        orientationDirection: Orientation.Bottom,
                        maximumSize: environment.breakpoints.small
                    },
                    {
                        orientationDirection: Orientation.Right,
                    }
                ]
            },
            {
                title: 'Neue Mitglieder',
                selector: '#inviteUser',
                content: 'Hier kannst du neue Mitglieder zu deinem Verein einladen.',
                orientation: [
                    {
                        orientationDirection: Orientation.Top,
                        maximumSize: environment.breakpoints.small
                    },
                    {
                        orientationDirection: Orientation.Left,
                    }
                ]
            },
            {
                title: 'Abo',
                selector: '#organisationAbo',
                content: 'Wenn du onesemble mit deinem Verein nutzen möchtest, dann kannst du hier ein Abo abschließen. Dabei kannst du dich zwischen einen monatlichen und eine jährlichen Variante entscheiden.',
                action: () => {
                    this.router.navigateByUrl(`/organizations/${this.getOrganisatorPayload().organization.id}/groups`)
                },
                orientation: [
                    {
                        orientationDirection: Orientation.Bottom,
                        maximumSize: environment.breakpoints.small
                    },
                    {
                        orientationDirection: Orientation.Left,
                    }
                ]
            },
            {
                title: 'Gruppen',
                selector: '#organisationGroupCard',
                content: 'Mit Gruppen kannst du Benutzer zusammenfassen, um mit ihnen zu kommunzieren. Du kannst Nachrichten auf der Pinnwand an Gruppen senden und Mails an Gruppen schreiben.',
                orientation: [
                    {
                        orientationDirection: Orientation.Bottom,
                        maximumSize: environment.breakpoints.small
                    },
                    {
                        orientationDirection: Orientation.Right,
                    }
                ]
            },
            {
                title: 'Ensembles',
                content: 'Die Verwaltung deiner Ensembles sieht der Verwaltung der Vereine sehr ähnlich. Auch hier kannst du Benutzer hinzufügen und einladen. Anstatt Gruppen kannst du hier Register/Stimmgruppen anlegen. Diese sind sehr wichtig für deine Projekte, da du hier Proben für Register festlegen kannst. Damit alle Mitglieder auch die Proben sehen ist es wichtig, dass die Ensemblemitglieder den Registern zugeordnet sind.',
                action: () => {
                    this.router.navigateByUrl(`/ensembles/${this.getOrganisatorPayload().ensemble.id}/registers`)
                }
            },
            {
                title: 'Register',
                selector: '#registerUsers',
                content: 'Hier kannst du angeben, welcher Benutzer in welchem Register ist. Die Auswahl ist wichtig für die Sichtbarkeit deiner Proben in Projekten.',
                orientation: [
                    {
                        orientationDirection: Orientation.Top,
                        maximumSize: environment.breakpoints.small
                    },
                    {
                        orientationDirection: Orientation.Left,
                    }
                ]
            },
            {
                title: 'Projekte',
                selector: '#sidebar-projects',
                content: 'Hier findest du alle Projekte zu deinen Ensembles',
                action: () => {
                    this.router.navigateByUrl(`/projects/${this.getOrganisatorPayload().project.id}/ensembles/${this.getOrganisatorPayload().ensemble.id}`)
                },
                orientation: [
                    {
                        orientationDirection: Orientation.Top,
                        maximumSize: environment.breakpoints.small
                    },
                    {
                        orientationDirection: Orientation.Right,
                    }
                ]
            },
            {
                title: 'Termine',
                selector: '#projectEvents',
                content: 'Alle kommenden Termine des Projekts findest du hier. Außerdem kannst du die Termine verändern und Anwesenheiten erfassen. Klicke dafür auf die Icons. Eine ausführliche Übersicht bekommst du, wenn du auf einen Termin klickst.',
                orientation: [
                    {
                        orientationDirection: Orientation.Top,
                        maximumSize: environment.breakpoints.medium
                    },
                    {
                        orientationDirection: Orientation.Left,
                    }
                ]
            },
            {
                title: 'Aktionen',
                selector: '#projectActions',
                content: 'Hier kannst du eine Erinnerung an das Projekt an deine Mitglieder schicken und auf dem Zahnrad, das Projektbearbeiten und ein paar nützliche Einstellungen machen. Außerdem kannst du dir einen Probenplan als PDF und als iCal herunterladen.',
                skipStep: window.innerWidth < environment.breakpoints.small + 1,
                orientation: [
                    {
                        orientationDirection: Orientation.Bottom,
                        maximumSize: environment.breakpoints.medium
                    },
                    {
                        orientationDirection: Orientation.Left,
                    }
                ]
            },
            {
                title: 'Probenphasen',
                selector: '#eventgroupCard',
                content: 'Zusätzlich zu Terminen kannst du auch Probenphasen anlegen, um z.B. ein Probenwochenende abzubilden. Hier stehen dir die gleichen Funktionen wie im Projekt zur verfügung. Zusätzlich bekommst du bei der Statistik noch nützliche Auswertungen, die für die Buchung einer Probenphase interessant sein können',
                orientation: [
                    {
                        orientationDirection: Orientation.Top,
                        maximumSize: environment.breakpoints.small
                    },
                    {
                        orientationDirection: Orientation.Right,
                    }
                ]
            },
            {
                title: 'Kontakt',
                selector: '#sidebar-communication',
                content: 'Damit du mit deinen Mitgliedern in Kontakt bleibst, kannst du hier E-Mails an verschiedene Zielgruppen senden.',
                action: () => {
                    this.router.navigateByUrl(`/communication`)
                },
                orientation: [
                    {
                        orientationDirection: Orientation.Top,
                        maximumSize: environment.breakpoints.small
                    },
                    {
                        orientationDirection: Orientation.Right,
                    }
                ]
            },
            {
                title: 'Los gehts!',
                content: 'Lege jetzt los und erstelle deinen ersten Termin in deinem Projekt.',
                action: () => {
                    this.router.navigateByUrl(`/projects/${this.getOrganisatorPayload().project.id}/ensembles/${this.getOrganisatorPayload().ensemble.id}`)
                },
            },
        ]
    }

    @Output()
    startTourEmitter: EventEmitter<any> = new EventEmitter<any>();

    constructor (private guidedTourService: GuidedTourService,
                 private sidebarService: SidebarService,
                 private cookieService: CookieService,
                 private router: Router) {
    }

    startMemberTour () {
        this.startTourEmitter.emit(this.memberTour);
    }

    startOrganisatorTour () {
        this.startTourEmitter.emit(this.organisatorTour);
        this.getOrganisatorPayload();
    }

    setOrganisatorPayload (payload, property: 'project' | 'ensemble' | 'organization') {
        const cookiePayload = this.getOrganisatorPayload();
        cookiePayload[property] = payload;
        this.cookieService.set('os_organizatorTourPayload', JSON.stringify(cookiePayload), 1, '/');
        console.log(cookiePayload);
    }

    getOrganisatorPayload(){
        if(this.cookieService.check('os_organizatorTourPayload')){
            console.log(JSON.parse(this.cookieService.get('os_organizatorTourPayload')))
            return JSON.parse(this.cookieService.get('os_organizatorTourPayload'));
        } else {
            return {};
        }

    }

    toggleSidebar () {
        if (window.innerWidth < environment.breakpoints.small + 1) {
            this.sidebarService.toggle();
        }
    }
}
