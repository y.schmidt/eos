import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { OrganizationGroup } from '../models/organizationGroup';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrganizationGroupService {

    apiUrl = environment.apiUrl + '/organizations';
    constructor(private http: HttpClient) { }

    getOrganizationGroupsForOrganization(orgaId: string): Observable<OrganizationGroup[]> {
        return this.http.get<OrganizationGroup[]>(`${this.apiUrl}/${orgaId}/organizationGroups`);
    }

    createOrganizationGroup(organizationGroup: OrganizationGroup): Observable<OrganizationGroup> {
        return this.http.post<OrganizationGroup>(`${this.apiUrl}/${organizationGroup.organization.id}/organizationGroups`, organizationGroup);
    }

    removeOrganizationGroup(organizationGroup: OrganizationGroup): Observable<any> {
        return this.http.delete(`${this.apiUrl}/${organizationGroup.organization.id}/organizationGroups/${organizationGroup.id}`);
    }

    updateOrganizationGroup(organizationGroup: OrganizationGroup): Observable<OrganizationGroup> {
        return this.http.put<OrganizationGroup>(`${this.apiUrl}/${organizationGroup.organization.id}/organizationGroups`, organizationGroup);
    }

    getUsersForOrganizationGroups(orgaId: string): Observable<User[]>{
        return this.http.get<User[]>(`${this.apiUrl}/${orgaId}/organizationGroups/users`);
    }

    assignUser(userId: string, orgaGroupId: string, orgaId: string): Observable<any>{
        return this.http.post<any>(`${this.apiUrl}/${orgaId}/organizationGroups/${orgaGroupId}/users/${userId}`, null);
    }

    unassignUser(userId: string, orgaGroupId: string, orgaId: string): Observable<any>{
        return this.http.delete<any>(`${this.apiUrl}/${orgaId}/organizationGroups/${orgaGroupId}/users/${userId}`);
    }
}
