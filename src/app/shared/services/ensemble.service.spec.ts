import { TestBed, inject } from '@angular/core/testing';

import { EnsembleService } from './ensemble.service';

describe('EnsembleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnsembleService]
    });
  });

  it('should be created', inject([EnsembleService], (service: EnsembleService) => {
    expect(service).toBeTruthy();
  }));
});
