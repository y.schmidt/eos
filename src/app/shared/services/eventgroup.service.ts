import { Injectable } from '@angular/core';
import { Eventgroup } from '../models/eventgroup';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Register } from '../models/register';
import { environment } from '../../../environments/environment';


@Injectable({
    providedIn: 'root'
})
export class EventgroupService {

    private apiUrl = environment.apiUrl;

    constructor(private http: HttpClient) { }

    getEventgroupById(ensembleId: string, eventgroupId: string): Observable<Eventgroup> {
        return this.http.get<Eventgroup>(`${this.apiUrl}/ensembles/${ensembleId}/eventgroups/${eventgroupId}`)
            .pipe(
                map(this.convertEventgroupDate)
            );
    }

    createEventgroup(ensembleId: string, projectId: string, eventgroup: Eventgroup): Observable<Eventgroup>{
        return this.http.post<Eventgroup>(`${this.apiUrl}/ensembles/${ensembleId}/projects/${projectId}/eventgroups`, eventgroup)
            .pipe(
                map(this.convertEventgroupDate)
            );
    }

    updateEventgroup(ensembleId: string, eventgroup: Eventgroup): Observable<Eventgroup>{
        return this.http.put<Eventgroup>(`${this.apiUrl}/ensembles/${ensembleId}/eventgroups`, eventgroup)
            .pipe(
                map(this.convertEventgroupDate)
            );
    }

    deleteEventgroup(ensembleId: string, eventgroupId: string): Observable<any> {
        return this.http.delete<any>(`${this.apiUrl}/ensembles/${ensembleId}/eventgroups/${eventgroupId}`);
    }

    getEventgroupRegisters(ensembleId: string, eventgroupId: string): Observable<Register[]> {
        return this.http.get<Register[]>(`${this.apiUrl}/ensembles/${ensembleId}/eventgroups/${eventgroupId}/registers`);
    }

    getEventgroupsForProject(ensembleId: string, projectId: string): Observable<Eventgroup[]>{
        return this.http.get<Eventgroup[]>(`${this.apiUrl}/ensembles/${ensembleId}/projects/${projectId}/eventgroups`)
            .pipe(
                map(this.convertEventgroupsDate)
            );
    }

    sendEventgroupReminder(ensembleId: string, eventgroupId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/ensembles/${ensembleId}/eventgroups/${eventgroupId}/sendReminder`);
    }

    private convertEventgroupsDate(eventgroups: Eventgroup[]): Eventgroup[]{
        return eventgroups.map((eventgroup: Eventgroup) => {
            eventgroup.start = new Date(eventgroup.start);
            eventgroup.end = new Date(eventgroup.end);
            if(eventgroup.deadline !== null){
                eventgroup.deadline = new Date(eventgroup.deadline);
            }
            if(eventgroup.project && eventgroup.project.start && eventgroup.project.end){
                eventgroup.project.start = new Date(eventgroup.project.start);
                eventgroup.project.end = new Date(eventgroup.project.end);
            }
            return eventgroup;
        });
    }

    private convertEventgroupDate(eventgroup: Eventgroup): Eventgroup{
        eventgroup.start = new Date(eventgroup.start);
        eventgroup.end = new Date(eventgroup.end);
        eventgroup.range = [eventgroup.start, eventgroup.end];
        if(eventgroup.deadline !== null){
            eventgroup.deadline = new Date(eventgroup.deadline);
        }
        if(eventgroup.project && eventgroup.project.start && eventgroup.project.end){
            eventgroup.project.start = new Date(eventgroup.project.start);
            eventgroup.project.end = new Date(eventgroup.project.end);
            eventgroup.range = [eventgroup.start, eventgroup.end];
        }
        return eventgroup;
    }

}
