import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Register } from '../models/register';

@Injectable({
    providedIn: 'root'
})
export class StatisticsService {

    apiUrl = environment.apiUrl;

    constructor(private http: HttpClient) { }

    countProjectEvents(ensembleId: string, projectId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/ensembles/${ensembleId}/projects/${projectId}/countEvents`);
    }

    countEventgroupEvents(ensembleId: string, eventgroupId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/ensembles/${ensembleId}/eventgroups/${eventgroupId}/countEvents`);
    }

    getProjectUserAnnouncementStats(ensembleId: string, projectId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/ensembles/${ensembleId}/projects/${projectId}/statistics`);
    }

    getEventgroupUserAnnouncementStats(ensembleId: string, eventgroupId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/ensembles/${ensembleId}/eventgroups/${eventgroupId}/statistics`);
    }

    getProjectRegistersStats(ensembleId: string, projectId: string): Observable<Register[]>{
        return this.http.get<Register[]>(`${this.apiUrl}/ensembles/${ensembleId}/projects/${projectId}/countRegisters`);
    }

    getEventRegistersStats(ensembleId: string, eventId: string): Observable<Register[]>{
        return this.http.get<Register[]>(`${this.apiUrl}/ensembles/${ensembleId}/events/${eventId}/countRegisters`);
    }

    getEventAnnouncementStats(ensembleId: string, eventId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/ensembles/${ensembleId}/events/${eventId}/announcementStats`);
    }

    getEventgroupAnnouncementStats(ensembleId: string, eventgroupId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/ensembles/${ensembleId}/eventgroups/${eventgroupId}/announcementStats`);
    }

    getProjectAnnouncementStats(ensembleId: string, projectId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/ensembles/${ensembleId}/projects/${projectId}/announcementStats`);
    }

    getEventgroupRegistersStats(ensembleId: string, eventgroupId: string): Observable<Register[]>{
        return this.http.get<Register[]>(`${this.apiUrl}/ensembles/${ensembleId}/eventgroups/${eventgroupId}/countRegisters`);
    }

    countAnnouncementsByType(ensembleId: string, target: 'eventgroup' | 'project', targetId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/ensembles/${ensembleId}/${target}/${targetId}/countAnnouncementsByType`);
    }

    countAnnouncementsFromUserByType(ensembleId: string, target: 'eventgroup' | 'project', targetId: string): Observable<any>{
        return this.http.get<any>(`${this.apiUrl}/ensembles/${ensembleId}/${target}/${targetId}/countAnnouncementsFromUserByType`);
    }

}
