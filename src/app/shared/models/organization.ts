import { User } from './user';
import { Address } from './address';
import { Nameble } from '../interfaces/nameble';
import { Official } from './official';
import { OrganizationUser } from './organizationUser';

export class Organization implements Nameble{
    id: string;
    name: string;
    owner: User;
    address: Address;
    userCounter: number;
    ensembleCounter: number;
    projectCounter: number;
    role: string;
    roleName: string;
    officials: Official[];
    organizationUsers: OrganizationUser[];
    stripeCustomerId: string;
    stripeSubscriptionId: string;


    constructor(){
        this.id = '';
        this.address = new Address();
    }
}