import { OrganizationUser } from './organizationUser';
import { EnsembleUser } from './ensembleUser';

export class Role{
    id: string;
    name: string;
    context: string;
    organizationUsers: OrganizationUser[];
    ensemblUsers: EnsembleUser[];
}