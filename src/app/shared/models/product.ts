import { ProductPlan } from './productPlan';

export class Product {
    id: string;
    name: string;
    productPlans: ProductPlan[];
    maxUsers: number;
    maxEnsembles: number;

    constructor (id: string) {
        this.id = id;
    }
}