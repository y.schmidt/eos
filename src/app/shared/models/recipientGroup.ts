import { Ensemble } from './ensemble';
import { Register } from './register';
import { OrganizationGroup } from './organizationGroup';
import { Organization } from './organization';

export class RecipientGroup{
    ensembles: Ensemble[];
    registers: Register[];
    groups: OrganizationGroup[];
    organizations: Organization[];

    constructor(){
        this.ensembles = [];
        this.registers = [];
        this.groups = [];
        this.organizations = [];
    }
}