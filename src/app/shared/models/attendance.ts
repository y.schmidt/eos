import { User } from './user';
import { Eventgroup } from './eventgroup';
import { Event } from './event';
import { Project } from './project';

export class Attendance {

    id: string;
    user: User;
    project: Project;
    eventgroup: Eventgroup;
    event: Event;
    status: boolean;
    comment: string;

    constructor(status: boolean){
        this.status = status;
        this.user = new User();
    }
}