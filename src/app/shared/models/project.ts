import { Event } from './event';
import { Eventgroup } from './eventgroup';
import { Ensemble } from './ensemble';
import { Announcement } from './announcement';
import { Address } from './address';
import { Register } from './register';

export class Project {

    id: string;
    name: string;
    description: string;
    address: Address;
    ensembles: Ensemble[];
    events: Event[];
    eventgroups: Eventgroup[];
    announcements: Announcement[];
    range: Date[];
    deadline: Date;
    start: Date;
    end: Date;
    status: string;
    registers: Register[];
    enableInformation: boolean;
    autoReminderSent: boolean;
    autoReminder: boolean;
    reminderDaysBeforeDeadline: number;
    color: any;

    constructor(){
        this.announcements = [];
        this.ensembles = [];
        this.status = 'draft';
        this.description = '';
        this.autoReminder = true;
        this.autoReminderSent = false;
        this.reminderDaysBeforeDeadline = 7;
    }
}
