import { Project } from './project';
import { Eventgroup } from './eventgroup';
import { Event } from './event';
import { User } from './user';

export class Announcement {

    id: string;
    user: User;
    project: Project;
    eventgroup: Eventgroup;
    event: Event;
    status: boolean;
    comment: string;
    roomWish: string;
    eatingHabits: string;
    allergy: string;
    updatedAt: Date;
    createdAt: Date;

    constructor(status: boolean){
        this.status = status;
        this.user = new User();
        this.comment = '';
        this.roomWish = '';
        this.eatingHabits = '';
    }
}