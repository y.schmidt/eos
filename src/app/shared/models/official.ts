import { User } from './user';
import { Ensemble } from './ensemble';
import { Organization } from './organization';

export class Official{

    id: string;
    name: string;
    prio: number;
    user: User;
    ensemble: Ensemble;
    organization: Organization;

    constructor () {
        this.name = '';
    }

}