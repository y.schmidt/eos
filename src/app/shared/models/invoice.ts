import { ProductPlan } from './productPlan';
import { Address } from './address';
import { Organization } from './organization';

export class Invoice{

    id: string;
    createdAt: Date;
    invoiceNumber: string;
    organization: Organization;
    productPlan: ProductPlan;
    amount: number;
    firstName: string;
    lastName: string;
    address: Address;
    status: string;
}