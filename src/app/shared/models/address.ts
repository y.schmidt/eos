export class Address {
  id: string;
  street: string;
  number: string;
  city: string;
  postalCode: string;
  country: string;
  custom: string;
}
