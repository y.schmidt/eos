export class UserSystem {
    id: string;
    password: string;
    confirmPassword: string;
    status: string;
    role: string
}