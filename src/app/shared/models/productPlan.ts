import { Product } from './product';
import { Abonnement } from './abonnement';

export class ProductPlan{

    id: string;
    product: Product;
    abonnement: Abonnement;
    price: number;
    currency: string;
    period: number;

}