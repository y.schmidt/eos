import { Organization } from './organization';
import { User } from './user';
import { Project } from './project';
import { Register } from './register';
import { EnsembleUser } from './ensembleUser';
import { Nameble } from '../interfaces/nameble';
import { Official } from './official';

export class Ensemble implements Nameble{
    id: string;
    name: string;
    organization: Organization;
    owner: User;
    projects: Project[];
    registers: Register[];
    ensembleUsers: EnsembleUser[];
    userCounter: number;
    projectCounter: number;
    role: string;
    officials: Official[];
}