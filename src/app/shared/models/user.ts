import { Address } from './address';
import { UserSystem } from './userSystem';
import { Register } from './register';
import { OrganizationGroup } from './organizationGroup';
import { Announcement } from './announcement';
import { Nameble } from '../interfaces/nameble';
import { Attendance } from './attendance';
import { Official } from './official';

export class User implements Nameble{
    id: string;
    firstName: string;
    lastName: string;
    mail: string;
    phone: string;
    address: Address;
    userSystem: UserSystem;
    registers: Register[];
    organizationGroups: OrganizationGroup[];
    announcements: Announcement[];
    announcement: Announcement;
    attendance: Attendance[];
    comment: string;
    name: string;
    autoReminder: boolean;
    shareAddressWithOrganizations: boolean;
    sharePhoneWithOrganizations: boolean;
    sharePhoneWithMembers: boolean;
    profilePicture: string;
    officials: Official[];
    acceptTermsOfUse: boolean;

    checked: boolean;

    constructor () {
        this.name = this.firstName + ' ' + this.lastName;
    }

}