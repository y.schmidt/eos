import { User } from './user';
import { Organization } from './organization';
import { Ensemble } from './ensemble';
import { Project } from './project';
import { Register } from './register';
import { OrganizationGroup } from './organizationGroup';
import { PostComment } from './postComment';

export class Post {
    id: string;
    author: User;
    date: Date;
    content: string;
    organization: Organization;
    ensemble: Ensemble;
    project: Project;
    register: Register;
    orgaGroup: OrganizationGroup;
    comments: PostComment[];

    constructor(){
        this.content = '';
        this.comments = [];
    }
}