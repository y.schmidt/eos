import { Organization } from './organization';
import { Ensemble } from './ensemble';
import { User } from './user';

export class Invite {
    id: string;
    mail: string;
    organization: Organization;
    ensemble: Ensemble;
    counter: number;
    inviter: User;

    constructor(organization: Organization, mail: string, inviter: User, ensemble?: Ensemble){
        this.counter = 1;
        this.organization = organization;
        this.mail = mail;
        this.ensemble = ensemble;
        this.inviter = inviter;
    }
}