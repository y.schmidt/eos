import { Organization } from './organization';
import { User } from './user';
import { Role } from './role';

export class OrganizationUser {
    organization: Organization;
    user: User;
    role: Role;
    status: string;
}