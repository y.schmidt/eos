import { User } from './user';
import { Organization } from './organization';

export class OrganizationGroup {
    id: string;
    name: string;
    users: User[];
    organization: Organization;
}