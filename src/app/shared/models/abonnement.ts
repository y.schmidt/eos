import { Organization } from './organization';
import { ProductPlan } from './productPlan';

export class Abonnement {

    id: string;
    organization: Organization;
    productPlan: ProductPlan;
    validTill: Date;
    updatedAt: Date;
}