import { Address } from './address';
import { Announcement } from './announcement';
import { Project } from './project';
import { Event } from './event';
import { Register } from './register';
import { CalendarEvent } from 'angular-calendar';


export class Eventgroup implements CalendarEvent{

    id: string;
    title: string;
    location: string;
    description: string;
    start: Date;
    end: Date;
    range: Date[];
    address: Address;
    announcements: Announcement[];
    events: Event[];
    project: Project;
    registers: Register[];
    deadline: Date;
    autoReminderSent: boolean;
    autoReminder: boolean;
    reminderDaysBeforeDeadline: number;
    enableInformation: boolean;
    roomWish: boolean;
    eatingHabits: boolean;
    allergy: boolean;
    color?: any;
    allDay?: boolean;
    actions?: any;

    constructor () {
        this.address = new Address();
        this.announcements = [];
        this.project = new Project();
        this.description = '';

    }


}

