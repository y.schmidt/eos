import { User } from './user';
import { Post } from './post';

export class PostComment{

    id: string;
    author: User;
    date: Date;
    content: string;
    post: Post;

    constructor(post: Post){
        this.post = post;
    }
}



