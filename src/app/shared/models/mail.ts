import { User } from './user';

export class Mail {

    id: string;
    sender: User;
    recipients: User[];
    subject: string;
    message: string;
    html: string;
    sendAt: Date;
    type: string;
    isDraft: boolean;
    isSent: boolean;
    isDeleted: boolean;
    attachements: Array<File>;

    constructor(){
        this.message = '';
        this.subject = '';
        this.isDraft = true;
        this.recipients = [];
        this.attachements = [];
    }
}