import { Address } from './address';
import { Announcement } from './announcement';
import { Project } from './project';
import { Eventgroup } from './eventgroup';
import { Register } from './register';
import { CalendarEvent } from 'angular-calendar';

export class Event implements CalendarEvent{

    id: string;
    title: string;
    start: Date;
    end: Date;
    address: Address;
    project: Project;
    eventgroup: Eventgroup;
    location: string;
    type: string;
    description: string;
    announcements: Announcement[];
    serial: boolean;
    editRepeat: boolean;
    successor: Event;
    interval: number;
    endRepeat: Date;
    registers: Register[];
    deadline: Date;
    enableInformation: boolean;
    autoReminderSent: boolean;
    autoReminder: boolean;
    color: any;
    actions: any;


    constructor () {
        this.address = new Address();
        this.start = new Date();
        this.end = new Date();
        this.endRepeat = new Date();
        this.start.setMinutes(0);
        this.end.setMinutes(0);
        this.start.setHours(this.end.getHours() + 1);
        this.end.setHours(this.end.getHours() + 3);
        this.announcements = [];
        this.serial = false;
        this.interval = 0;
        this.type = 'rehearsal';
        this.editRepeat = true;
        this.project = new Project();
        this.eventgroup = new Eventgroup();
    }
}

