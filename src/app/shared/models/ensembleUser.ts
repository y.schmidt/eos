import { Ensemble } from './ensemble';
import { User } from './user';
import { Role } from './role';

export class EnsembleUser {
    ensemble: Ensemble;
    user: User;
    role: Role;
    status: string;
}