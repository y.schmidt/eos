import { User } from './user';
import { Ensemble } from './ensemble';

export class Register{
    id: string;
    name: string;
    users: User[];
    ensemble: Ensemble;
    prio: number;
    userCounter: number;
    shortcut: string;

    constructor() {
        this.userCounter = 0;
    }
}