import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
    selector: 'eos-confirm-modal',
    templateUrl: './confirm-modal.component.html',
    styleUrls: ['./confirm-modal.component.css']
})
export class ConfirmModalComponent<T> implements OnInit {

    private object: T;

    info: string;
    @Output()
    confirmEvent: EventEmitter<any> = new EventEmitter<any>();
    constructor(public confirmModal: BsModalRef) {}

    ngOnInit() {
    }

    confirm(){
        this.confirmEvent.emit(this.object);
    }

    setObject(object: T){
        this.object = object;
    }

    getObject(): T {
        return this.object;
    }

}
