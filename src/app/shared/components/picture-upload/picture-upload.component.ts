import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FileUploadService } from '../../services/file-upload.service';
import { NotificationService } from '../../services/notification.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { ImageCroppedEvent, ImageCropperComponent } from 'ngx-image-cropper';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'eos-picture-upload',
  templateUrl: './picture-upload.component.html',
  styleUrls: ['./picture-upload.component.css']
})
export class PictureUploadComponent implements OnInit {

  @Input()
  aspectRatio: number = 1;

  @Input()
  resizeWidth: number = 150;

  @Input()
  resizeHeight: number = 150;

  @Input()
  mode: string;

  @Input()
  target: any;

  percentDone: number;
  uploadSucceed: boolean;
  wrongType: boolean;
  uploadStarted: boolean = false;
  file: Blob;

  errors = [];

  imageChangedEvent: any = '';
  showCropper: boolean = false;

  editPictureEvent: boolean = false;

  @ViewChild(ImageCropperComponent, {static: true})
  imageCropper: ImageCropperComponent;

  @Output()
  uploadEmitter: EventEmitter<any> = new EventEmitter<any>();

  constructor(public modalRef: BsModalRef,
              private uploadService: FileUploadService,
              private notificationService: NotificationService,
              private modalService: BsModalService,
              private userService: UserService) { }

  ngOnInit() {
  }

  fileChangeEvent(event: any): void {
    this.editPictureEvent = true;
    if(event.target.files[0].type.substr(0, 6) != 'image/'){
      this.wrongType = true;
    } else {
      this.wrongType = false;
      this.imageChangedEvent = event;
    }
  }

  imageCropped(event: ImageCroppedEvent) {
    this.file = event.file;
  }

  rotate() {
    this.editPictureEvent = true;
    this.imageCropper.rotateLeft();
  }

  imageLoaded() {
    this.showCropper = true;
  }

  submit(){
    this.uploadStarted = true;
    switch (this.mode) {
      case 'profilePicture':
        this.uploadService.uploadProfilePicture(this.file, this.target.id)
            .subscribe((event) => {
                  if (event.type === HttpEventType.UploadProgress) {
                    // This is an upload progress event. Compute and show the % done:
                    this.percentDone = Math.round(100 * event.loaded / event.total);
                  } else if (event instanceof HttpResponse) {
                    this.uploadSucceed = true;
                    setTimeout(() => {
                      this.userService.profilePictureEmitter.emit();
                      this.uploadSuccess();
                    }, 1500);
                  }
                },
                error => {
                  this.uploadError(error);
                });
        break;
      case 'organizationPicture':
        this.uploadService.uploadOrganizationPicture(this.file, this.target.id)
            .subscribe((event) => {
              if (event.type === HttpEventType.UploadProgress) {
                // This is an upload progress event. Compute and show the % done:
                this.percentDone = Math.round(100 * event.loaded / event.total);
              } else if (event instanceof HttpResponse) {
                this.uploadSucceed = true;
                setTimeout(() => {
                  this.userService.profilePictureEmitter.emit();
                  this.uploadSuccess();
                }, 1500);
              }
            },
                error => {
                  this.uploadError(error);
                });
        break;
      case 'projectPicture':
        this.uploadService.uploadProjectPicture(this.file, this.target.ensembles[0].id, this.target.id)
            .subscribe((event) => {
                  if (event.type === HttpEventType.UploadProgress) {
                    // This is an upload progress event. Compute and show the % done:
                    this.percentDone = Math.round(100 * event.loaded / event.total);
                  } else if (event instanceof HttpResponse) {
                    this.uploadSucceed = true;
                    setTimeout(() => {
                      this.uploadSuccess();
                    }, 1500);
                  }
                },
                error => {
                  this.uploadError(error);
                });
        break;
    }
  }

  uploadSuccess(){
    this.resetUpload();
    this.uploadEmitter.emit();
    this.modalRef.hide();
  }

  uploadError(error){
    this.notificationService.setErrorNotification(error);
    this.resetUpload();
  }

  cropperReady(){
    this.editPictureEvent = false;
  }

  resetUpload(){
    this.uploadSucceed = false;
    this.percentDone = 0;
    this.uploadStarted = false;
    delete(this.file);
  }

}
