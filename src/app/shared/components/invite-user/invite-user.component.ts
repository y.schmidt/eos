import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { User } from '../../models/user';
import { Invite } from '../../models/invite';
import { InviteService } from '../../services/invite.service';
import { Ensemble } from '../../models/ensemble';
import { Organization } from '../../models/organization';
import { NotificationService } from '../../services/notification.service';
import { AuthGuardService } from '../../services/auth-guard.service';
import { NgForm } from '@angular/forms';
import { EnsembleUser } from '../../models/ensembleUser';
import { OrganizationUser } from '../../models/organizationUser';

@Component({
    selector: ' eos-invite-user',
    templateUrl: './invite-user.component.html',
    styleUrls: ['./invite-user.component.css']
})
export class InviteUserComponent implements OnInit {

    @Input()
    ensemble: Ensemble;

    @Input()
    organization: Organization;

    @Output()
    newOrganizationUserEmitter: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    newEnsembleUserEmitter: EventEmitter<any> = new EventEmitter<any>();

    newMember: User = new User();
    loading: boolean = false;
    loadReminder: boolean = false;
    invites: Invite[];
    isAdmin: boolean = false;
    errorList: [];

    @ViewChild('f')
    form: NgForm;

    constructor(private inviteService: InviteService,
                private notificationService: NotificationService,
                private authGuard: AuthGuardService) { }

    ngOnInit() {
        this.isAdmin = this.authGuard.hasUserPermission('organizationAdmin', this.organization.id);
        this.getInvites();
    }

    getInvites(){
        if(this.ensemble){
            this.inviteService.getInvitesForEnsemble(this.ensemble.id)
                .subscribe((invites) => {
                        this.invites = invites;
                    },
                    error => this.notificationService.setErrorNotification(error)
                );
        } else {
            this.inviteService.getInvitesForOrganization(this.organization.id)
                .subscribe((invites) => {
                        this.invites = invites;
                    },
                    error => this.notificationService.setErrorNotification(error)
                );
        }
    }

    inviteUser(){
        const invite = new Invite(
            this.organization,
            this.newMember.mail,
            this.authGuard.getAuthedUser(),
            this.ensemble);
        this.inviteService.inviteUser(invite)
            .subscribe((invite) => {
                if(!invite.status){
                    this.invites.push(invite);
                } else {
                    if(invite.ensemble){
                        this.newEnsembleUserEmitter.emit(invite);
                    } else {
                        this.newOrganizationUserEmitter.emit(invite);
                    }
                }
                this.form.form.reset();
                this.notificationService.setSuccessNotification('Erfolg');
            }, error => this.notificationService.setErrorNotification(error));
    }

    deleteInvite(invite: Invite){
        this.inviteService.deleteInvite(this.organization.id, invite.id)
            .subscribe(() => {
                    const index = this.invites.findIndex(remInvite => remInvite.id == invite.id);
                    if (index !== -1) {
                        this.invites.splice(index, 1);
                    }
                    this.notificationService.setSuccessNotification('Einladung gelöscht');
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    sendReminder(invite: Invite){
        this.loadReminder = true;
        invite.organization = this.organization;
        if(this.ensemble){
            invite.ensemble = this.ensemble;
        }
        this.inviteService.reinviteUser(this.organization.id, invite)
            .subscribe((newInvite) => {
                    invite = newInvite;
                    this.loadReminder = false;
                    this.notificationService.setSuccessNotification('Erinnerung gesendet');
                },
                error => this.notificationService.setErrorNotification(error)
            );
    }

    newInvite(invite: Invite){
        this.invites.push(invite);
    }

    newEnsembleUser(ensembleUser: EnsembleUser){
        this.newEnsembleUserEmitter.emit(ensembleUser);
    }

    newInviteError(errorList){
        this.errorList = [];
        this.errorList = errorList;
    }

    newOrgaUser(organizationUser: OrganizationUser){
        this.newOrganizationUserEmitter.emit(organizationUser);
    }
}
