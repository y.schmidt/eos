import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Ensemble } from '../../models/ensemble';
import { Organization } from '../../models/organization';
import { EnsembleUser } from '../../models/ensembleUser';
import { OrganizationUser } from '../../models/organizationUser';
import { EnsembleService } from '../../services/ensemble.service';
import { FormGroup } from '@angular/forms';
import { Official } from '../../models/official';
import { OrganizationService } from '../../services/organization.service';
import { NotificationService } from '../../services/notification.service';

@Component({
  selector: 'eos-officials',
  templateUrl: './officials.component.html',
  styleUrls: ['./officials.component.css']
})
export class OfficialsComponent implements OnInit {

  @Input()
  target: Ensemble | Organization;

  @Input()
  users: EnsembleUser[] | OrganizationUser[];

  @Input()
  mode: 'ensemble' | 'orga';

  @ViewChild('f')
  form: FormGroup;

  officials: Official[];
  newOfficial: Official;

  constructor(private ensembleService: EnsembleService,
              private orgaService: OrganizationService,
              private notificationService: NotificationService) { }

  ngOnInit() {
    this.newOfficial = new Official();
    this.getOfficials();
  }

  getOfficials(){
      if(this.mode == 'ensemble') {
          this.ensembleService.getEnsembleOfficials(this.target.id)
              .subscribe((officials) => {
                  this.officials = officials;
              });
      } else {
            this.orgaService.getOrganizationOfficials(this.target.id)
                .subscribe((officials) => {
                    this.officials = officials;
                });
      }
  }

  addOfficial(){
      if(this.form.valid){
          this.newOfficial.prio = this.officials.length;
          if(this.mode == 'ensemble'){
              this.ensembleService.createEnsembleOfficial(this.target.id, this.newOfficial)
                  .subscribe((official) => {
                      // @ts-ignore
                      this.officials.push(official);
                      this.newOfficial = new Official();
                      this.form.reset();
                      this.notificationService.setSuccessNotification('Funktionär erfolgreich angelegt');
                  },
                      error => this.notificationService.setErrorNotification(error));
          } else {
              this.orgaService.createOrganizationOfficial(this.target.id, this.newOfficial)
                  .subscribe((official) => {
                      // @ts-ignore
                      this.officials.push(official);
                      this.newOfficial = new Official();
                      this.form.reset();
                      this.notificationService.setSuccessNotification('Funktionär erfolgreich angelegt');
                  },
                      error => this.notificationService.setErrorNotification(error));
          }

      }
  }
  
  removeOfficial(official: Official){
     if(this.mode == 'ensemble'){
         this.ensembleService.deleteEnsembleOfficial(this.target.id, official.id)
             .subscribe(() => {
                    this.getOfficials();
                    this.notificationService.setSuccessNotification('Funktionär gelöscht');
                 },
                 error => this.notificationService.setErrorNotification(error)
             );
     } else {
         this.orgaService.deleteOrganizationOfficial(this.target.id, official.id)
             .subscribe(() => {
                     this.getOfficials();
                     this.notificationService.setSuccessNotification('Funktionär gelöscht');
             },
                 error => this.notificationService.setErrorNotification(error)
             );
     }
  }

  updateOfficial(official: Official, notification = true){
      if(this.mode == 'ensemble'){
          this.ensembleService.updateEnsembleOfficial(this.target.id, official)
              .subscribe((updatedOfficial) => {
                  official = updatedOfficial;
                  if(notification){
                      this.notificationService.setSuccessNotification('Funktionär erfolgreich geändert');
                  }
              });
      } else {
          this.orgaService.updateOrganizationOfficial(this.target.id, official)
              .subscribe((updatedOfficial) => {
                  official = updatedOfficial;
                  if(notification){
                      this.notificationService.setSuccessNotification('Funktionär erfolgreich geändert');
                  }
              });
      }
  }

    decrementOfficial(official: Official){
        const lowerOfficial = this.officials.find(off => off.prio === official.prio+1);
        lowerOfficial.prio--;
        official.prio++;
        this.updateOfficial(lowerOfficial, false);
        this.updateOfficial(official, false);
    }

    incrementOfficial(official: Official){
        const lowerOfficial = this.officials.find(off => off.prio === official.prio-1);
        lowerOfficial.prio++;
        official.prio--;
        this.updateOfficial(lowerOfficial, false);
        this.updateOfficial(official, false);
    }

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

}
