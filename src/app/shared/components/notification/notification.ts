export class Notification {
    type: string;
    heading: string;
    message: string;
    dismissTime: number;

    constructor(type: string, heading: string, message: string, dismissTime?: number){
        this.type = type;
        this.heading = heading;
        this.message = message;
        this.dismissTime = dismissTime;
    }
}