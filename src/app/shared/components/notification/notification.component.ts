import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../services/notification.service';
import { Notification } from './notification';

@Component({
    selector: 'eos-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

    notifications: Notification[] = [];
    constructor(private notificationService: NotificationService) { }

    ngOnInit() {
        this.notificationService.notifcation
            .subscribe((message) => {
                this.notifications.push(message);
            });
    }



}
