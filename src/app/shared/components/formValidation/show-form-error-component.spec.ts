import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowFromErrorComponent } from './show-form-error.component';

describe('ShowFromErrorComponent', () => {
  let component: ShowFromErrorComponent;
  let fixture: ComponentFixture<ShowFromErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowFromErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowFromErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
