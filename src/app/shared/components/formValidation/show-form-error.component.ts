import { Component, Input } from '@angular/core';
import { AbstractControl, AbstractControlDirective } from '@angular/forms';

@Component({
    selector: 'app-show-form-errors',
    template: `
      <div *ngIf="shouldShowErrors()">
        <span *ngFor="let error of listOfErrors()">{{error}} </span>
      </div>
    `,
    styles: ['div { width: 100%; font-size: 80%; color: #dc3545; margin-top: .25rem; }']
})
export class ShowFromErrorComponent {

    private static readonly errorMessages = {
        'required': () => 'Das Feld muss ausgefüllt sein.',
        'minlength': (params) => 'Das Feld muss mindestens ' + params.requiredLength + ' Zeichen enthalten.',
        'maxlength': (params) => 'Das Feld darf maximal ' + params.requiredLength + ' Zeichen enthalten.',
        'pattern': (params) => 'The required pattern is: ' + params.requiredPattern,
        'email': () => 'Der Wert ist keine korrekte E-Mailadresse.',
        'equals': (params) => params.message,
        'mailExists': (params) => params.message,
        'custom': (params) => params.message,
        'passwordStrength': (params) => params.message,
        'bsDate': () => 'Ungültiges Datum',
    };

    @Input()
    private control: AbstractControlDirective | AbstractControl;

    /* +dirty, wenn Fehler erst nach eingabe auftauchen soll */
    shouldShowErrors(): boolean {
        return this.control &&
            this.control.errors && this.control.touched &&
            this.control.invalid;
    }

    listOfErrors(): string[] {
        return Object.keys(this.control.errors)
            .map(field => this.getMessage(field, this.control.errors[field]));
    }

    private getMessage(type: string, params: any) {
        return ShowFromErrorComponent.errorMessages[type](params);
    }

}
