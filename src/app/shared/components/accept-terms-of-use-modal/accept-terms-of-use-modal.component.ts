import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { UserService } from '../../services/user.service';
import { AuthGuardService } from '../../services/auth-guard.service';

@Component({
  selector: 'eos-accept-terms-of-use-modal',
  templateUrl: './accept-terms-of-use-modal.component.html',
  styleUrls: ['./accept-terms-of-use-modal.component.css']
})
export class AcceptTermsOfUseModalComponent implements OnInit {

  @Output()
  confirmEvent: EventEmitter<any> = new EventEmitter<any>();

  constructor(public confirmModal: BsModalRef,
              private authGuardService: AuthGuardService,
              private userService: UserService) {}

  ngOnInit(): void {
  }

  confirm(){
    const user = this.authGuardService.getAuthedUser();
    user.acceptTermsOfUse = true;
    this.userService.updateUser(user)
        .subscribe((user) => {
          if(user.acceptTermsOfUse){
            this.confirmModal.hide();
          }
        })
  }

}
