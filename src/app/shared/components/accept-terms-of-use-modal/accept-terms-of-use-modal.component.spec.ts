import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptTermsOfUseModalComponent } from './accept-terms-of-use-modal.component';

describe('AcceptTermsOfUseModalComponent', () => {
  let component: AcceptTermsOfUseModalComponent;
  let fixture: ComponentFixture<AcceptTermsOfUseModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptTermsOfUseModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptTermsOfUseModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
