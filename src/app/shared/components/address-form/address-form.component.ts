import { Component, Input, OnInit } from '@angular/core';
import { Address } from '../../models/address';
import { ControlContainer, NgForm } from '@angular/forms';

@Component({
    selector: 'eos-address-form',
    templateUrl: './address-form.component.html',
    styleUrls: ['./address-form.component.css'],
    viewProviders: [ { provide: ControlContainer, useExisting: NgForm } ]
})
export class AddressFormComponent implements OnInit {

    @Input() address: Address;
    @Input() required: boolean;

    constructor() { }

    ngOnInit() {
    }
}
