import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as ClassicEditor from 'ckeditor5-build-classic-alignment-highlight';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular';

@Component({
  selector: 'eos-ckeditor',
  templateUrl: './ckeditor.component.html',
  styleUrls: ['./ckeditor.component.css']
})
export class CkeditorComponent implements OnInit {

  @Input()
  content: string;

  @Input()
  autoFocus: boolean = false;

  @Output()
  contentChanged: EventEmitter<any> = new EventEmitter();


  public Editor = ClassicEditor;
  public config = {
    toolbar: [ 'heading', '|', 'bold', 'italic', 'underline', 'link', 'highlight', 'bulletedList', 'numberedList' ],
  };
  constructor() { }

  ngOnInit() {
  }

  onReady(event: any) {
    if(this.autoFocus){
      event.editing.view.focus();
    }
  }

  onChange( { editor }: ChangeEvent ) {
    this.contentChanged.emit(editor.getData());
  }

}
