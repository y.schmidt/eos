import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CsvMemberUploadComponent } from './csv-member-upload.component';

describe('CsvMemberUploadComponent', () => {
  let component: CsvMemberUploadComponent;
  let fixture: ComponentFixture<CsvMemberUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CsvMemberUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CsvMemberUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
