import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { FileUploadService } from '../../services/file-upload.service';
import { NotificationService } from '../../services/notification.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Ensemble } from '../../models/ensemble';
import { Organization } from '../../models/organization';
import { NgxCsvParser, NgxCSVParserError } from 'ngx-csv-parser';


@Component({
    selector: 'eos-csv-member-upload',
    templateUrl: './csv-member-upload.component.html',
    styleUrls: ['./csv-member-upload.component.css']
})
export class CsvMemberUploadComponent implements OnInit {

    percentDone: number;
    uploadSuccess: boolean;
    wrongType: boolean;
    uploadStarted: boolean = false;
    modalRef: BsModalRef;
    file: File;

    parsedMails: string[];

    @ViewChild('fileImportInput', { static: false }) fileImportInput: any;

    @Input()
    ensemble: Ensemble;

    @Input()
    organization: Organization;

    @Output()
    newInviteEmitter: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    newEnsembleUserEmitter: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    newOrgaUserEmitter: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    newErrorEmitter: EventEmitter<any> = new EventEmitter<any>();

    errors = [];

    constructor(private uploadService: FileUploadService,
                private notificationService: NotificationService,
                private modalService: BsModalService,
                private ngxCsvParser: NgxCsvParser) { }

    ngOnInit() {
    }

    submit(){
        let subject = null;
        let mode;
        if(this.ensemble){
            mode = 'ensembles';
            subject = this.ensemble.id;
        } else {
            mode = 'organizations';
        }
        this.uploadStarted = true;
        this.uploadService.uploadUserCsv(this.file, this.organization.id, mode, subject)
            .subscribe((event) => {
                if (event.type === HttpEventType.UploadProgress) {
                    // This is an upload progress event. Compute and show the % done:
                    this.percentDone = Math.round(100 * event.loaded / event.total);
                } else if (event instanceof HttpResponse) {
                    this.uploadSuccess = true;
                    this.distinguishResponse(JSON.parse(JSON.stringify(event.body)));
                    setTimeout(() => {
                        this.resetUpload();
                        this.modalRef.hide();
                    }, 1500);

                }
            }, error => {
                this.notificationService.setErrorNotification(error);
                this.resetUpload();
            });
    }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    resetUpload(){
        this.uploadSuccess = false;
        this.percentDone = 0;
        this.uploadStarted = false;
        delete(this.file);
    }

    distinguishResponse(response){
        for(let i = 0; i < response.length; i++){
            if(response[i].counter){
                this.newInviteEmitter.emit(response[i]);
            } else if(response[i].isProjectEditor){
                this.newEnsembleUserEmitter.emit(response[i]);
                this.newOrgaUserEmitter.emit(response[i])
            } else {
                this.errors.push(response[i]);
            }
        }
        if(this.errors.length > 0) {
            this.newErrorEmitter.emit(this.errors);
        }
    }

    // Your applications input change listener for the CSV File
    fileChangeListener($event: any): void {
        this.parsedMails = [];
        // Select the files from the event
        const files = $event.target.files;
        // Parse the file you want to select for the operation along with the configuration
        this.ngxCsvParser.parse(files[0], { header: true, delimiter: ',' })
            .pipe().subscribe((result: Array<any>) => {
                result.forEach((res) => {
                    this.parsedMails.push(res.mail);
                });
            this.file = files[0];
        }, (error: NgxCSVParserError) => {
            this.notificationService.setManualErrorNotification(error.message)
        });

    }
}
