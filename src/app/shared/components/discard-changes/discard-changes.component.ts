import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'eos-discard-changes',
  templateUrl: './discard-changes.component.html',
  styleUrls: ['./discard-changes.component.css']
})
export class DiscardChangesComponent implements OnInit {

  @Input()
  showSaveEvent: boolean = false;

  @Output()
  confirmEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  saveEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(public confirmModal: BsModalRef) {}

  ngOnInit() {
  }

  discard(){
    this.confirmEvent.emit(true);
  }

  saveChanges(){
    this.confirmEvent.emit(true);
    this.saveEvent.emit(true);
  }

}
