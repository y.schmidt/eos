import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { interval, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'eos-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.css'],
})
export class ToastComponent implements OnInit, OnDestroy {

  /** Number in milliseconds, after which alert will be closed */
  @Input()
  dismissOnTimeout: number;

  /** Is alert visible */
  @Input()
  isOpen = true;

  @Input()
  heading: string;

  @Input()
  type: string;

  private durationTimer: Subscription;
  public progressLineWidth: string = '100%';
  private currentDuration: number;
  private msInterval: number;

  constructor() {}

  ngOnInit(): void {
    if (this.dismissOnTimeout) {
      this.currentDuration = this.dismissOnTimeout;
      this.msInterval = this.dismissOnTimeout / 100;
      this.durationTimer = interval(this.msInterval).pipe(
        tap(() => {
          this.currentDuration -= this.msInterval;
          this.progressLineWidth = (100 / this.dismissOnTimeout * this.currentDuration).toFixed(0) + '%';

          if (this.currentDuration <= 0) {
            this.close();
            this.durationTimer.unsubscribe();
          }
        })
      ).subscribe();
    }
  }

  /**
   * Closes an alert by removing it from the DOM.
   */
  close(): void {
    if (!this.isOpen) {
      return;
    }
    this.isOpen = false;
  }

  ngOnDestroy() {
    this.durationTimer.unsubscribe();
  }

}
