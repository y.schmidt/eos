import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmModalAdvancedComponent } from './confirm-modal-advanced.component';

describe('ConfirmModalAdvancedComponent', () => {
  let component: ConfirmModalAdvancedComponent;
  let fixture: ComponentFixture<ConfirmModalAdvancedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmModalAdvancedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmModalAdvancedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
