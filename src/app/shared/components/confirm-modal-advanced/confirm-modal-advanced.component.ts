import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
    selector: 'eos-confirm-modal-advanced',
    templateUrl: './confirm-modal-advanced.component.html',
    styleUrls: ['./confirm-modal-advanced.component.css']
})
export class ConfirmModalAdvancedComponent<T> implements OnInit {


    @Input()
    private object: T;

    @Input()
    heading: string;

    @Input()
    confirmationInfo: string;

    @Input()
    info: string;

    confirmString: string;
    name: string;

    @Output()
    confirmEvent: EventEmitter<any> = new EventEmitter<any>();

    constructor(public confirmModal: BsModalRef) {}

    ngOnInit() {
        // @ts-ignore
        if(this.getObject().name != undefined){
            //@ts-ignore
            this.name = this.getObject().name
            // @ts-ignore
        } else if(this.getObject().title != undefined){
            // @ts-ignore
            this.name = this.getObject().title
        }
    }

    confirm(){
        this.confirmEvent.emit(this.object);
    }

    getObject(): T {
        return this.object;
    }

}
