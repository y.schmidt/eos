import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CdkStepper } from '@angular/cdk/stepper';
import { Directionality } from '@angular/cdk/bidi';
import { StepperService } from '../../services/stepper.service';

@Component({
  selector: 'eos-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.css'],
  providers: [{ provide: CdkStepper, useExisting: StepperComponent }],
})
export class StepperComponent extends CdkStepper implements OnInit{

  @Input()
  linear: boolean = true;

  @Input()
  submitLabel: string;

  @Input()
  showFooter: boolean = true;

  @Input()
  headerClickEnabled: boolean = true;

  @Output()
  submitted: EventEmitter<any> = new EventEmitter<any>();

  constructor(dir: Directionality,
              changeDetectorRef: ChangeDetectorRef,
              private stepperService: StepperService
  ) {
    super(dir, changeDetectorRef);
  }

  ngOnInit () {
    this.stepperService.nextStepEmitter.subscribe(() => {
      this.nextStep();
    });
    this.stepperService.lastStepEmitter.subscribe(() => {
      this.previous();
    })
  }


  onHeaderClick(index: number): void {
    if(this.headerClickEnabled){
      this.selectedIndex = index;
    }
  }

  submit(){
    this.submitted.emit();
  }

  nextStep (): void {
    window.scrollTo(0,0);
    this.selectionChange.emit();
    this.next();
  }
}
