import { Component, OnInit, Input, QueryList, Output, EventEmitter } from '@angular/core';
import { CdkStep } from '@angular/cdk/stepper';

@Component({
  selector: 'eos-stepper-header',
  templateUrl: './stepper-header.component.html',
  styleUrls: ['./stepper-header.component.css'],
})
export class StepperHeaderComponent implements OnInit {

  @Input()
  steps: QueryList<CdkStep>;

  _selectedIndex: number;
  progress: number = 0;

  @Output()
  stepClickedEmitter: EventEmitter<number> = new EventEmitter<number>();
  constructor() { }

  ngOnInit() {
  }

  @Input() set selectedIndex(number: number){
    this._selectedIndex = number;
    this.setProgressBar(number);
  }

  setProgressBar(selectedStep: number) {
    const numberOfSteps = this.getNumberOfSteps();
    const part = (100 / numberOfSteps);
    if(selectedStep === numberOfSteps - 1){
      this.progress = ((selectedStep + 1) * part);
    } else {
      this.progress = ((selectedStep + 1) * part) - (part/2);

    }
  }

  getNumberOfSteps(): number{
    return this.steps.length;

  }

  stepClicked(index: number){
    this.stepClickedEmitter.emit(index);
  }

}
