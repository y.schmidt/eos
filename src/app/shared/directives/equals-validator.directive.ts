import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

@Directive({
    selector: '[eosEqualsValidator]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: EqualsValidatorDirective, multi: true }
    ]
})
export class EqualsValidatorDirective implements Validator{

    private message = {
        'equals': {
            'message': 'Die Felder stimmen nicht überein.'
        }
    };

    @Input()
    eosEqualsValidator: string;

    constructor() { }

    validate(c: AbstractControl): ValidationErrors {
        if(c.touched && c.invalid){
            return null;
        }
        let value = c.value;
        let equals = c.root.get(this.eosEqualsValidator);

        if (equals && value !== equals.value) {
            return this.message;
        }
        return null;
    }

}
