import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: 'img[eosImgFallback]'
})
export class ImgFallbackDirective {

  @Input()
  imgFallback: string;

  constructor(private eRef: ElementRef) { }

  @HostListener('error')
  loadFallbackOnError(){
    const element: HTMLImageElement = <HTMLImageElement>this.eRef.nativeElement;
    element.src = this.imgFallback || './assets/img/user.jpg';
  }

}
