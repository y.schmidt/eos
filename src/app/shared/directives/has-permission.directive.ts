import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthGuardService } from '../services/auth-guard.service';

@Directive({
  selector: '[hasPermission]'
})
export class HasPermissionDirective implements OnInit{

  constructor(
      private templateRef: TemplateRef<any>,
      private authGuardService: AuthGuardService,
      private viewContainer: ViewContainerRef
  ) {}

  private _permission: any;
  private _context: string;

  ngOnInit() {
    if (this.authGuardService.hasUserPermission(this._permission, this._context)) {
      this.viewContainer.clear();
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }

  @Input() set hasPermission(permission: string) {
    this._permission = permission;
  }

}
