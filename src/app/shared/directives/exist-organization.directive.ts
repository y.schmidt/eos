import { Directive, Input } from '@angular/core';
import { OrganizationService } from '../services/organization.service';
import { AbstractControl, NG_ASYNC_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

@Directive({
    selector: '[eosExistOrganization]',
    providers: [
        { provide: NG_ASYNC_VALIDATORS, useExisting: ExistOrganizationDirective, multi: true }
    ]
})
export class ExistOrganizationDirective implements Validator{
    private message = {
        'custom': {
            'message': 'Organisation existiert bereits.'
        }
    };

    @Input('eosExistOrganization')
    initialName: string;

    constructor(private organizationService: OrganizationService) { }

    validate (c: AbstractControl): Promise<ValidationErrors> {
        return new Promise(resolve => {
            if((c.touched && c.invalid) || this.initialName == c.value){
                resolve(null);
            }
            this.organizationService.existOrganization(c.value)
                .subscribe((exists) => {
                    if (exists) {
                        resolve(this.message)
                    } else {
                        resolve(null);
                    }
                });
        })
    }
}


