import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';


@Directive({
    selector: '[eosPasswordStrength]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: PasswordStrengthDirective, multi: true }
    ]
})
export class PasswordStrengthDirective implements Validator{

    private message = {
        'passwordStrength': {
            'message': 'Passwort muss mindestens 8 Zeichen, Groß- und Kleinbuchstaben, sowie Ziffern enthalten.'
        }
    };

    constructor() { }

    validate(c: AbstractControl): ValidationErrors {
        if(c.touched && c.invalid){
            return null;
        }
        if(c.value){
            const hasNumber = /\d/.test(c.value);
            const hasUpper = /[A-Z]/.test(c.value);
            const hasLower = /[a-z]/.test(c.value);
            const length = c.value.length >= 8;
            //console.log('Num, Upp, Low, length', hasNumber, hasUpper, hasLower, length);
            const isStrong = hasNumber && hasUpper && hasLower && length;
            if (!isStrong) {
                return this.message;
            }
        }
        return null;
    }

}