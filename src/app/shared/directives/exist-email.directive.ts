import { Directive } from '@angular/core';
import {
    AbstractControl,
    NG_ASYNC_VALIDATORS, ValidationErrors,
    Validator
} from '@angular/forms';
import { UserService } from '../services/user.service';

@Directive({
    selector: '[eosExistEmail]',
    providers: [
        { provide: NG_ASYNC_VALIDATORS, useExisting: ExistEmailDirective, multi: true }
    ]
})
export class ExistEmailDirective implements Validator {
    private message = {
        'mailExists': {
            'message': 'E-Mailadresse ist schon vergeben'
        }
    };

    constructor (private userService: UserService) {
    }
    //reverse: https://scotch.io/tutorials/how-to-implement-a-custom-validator-directive-confirm-password-in-angular-2

    validate (c: AbstractControl): Promise<ValidationErrors> {
        return new Promise(resolve => {
            if(c.touched && c.valid && c.value){
                resolve(null);
            }
            this.userService.existMail(c.value)
                .subscribe((exists) => {
                    if (exists) {
                        resolve(this.message)
                    } else {
                        resolve(null);
                    }
                });
        })
    }
}
