import {
    Directive,
    Input,
    OnInit,
    TemplateRef,
    ViewContainerRef
} from '@angular/core';

import { AuthGuardService } from '../services/auth-guard.service';

@Directive({ selector: '[showAuthed]' })
export class ShowAuthedDirective implements OnInit {
    constructor(
        private templateRef: TemplateRef<any>,
        private authGuardService: AuthGuardService,
        private viewContainer: ViewContainerRef
    ) {}

    condition: boolean;

    ngOnInit() {
        this.authGuardService.isAuthenticated
            .subscribe((isAuthenticated) => {
                if (isAuthenticated == this.condition) {
                    this.viewContainer.clear();
                    this.viewContainer.createEmbeddedView(this.templateRef);
                } else {
                    this.viewContainer.clear();
                }
            }
        )
    }

    @Input() set showAuthed(condition: boolean) {
        this.condition = condition;
    }

}
