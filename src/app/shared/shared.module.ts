import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ShowFromErrorComponent } from './components/formValidation/show-form-error.component';
import { AddressFormComponent } from './components/address-form/address-form.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ShowAuthedDirective } from './directives/show-authed.directive';
import { httpInterceptorProviders } from './httpInterceptorProviders';
import { NotificationComponent } from './components/notification/notification.component';
import { ExistEmailDirective } from './directives/exist-email.directive';
import { EqualsValidatorDirective } from './directives/equals-validator.directive';
import { ExistOrganizationDirective } from './directives/exist-organization.directive';
import { FilterByPipe } from './pipes/filter.pipe';
import { ArraysortPipe } from './pipes/arraysort.pipe';
import { ConfirmModalComponent } from './components/confirm-modal/confirm-modal.component';
import { PasswordStrengthDirective } from './directives/password-strength.directive';
import { ConfirmModalAdvancedComponent } from './components/confirm-modal-advanced/confirm-modal-advanced.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgcCookieConsentModule } from 'ngx-cookieconsent';
import { cookieConfig } from '../../cookie-popup.config';
import { CookieService } from 'ngx-cookie-service';
import { InviteUserComponent } from './components/invite-user/invite-user.component';
import { CsvMemberUploadComponent } from './components/csv-member-upload/csv-member-upload.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { TimeAgoPipe } from './pipes/time-ago.pipe';
import { ClickOutsideModule } from 'ng-click-outside';
import { CkeditorComponent } from './components/ckeditor/ckeditor.component';
import { HasPermissionDirective } from './directives/has-permission.directive';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { PictureUploadComponent } from './components/picture-upload/picture-upload.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ImgFallbackDirective } from './directives/img-fallback.directive';
import { OfficialsComponent } from './components/officials/officials.component';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { StepperComponent } from './components/stepper/stepper.component';
import { StepperHeaderComponent } from './components/stepper-header/stepper-header.component';
import { DiscardChangesComponent } from './components/discard-changes/discard-changes.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AlertModule } from 'ngx-bootstrap/alert';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { ToastComponent } from './components/toast/toast.component';
import { NgxCsvParserModule } from 'ngx-csv-parser';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Angulartics2Module } from 'angulartics2';
import { GuidedTourModule, GuidedTourService } from 'ngx-guided-tour';
import { AcceptTermsOfUseModalComponent } from './components/accept-terms-of-use-modal/accept-terms-of-use-modal.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}

@NgModule({
    imports: [
        CommonModule,
        ModalModule.forRoot(),
        AlertModule.forRoot(),
        CollapseModule.forRoot(),
        BsDropdownModule.forRoot(),
        ButtonsModule.forRoot(),
        TabsModule.forRoot(),
        FormsModule,
        BsDatepickerModule.forRoot(),
        TimepickerModule.forRoot(),
        ProgressbarModule.forRoot(),
        TooltipModule.forRoot(),
        HttpClientModule,
        PopoverModule.forRoot(),
        NgxChartsModule,
        NgcCookieConsentModule.forRoot(cookieConfig),
        NgxSpinnerModule,
        CKEditorModule,
        ClickOutsideModule,
        CollapseModule.forRoot(),
        PerfectScrollbarModule,
        ImageCropperModule,
        CdkStepperModule,
        FontAwesomeModule,
        NgxCsvParserModule,
        TranslateModule.forRoot({
            defaultLanguage: 'de',
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        Angulartics2Module.forRoot({
            pageTracking: {
                clearIds: true,
                clearQueryParams: true,
                clearHash: true,
            }
        }),
        GuidedTourModule
    ],
    declarations: [
        ShowFromErrorComponent,
        AddressFormComponent,
        ShowAuthedDirective,
        NotificationComponent,
        ExistEmailDirective,
        EqualsValidatorDirective,
        ExistOrganizationDirective,
        FilterByPipe,
        ArraysortPipe,
        ConfirmModalComponent,
        PasswordStrengthDirective,
        ConfirmModalAdvancedComponent,
        InviteUserComponent,
        CsvMemberUploadComponent,
        SpinnerComponent,
        TimeAgoPipe,
        CkeditorComponent,
        HasPermissionDirective,
        PictureUploadComponent,
        ImgFallbackDirective,
        OfficialsComponent,
        StepperComponent,
        StepperHeaderComponent,
        DiscardChangesComponent,
        ToastComponent,
        AcceptTermsOfUseModalComponent,
    ],
    exports: [
        ShowFromErrorComponent,
        ModalModule,
        AlertModule,
        ButtonsModule,
        CollapseModule,
        FormsModule,
        BsDropdownModule,
        AddressFormComponent,
        BsDatepickerModule,
        TimepickerModule,
        ProgressbarModule,
        HttpClientModule,
        TabsModule,
        ShowAuthedDirective,
        HasPermissionDirective,
        NotificationComponent,
        ExistEmailDirective,
        EqualsValidatorDirective,
        ExistOrganizationDirective,
        FilterByPipe,
        ArraysortPipe,
        ConfirmModalComponent,
        TooltipModule,
        PopoverModule,
        PasswordStrengthDirective,
        ConfirmModalAdvancedComponent,
        NgxChartsModule,
        NgcCookieConsentModule,
        InviteUserComponent,
        CsvMemberUploadComponent,
        NgxSpinnerModule,
        CKEditorModule,
        TimeAgoPipe,
        ClickOutsideModule,
        CkeditorComponent,
        CollapseModule,
        PerfectScrollbarModule,
        PictureUploadComponent,
        ImageCropperModule,
        ImgFallbackDirective,
        OfficialsComponent,
        CdkStepperModule,
        StepperComponent,
        DiscardChangesComponent,
        FontAwesomeModule,
        TranslateModule,
        Angulartics2Module,
        GuidedTourModule

    ],
    providers: [
        httpInterceptorProviders,
        CookieService,
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        },
        GuidedTourService
    ],
    entryComponents: [
        ConfirmModalAdvancedComponent,
        ConfirmModalComponent,
        DiscardChangesComponent,
        PictureUploadComponent
    ]
})
export class SharedModule { }
