export const environment = {
    production: true,
    apiUrl: 'https://api.mymember.one/api',
    version: require('../../package.json').version,
    appName: require('../../package.json').name,
    mapsApiKey: 'AIzaSyDL5j3eGHQa8Lo_XCep-a3JlLoUnCeznMk',
    breakpoints: {
        small: 576,
        medium: 768,
        large: 960
    }
};
