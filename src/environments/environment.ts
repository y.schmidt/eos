// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    apiUrl: 'http://localhost:3000/api',
    version: require('../../package.json').version,
    appName: require('../../package.json').name,
    stripeApiKey: 'pk_test_Hh8ktlc6s40yUJmiStvZw9kt001cKgJDEF',
    mapsApiKey: 'AIzaSyDL5j3eGHQa8Lo_XCep-a3JlLoUnCeznMk',
    breakpoints: {
        small: 576,
        medium: 768,
        large: 992
    }
};

/*
 * In development recipientGroup, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production recipientGroup
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
